import 'dart:convert';
import 'dart:js';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/screens/job_admin_home.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginRepository{
  Future _login(String username,String pass) async {

    print(username+pass);

    Map<String, String> body = {
      'username': username,
      'password': pass,
    };

    final response =
    await http.post(BaseUrl.baseurl+"check_login",body: json.encode(body));

    print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    final String status = jsonresp['status'];
    //final String token = jsonresp['data']['status'];
    print(status);

    return status;


//    DataUser duser = new DataUser.fromJson(jsonresp);
//
//    print('Status : ${duser.status}');
    //var status = datauser.message.toString();
    //print(datauser.message+"body"+response.body);



//    if(response.statusCode==200){
//      var datauser = json.decode(response.body);
//
//      Navigator.pop(context);
//      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
//          HomePage()));
//
//    }else{

//      Navigator.pop(context);
//      Flushbar(
//        message:  "Login Failed",
//        duration:  Duration(seconds: 3),
//      )..show(context);
//    }
  }
}