class TechnicianClass {
  final String id;
  final String name;
  final String designation;
  final String jobinhand;
  final String userprofile;

  TechnicianClass(this.id,this.name, this.designation, this.jobinhand,this.userprofile);
}