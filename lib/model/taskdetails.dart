class Task_Details {
  List<Data> data;

  Task_Details({this.data});

  Task_Details.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  List<Results> results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  Job job;

  Results({this.job});

  Results.fromJson(Map<String, dynamic> json) {
    job = json['job'] != null ? new Job.fromJson(json['job']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.job != null) {
      data['job'] = this.job.toJson();
    }
    return data;
  }
}

class Job {
  String id;
  String taskTitle;
  String ticketId;
  String jobAdminInstruction;
  String serviceDescription;
  String jobStatusTypeId;
  String jobStatus;
  String priorityTypeId;
  String priority;
  String progress;
  String jobStartDate;
  String assignedDate;
  String deliveryDate;

  Job(
      {this.id,
        this.taskTitle,
        this.ticketId,
        this.jobAdminInstruction,
        this.serviceDescription,
        this.jobStatusTypeId,
        this.jobStatus,
        this.priorityTypeId,
        this.priority,
        this.progress,
        this.jobStartDate,
        this.assignedDate,
        this.deliveryDate});

  Job.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    taskTitle = json['task_title'];
    ticketId = json['ticket_id'];
    jobAdminInstruction = json['job_admin_instruction'];
    serviceDescription = json['service_description'];
    jobStatusTypeId = json['job_status_type_id'];
    jobStatus = json['job_status'];
    priorityTypeId = json['priority_type_id'];
    priority = json['priority'];
    progress = json['progress'];
    jobStartDate = json['job_start_date'];
    assignedDate = json['assigned_date'];
    deliveryDate = json['delivery_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['task_title'] = this.taskTitle;
    data['ticket_id'] = this.ticketId;
    data['job_admin_instruction'] = this.jobAdminInstruction;
    data['service_description'] = this.serviceDescription;
    data['job_status_type_id'] = this.jobStatusTypeId;
    data['job_status'] = this.jobStatus;
    data['priority_type_id'] = this.priorityTypeId;
    data['priority'] = this.priority;
    data['progress'] = this.progress;
    data['job_start_date'] = this.jobStartDate;
    data['assigned_date'] = this.assignedDate;
    data['delivery_date'] = this.deliveryDate;
    return data;
  }
}