class ReportList {
  List<Data> data;

  ReportList({this.data});

  ReportList.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  List<Results> results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  Tasks tasks;

  Results({this.tasks});

  Results.fromJson(Map<String, dynamic> json) {
    tasks = json['tasks'] != null ? new Tasks.fromJson(json['tasks']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tasks != null) {
      data['tasks'] = this.tasks.toJson();
    }
    return data;
  }
}

class Tasks {
  String id;
  String ticketId;
  String assignDate;
  String deliveryDate;
  String completionDate;
  String name;
  String taskTitle;
  String jobStatusTypeId;
  String jobStatus;

  Tasks(
      {this.id,
        this.ticketId,
        this.assignDate,
        this.deliveryDate,
        this.completionDate,
        this.name,
        this.taskTitle,
        this.jobStatusTypeId,
        this.jobStatus});

  Tasks.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ticketId = json['ticket_id'];
    assignDate = json['assign_date'];
    deliveryDate = json['delivery_date'];
    completionDate = json['completion_date'];
    name = json['name'];
    taskTitle = json['task_title'];
    jobStatusTypeId = json['job_status_type_id'];
    jobStatus = json['job_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ticket_id'] = this.ticketId;
    data['assign_date'] = this.assignDate;
    data['delivery_date'] = this.deliveryDate;
    data['completion_date'] = this.completionDate;
    data['name'] = this.name;
    data['task_title'] = this.taskTitle;
    data['job_status_type_id'] = this.jobStatusTypeId;
    data['job_status'] = this.jobStatus;
    return data;
  }
}