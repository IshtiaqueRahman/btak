class Technician_Profile {
  List<Data> data;

  Technician_Profile({this.data});

  Technician_Profile.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  Result result;

  Data({this.status, this.message, this.result});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  String name;
  String phone;
  String email;
  String address;
  String employeeid;
  String joiningDate;
  String username;
  String designationName;
  String officeName;
  Null usercategoryName;
  String departmentName;
  String img;
  String totalRatings;
  String ratings;

  Result(
      {this.name,
        this.phone,
        this.email,
        this.address,
        this.employeeid,
        this.joiningDate,
        this.username,
        this.designationName,
        this.officeName,
        this.usercategoryName,
        this.departmentName,
        this.img,
        this.totalRatings,
        this.ratings});

  Result.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    employeeid = json['employeeid'];
    joiningDate = json['joining_date'];
    username = json['username'];
    designationName = json['designation_name'];
    officeName = json['office_name'];
    usercategoryName = json['usercategory_name'];
    departmentName = json['department_name'];
    img = json['img'];
    totalRatings = json['total_ratings'];
    ratings = json['ratings'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['employeeid'] = this.employeeid;
    data['joining_date'] = this.joiningDate;
    data['username'] = this.username;
    data['designation_name'] = this.designationName;
    data['office_name'] = this.officeName;
    data['usercategory_name'] = this.usercategoryName;
    data['department_name'] = this.departmentName;
    data['img'] = this.img;
    data['total_ratings'] = this.totalRatings;
    data['ratings'] = this.ratings;
    return data;
  }
}