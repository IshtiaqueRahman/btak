class Technician_Dashboard {
  List<Data> data;

  Technician_Dashboard({this.data});

  Technician_Dashboard.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  Results results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    results =
    json['results'] != null ? new Results.fromJson(json['results']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.toJson();
    }
    return data;
  }
}

class Results {
  String newTasks;
  String pendingTasks;
  String inProgressTasks;
  String completeTasks;
  String toalServices;

  Results(
      {this.newTasks,
        this.pendingTasks,
        this.inProgressTasks,
        this.completeTasks,
        this.toalServices});

  Results.fromJson(Map<String, dynamic> json) {
    newTasks = json['new_tasks'];
    pendingTasks = json['pending_tasks'];
    inProgressTasks = json['in_progress_tasks'];
    completeTasks = json['complete_tasks'];
    toalServices = json['toal_services'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['new_tasks'] = this.newTasks;
    data['pending_tasks'] = this.pendingTasks;
    data['in_progress_tasks'] = this.inProgressTasks;
    data['complete_tasks'] = this.completeTasks;
    data['toal_services'] = this.toalServices;
    return data;
  }
}