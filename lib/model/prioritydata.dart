class Priority {
  List<PriorityTypes> priorityTypes;

  Priority({this.priorityTypes});

  Priority.fromJson(Map<String, dynamic> json) {
    if (json['priority_types'] != null) {
      priorityTypes = new List<PriorityTypes>();
      json['priority_types'].forEach((v) {
        priorityTypes.add(new PriorityTypes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.priorityTypes != null) {
      data['priority_types'] =
          this.priorityTypes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PriorityTypes {
  PriorityType priorityType;

  PriorityTypes({this.priorityType});

  PriorityTypes.fromJson(Map<String, dynamic> json) {
    priorityType = json['PriorityType'] != null
        ? new PriorityType.fromJson(json['PriorityType'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.priorityType != null) {
      data['PriorityType'] = this.priorityType.toJson();
    }
    return data;
  }
}

class PriorityType {
  String id;
  String name;
  String updatedAt;

  PriorityType({this.id, this.name, this.updatedAt});

  PriorityType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}