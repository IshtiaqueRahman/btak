class PendingTicketService {
  List<Data> data;

  PendingTicketService({this.data});

  PendingTicketService.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  String today;
  List<Results> results;

  Data({this.status, this.message, this.today, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    today = json['today'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['today'] = this.today;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  List<LatestServices> latestServices;
  List<OldServices> oldServices;

  Results({this.latestServices, this.oldServices});

  Results.fromJson(Map<String, dynamic> json) {
    if (json['latest_services'] != null) {
      latestServices = new List<LatestServices>();
      json['latest_services'].forEach((v) {
        latestServices.add(new LatestServices.fromJson(v));
      });
    }
    if (json['old_services'] != null) {
      oldServices = new List<OldServices>();
      json['old_services'].forEach((v) {
        oldServices.add(new OldServices.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.latestServices != null) {
      data['latest_services'] =
          this.latestServices.map((v) => v.toJson()).toList();
    }
    if (this.oldServices != null) {
      data['old_services'] = this.oldServices.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OldServices {
  Services services;

  OldServices({this.services});

  OldServices.fromJson(Map<String, dynamic> json) {
    services = json['services'] != null
        ? new Services.fromJson(json['services'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.services != null) {
      data['services'] = this.services.toJson();
    }
    return data;
  }
}

class LatestServices {
  Services services;

  LatestServices({this.services});

  LatestServices.fromJson(Map<String, dynamic> json) {
    services = json['services'] != null
        ? new Services.fromJson(json['services'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.services != null) {
      data['services'] = this.services.toJson();
    }
    return data;
  }
}

class Services {
  String id;
  String serviceTitle;
  String bnServiceTitle;
  String userId;
  String officeId;
  String isJobAssign;
  String requisitionDepartmentId;
  String ticketStatusTypeId;
  String createdAt;
  String userName;
  String userProfile;
  String departmentName;

  Services(
      {this.id,
        this.serviceTitle,
        this.bnServiceTitle,
        this.userId,
        this.officeId,
        this.isJobAssign,
        this.requisitionDepartmentId,
        this.ticketStatusTypeId,
        this.createdAt,
        this.userName,
        this.userProfile,
        this.departmentName});

  Services.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    serviceTitle = json['service_title'];
    bnServiceTitle = json['bn_service_title'];
    userId = json['user_id'];
    officeId = json['office_id'];
    isJobAssign = json['is_job_assign'];
    requisitionDepartmentId = json['requisition_department_id'];
    ticketStatusTypeId = json['ticket_status_type_id'];
    createdAt = json['created_at'];
    userName = json['user_name'];
    userProfile = json['user_profile'];
    departmentName = json['department_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service_title'] = this.serviceTitle;
    data['bn_service_title'] = this.bnServiceTitle;
    data['user_id'] = this.userId;
    data['office_id'] = this.officeId;
    data['is_job_assign'] = this.isJobAssign;
    data['requisition_department_id'] = this.requisitionDepartmentId;
    data['ticket_status_type_id'] = this.ticketStatusTypeId;
    data['created_at'] = this.createdAt;
    data['user_name'] = this.userName;
    data['user_profile'] = this.userProfile;
    data['department_name'] = this.departmentName;
    return data;
  }
}
