class DashboardProfile {
  List<Data> data;

  DashboardProfile({this.data});

  DashboardProfile.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  Results results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    results =
    json['results'] != null ? new Results.fromJson(json['results']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.toJson();
    }
    return data;
  }
}

class Results {
  String name;
  String userProfile;
  String departmentName;
  Null departmentBnName;
  String newPending;
  String onGoingTask;
  String completeTasks;
  String refusedJob;
  String totalRatings;
  Null ratings;

  Results(
      {this.name,
        this.userProfile,
        this.departmentName,
        this.departmentBnName,
        this.newPending,
        this.onGoingTask,
        this.completeTasks,
        this.refusedJob,
        this.totalRatings,
        this.ratings});

  Results.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    userProfile = json['user_profile'];
    departmentName = json['department_name'];
    departmentBnName = json['department_bn_name'];
    newPending = json['new_pending'];
    onGoingTask = json['on_going_task'];
    completeTasks = json['complete_tasks'];
    refusedJob = json['refused_job'];
    totalRatings = json['total_ratings'];
    ratings = json['ratings'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['user_profile'] = this.userProfile;
    data['department_name'] = this.departmentName;
    data['department_bn_name'] = this.departmentBnName;
    data['new_pending'] = this.newPending;
    data['on_going_task'] = this.onGoingTask;
    data['complete_tasks'] = this.completeTasks;
    data['refused_job'] = this.refusedJob;
    data['total_ratings'] = this.totalRatings;
    data['ratings'] = this.ratings;
    return data;
  }
}