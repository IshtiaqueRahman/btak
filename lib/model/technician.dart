class Technician_List {
  List<Data> data;

  Technician_List({this.data});

  Technician_List.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  List<Results> results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  Technician technician;

  Results({this.technician});

  Results.fromJson(Map<String, dynamic> json) {
    technician = json['technician'] != null
        ? new Technician.fromJson(json['technician'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.technician != null) {
      data['technician'] = this.technician.toJson();
    }
    return data;
  }
}

class Technician {
  String id;
  String name;
  String banglaName;
  String designation;
  String phone;
  String email;
  String userProfile;
  String totalJobInHands;

  Technician(
      {this.id,
        this.name,
        this.banglaName,
        this.designation,
        this.phone,
        this.email,
        this.userProfile,
        this.totalJobInHands});

  Technician.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    banglaName = json['bangla_name'];
    designation = json['designation'];
    phone = json['phone'];
    email = json['email'];
    userProfile = json['user_profile'];
    totalJobInHands = json['total_job_in_hands'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['bangla_name'] = this.banglaName;
    data['designation'] = this.designation;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['user_profile'] = this.userProfile;
    data['total_job_in_hands'] = this.totalJobInHands;
    return data;
  }
}