class Service_Lists {
  List<Data> data;

  Service_Lists({this.data});

  Service_Lists.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  List<Results> results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  Services services;

  Results({this.services});

  Results.fromJson(Map<String, dynamic> json) {
    services = json['services'] != null
        ? new Services.fromJson(json['services'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.services != null) {
      data['services'] = this.services.toJson();
    }
    return data;
  }
}

class Services {
  String id;
  String userId;
  String serviceTitle;
  String bnServiceTitle;
  String isVerifyByJobAdmin;
  String priority;
  String ticketStatusTypeId;
  String status;
  String createdAt;
  String userName;
  String progress;
  String userProfile;
  String departmentName;

  Services(
      {this.id,
        this.userId,
        this.serviceTitle,
        this.bnServiceTitle,
        this.isVerifyByJobAdmin,
        this.priority,
        this.ticketStatusTypeId,
        this.status,
        this.createdAt,
        this.userName,
        this.progress,
        this.userProfile,
        this.departmentName});

  Services.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    serviceTitle = json['service_title'];
    bnServiceTitle = json['bn_service_title'];
    isVerifyByJobAdmin = json['is_verify_by_job_admin'];
    priority = json['priority'];
    ticketStatusTypeId = json['ticket_status_type_id'];
    status = json['status'];
    createdAt = json['created_at'];
    userName = json['user_name'];
    progress = json['progress'];
    userProfile = json['user_profile'];
    departmentName = json['department_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['service_title'] = this.serviceTitle;
    data['bn_service_title'] = this.bnServiceTitle;
    data['is_verify_by_job_admin'] = this.isVerifyByJobAdmin;
    data['priority'] = this.priority;
    data['ticket_status_type_id'] = this.ticketStatusTypeId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['user_name'] = this.userName;
    data['progress'] = this.progress;
    data['user_profile'] = this.userProfile;
    data['department_name'] = this.departmentName;
    return data;
  }
}