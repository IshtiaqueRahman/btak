class ItemList {
  List<RefuseReasons> refuseReasons;

  ItemList({this.refuseReasons});

  ItemList.fromJson(Map<String, dynamic> json) {
    if (json['RefuseReasons'] != null) {
      refuseReasons = new List<RefuseReasons>();
      json['RefuseReasons'].forEach((v) {
        refuseReasons.add(new RefuseReasons.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.refuseReasons != null) {
      data['RefuseReasons'] =
          this.refuseReasons.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RefuseReasons {
  RefuseReason refuseReason;

  RefuseReasons({this.refuseReason});

  RefuseReasons.fromJson(Map<String, dynamic> json) {
    refuseReason = json['RefuseReason'] != null
        ? new RefuseReason.fromJson(json['RefuseReason'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.refuseReason != null) {
      data['RefuseReason'] = this.refuseReason.toJson();
    }
    return data;
  }
}

class RefuseReason {
  String id;
  String name;

  RefuseReason({this.id, this.name});

  RefuseReason.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}