class JobadminDashboard {
  List<Data> data;

  JobadminDashboard({this.data});

  JobadminDashboard.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int pendingServices;
  int allServices;

  Data({this.pendingServices, this.allServices});

  Data.fromJson(Map<String, dynamic> json) {
    pendingServices = json['pending_services'];
    allServices = json['all_services'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pending_services'] = this.pendingServices;
    data['all_services'] = this.allServices;
    return data;
  }
}
