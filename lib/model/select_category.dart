class SelectCategory {
  final int id;
  final String name;

  SelectCategory({this.id, this.name});

}