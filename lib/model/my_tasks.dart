class My_Task_Lists {
  List<Data> data;

  My_Task_Lists({this.data});

  My_Task_Lists.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  List<Results> results;

  Data({this.status, this.message, this.results});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  Tasks tasks;

  Results({this.tasks});

  Results.fromJson(Map<String, dynamic> json) {
    tasks = json['tasks'] != null ? new Tasks.fromJson(json['tasks']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tasks != null) {
      data['tasks'] = this.tasks.toJson();
    }
    return data;
  }
}

class Tasks {
  String id;
  String serviceId;
  String taskTitle;
  String jobStatusTypeId;
  String jobStatus;
  String priorityTypeId;
  String priority;
  String progress;
  String assignedDate;

  Tasks(
      {this.id,
        this.serviceId,
        this.taskTitle,
        this.jobStatusTypeId,
        this.jobStatus,
        this.priorityTypeId,
        this.priority,
        this.progress,
        this.assignedDate});

  Tasks.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    serviceId = json['service_id'];
    taskTitle = json['task_title'];
    jobStatusTypeId = json['job_status_type_id'];
    jobStatus = json['job_status'];
    priorityTypeId = json['priority_type_id'];
    priority = json['priority'];
    progress = json['progress'];
    assignedDate = json['assigned_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service_id'] = this.serviceId;
    data['task_title'] = this.taskTitle;
    data['job_status_type_id'] = this.jobStatusTypeId;
    data['job_status'] = this.jobStatus;
    data['priority_type_id'] = this.priorityTypeId;
    data['priority'] = this.priority;
    data['progress'] = this.progress;
    data['assigned_date'] = this.assignedDate;
    return data;
  }
}