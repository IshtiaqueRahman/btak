class ServiceDetailsClass {
  List<Data> data;

  ServiceDetailsClass({this.data});

  ServiceDetailsClass.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int status;
  String message;
  ServiceDetails serviceDetails;

  Data({this.status, this.message, this.serviceDetails});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    serviceDetails = json['service_details'] != null
        ? new ServiceDetails.fromJson(json['service_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.serviceDetails != null) {
      data['service_details'] = this.serviceDetails.toJson();
    }
    return data;
  }
}

class ServiceDetails {
  String id;
  String userId;
  String serviceTitle;
  String bnServiceTitle;
  String priorityTypeId;
  String requisitionDepartmentId;
  String departmentName;
  Null bnDepartmentName;
  String description;
  String requestDate;
  List<TaskList> taskList;

  ServiceDetails(
      {this.id,
        this.userId,
        this.serviceTitle,
        this.bnServiceTitle,
        this.priorityTypeId,
        this.requisitionDepartmentId,
        this.departmentName,
        this.bnDepartmentName,
        this.description,
        this.requestDate,
        this.taskList});

  ServiceDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    serviceTitle = json['service_title'];
    bnServiceTitle = json['bn_service_title'];
    priorityTypeId = json['priority_type_id'];
    requisitionDepartmentId = json['requisition_department_id'];
    departmentName = json['department_name'];
    bnDepartmentName = json['bn_department_name'];
    description = json['description'];
    requestDate = json['request_date'];
    if (json['task_list'] != null) {
      taskList = new List<TaskList>();
      json['task_list'].forEach((v) {
        taskList.add(new TaskList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['service_title'] = this.serviceTitle;
    data['bn_service_title'] = this.bnServiceTitle;
    data['priority_type_id'] = this.priorityTypeId;
    data['requisition_department_id'] = this.requisitionDepartmentId;
    data['department_name'] = this.departmentName;
    data['bn_department_name'] = this.bnDepartmentName;
    data['description'] = this.description;
    data['request_date'] = this.requestDate;
    if (this.taskList != null) {
      data['task_list'] = this.taskList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TaskList {
  String id;
  String jobStatusTypeId;
  String technicianName;
  String technicianBnName;
  String jobStatus;
  String task;

  TaskList(
      {this.id,
        this.jobStatusTypeId,
        this.technicianName,
        this.technicianBnName,
        this.jobStatus,
        this.task});

  TaskList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    jobStatusTypeId = json['job_status_type_id'];
    technicianName = json['technician_name'];
    technicianBnName = json['technician_bn_name'];
    jobStatus = json['job_status'];
    task = json['task'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['job_status_type_id'] = this.jobStatusTypeId;
    data['technician_name'] = this.technicianName;
    data['technician_bn_name'] = this.technicianBnName;
    data['job_status'] = this.jobStatus;
    data['task'] = this.task;
    return data;
  }
}