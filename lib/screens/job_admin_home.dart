import 'dart:convert';

import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/screens/pending_ticket.dart';
import 'package:btakapps/screens/teammember_userreport.dart';
import 'package:btakapps/screens/techinician_profile.dart';
import 'package:btakapps/model/jobadmindash.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:jiffy/jiffy.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'allticketjobadmin.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
//  final List<Trip> tripsList = [
//    Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
//    Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
//    Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
//    Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
//    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
//  ];

  var pending,acceptlist;
  var fullname;
  var currentdate,date;
  var datetime;

  @override
    void initState() {
    // TODO: implement initState
    getData();
    super.initState();

  }

  Future getData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");
    fullname = pref.get("full_name");
    currentdate = Jiffy().yMMMMd;

    date = DateTime.now();
    print(officeId);

    datetime =  DateFormat('EEEE').format(date) +" , "+ currentdate;


    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      'office_id': officeId,
      'department_id': departmentId
    };

    print(body);

    final response =
    await http.post(BaseUrl.baseurl+"service_count.json",body: json.encode(body));

    Map pendingTickerMap = jsonDecode(response.body);

    //print(response.body);
    print(body);

    var jobadmindash = JobadminDashboard.fromJson(pendingTickerMap);


    setState(() {
      pending = jobadmindash.data[0].pendingServices;
      acceptlist = jobadmindash.data[0].allServices;
    });

    //print(token);

    print(pending);

//    DataUser duser = new DataUser.fromJson(jsonresp);
//
//    print('Status : ${duser.status}');
    //var status = datauser.message.toString();
    //print(datauser.message+"body"+response.body);




  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff3D4FF4), //or set color with: Color(0xFF0000FF)
    ));
    return new Scaffold(
      backgroundColor: Color(0xffF6F7F8),
      body: SingleChildScrollView(
        child: new Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 267,
                  decoration: BoxDecoration(
                    color: Color(0xff3D4FF4),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  padding: EdgeInsets.only(top: 25),
                ),

                Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                          color: Color(0xff3D4FF4),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Align(
                                        alignment: AlignmentDirectional.topStart,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              child: Text(
                                                fullname ?? '',
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  color: Colors.white,
                                                  height: 1.5,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: Text(
                                                datetime ?? '',
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: Color(0xffCCFFFFFF),
                                                  height: 1.5,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 24,
                                        width: 24,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 24,
                                            width: 24,
                                            child: Image.asset('assets/images/bell.png'),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 25),
                                            height: 5,
                                            width: 5,
                                            child: Image.asset('assets/images/middledot.png'),
                                          )
                                        ],
                                      )


                                    ],
                                  ),
                                ),
                              )
                            ],
                          )
                      ),
                      SizedBox(
                        height: 70,
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _buildStatistic("Pending Service","$pending New Task Added",Image.asset('assets/images/newtask.png'),0xffFFF3F1),
                          _buildStatistic("All Services","You have $acceptlist task",Image.asset('assets/images/alltask.png'),0xffE0F4F4),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _buildStatistic("Report","Your history is here",Image.asset('assets/images/statistics.png'),0xffD0E9F8),
                          _buildStatistic("My Profile","See all your project",Image.asset('assets/images/businessman.png'),0xffEFEEFF),
                        ],
                      ),
                      SizedBox(
                        height: 35,
                      ),
                    ],
                  ),
                ),
              ],
            )

        ),
      ),
      //backgroundColor: AppColors.backgroundColor,
    );
  }

  Widget _buildStatistic(String title, String subtitle, Image image, int color) {
    return GestureDetector(
      child: Container(
        height: 150,
        width: 156,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
          border: Border.all(color: Colors.white),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(1, 1),
              spreadRadius: 1,
              blurRadius: 1,
            ),
          ],
        ),
        //margin: EdgeInsets.symmetric(horizontal: 5),
        padding: EdgeInsets.all(10),
        child: Center(
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(height: 12),
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: Container(
                        height: 43,
                        width: 43,
                        decoration: BoxDecoration(
                          color: Color(color),
                          border: Border.all(color: Color(color)),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                              image: image.image,
                              scale: 2.6
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 7),
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: Text(
                        title,
                        style: TextStyle(
                          color: Color(0xff1E2661),
                          fontSize: 16,
                          height: 1.5,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Align(
                          alignment: AlignmentDirectional.topStart,
                          child: Text(
                            subtitle,
                            style: TextStyle(
                              color: Color(0xff878DBA),
                              fontSize: 14,
                              height: 1.5,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: (){
        if(title=="Pending Service"){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
              JobAdminPending()));
        }else if(title=="All Services"){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
              AllticketJobAdmin()));
        }else if(title=="Report"){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
              TeamMemberUserReports()));
        }else if(title=="My Profile"){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
              TechnicianProfile()));
        }
      },
    );
  }


}

