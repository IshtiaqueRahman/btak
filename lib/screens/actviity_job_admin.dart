import 'package:btakapps/utils/constants.dart';
import 'package:flutter/material.dart';

class Activity extends StatelessWidget {
  String title = 'DropDownButton';
  String val;


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff6C5DD3),
      child:    Expanded(
        child: Container(
          width: double.infinity,
          height: 1000,
          decoration: BoxDecoration(
              color: kShadowColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0)
              )
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                    child: ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                        color: Colors.grey,
                      ),
                      itemCount: 20,
                      itemBuilder: (context, index) => Padding(
                          padding: EdgeInsets.all(8.0),
                          child: buildTripCard(context, index)
                      ),
                    )
                ),
              )
            ],
          ),
          //child: Text('This is text'),
        ),
      )
    );
  }
}
Widget buildTripCard(BuildContext context, int index) {

//  final List<Trip> tripsList = [
//    Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
//    Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
//    Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
//    Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
//    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
//  ];

  //final trip = tripsList[index];

  return new Container(
    margin: EdgeInsets.only(left: 25,right: 15),
    child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'You moved task to In Progress.',
            style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: 10),
          Text(
            'Just now',
            style: TextStyle(
                fontSize: 13.0,
                color: Color(0xff3D4FF4),
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: 10),
          Image.asset(
            'assets/images/bg.png',
            height: 120,
            width: 150,
          )
        ],
      ),
    ),
  );

}