import 'dart:convert';

import 'package:btakapps/model/dashboardprofile.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_widget.dart';
import 'package:btakapps/utils/flutter_icons.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:btakapps/apiBase/base.dart';
import 'custombar.dart';

class UserDashboard extends StatefulWidget {
  String teammemberid;

  UserDashboard({this.teammemberid});

  @override
  _UserDashboardState createState() => _UserDashboardState(teammemberid);
}

class _UserDashboardState extends State<UserDashboard> {
  var image,name,department,ratings,pendingtask,ongointask,completetask,canceleedtask,totalratings;
  String teammemberid;

  _UserDashboardState(String teammemberid){
    this.teammemberid = teammemberid;
  }

  Future getList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    //var userid = pref.get("user_id");
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    print(teammemberid);

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "user_id":teammemberid,
      "office_id":officeId,
      "department_id":departmentId

    };

    print(body);
    final response =
    await http.post(BaseUrl.baseurl+"technician_task_report.json",body: json.encode(body));

    Map technicianListMap = jsonDecode(response.body);
    var dashboardprofile = DashboardProfile.fromJson(technicianListMap);

     setState(() {
       name = dashboardprofile.data[0].results.name;
       image = dashboardprofile.data[0].results.userProfile;
       department = dashboardprofile.data[0].results.departmentName;
       ratings = dashboardprofile.data[0].results.ratings;
       pendingtask = dashboardprofile.data[0].results.newPending;
       ongointask = dashboardprofile.data[0].results.onGoingTask;
       completetask = dashboardprofile.data[0].results.completeTasks;
       canceleedtask = dashboardprofile.data[0].results.refusedJob;
       totalratings = dashboardprofile.data[0].results.totalRatings;

       print(image);

     });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getList();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 220,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  padding: EdgeInsets.only(top: 25),
                ),
                Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomBarWidget(),
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                      ),
                      SizedBox(height: 25),
                      _buildStatistic(),
                      Padding(
                          padding: EdgeInsets.all(16),
                          child : Column(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      child: Text(
                                        "User Activity",
                                        style: TextStyle(
                                          color: Color(0xff1E2661),
                                          fontSize: 20,
                                          height: 1.5,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: _buildGender(FlutterIcons.male,
                                        0xffF2C94C, pendingtask ?? '', "Tasks Pending"),
                                  ),
                                  SizedBox(width: 16),
                                  Expanded(
                                    child: _buildGender(FlutterIcons.female,
                                       0xff8C7DF3, ongointask ?? '', "Tasks OnGoing"),
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 15,
                              ),

                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: _buildGender(FlutterIcons.male,
                                        0xff33BC6D, completetask ?? '', "Tasks Completed"),
                                  ),
                                  SizedBox(width: 16),
                                  Expanded(
                                    child: _buildGender(FlutterIcons.female,
                                        0xffFB5E5E, canceleedtask ?? '', "Tasks Cancelled"),
                                  ),
                                ],
                              ),
                            ],
                          )

                      ),
                    ],
                  ),
                ),


            Positioned(
              top: 90 ,//change this as needed
              height: 64,
              width: 64,
              left: 150,
              child:CircleAvatar(
                radius: 30.0,
                backgroundImage: (image == null) ? AssetImage('images/user-avatar.png') : NetworkImage(image),
                backgroundColor: Colors.transparent,
              )
            )
              ],
            )

        ),
      ),
      //backgroundColor: AppColors.backgroundColor,

    );
  }

  Widget _buildStatistic() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 24),
      padding: EdgeInsets.all(24),
      child: Center(
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(height: 20),
                Text(
                  name ?? '',
                  style: TextStyle(
                    color: Color(0xff1E2661),
                    fontSize: 20,
                    height: 1.5,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  department ?? '',
                  style: TextStyle(
                    color: Color(0xff1DBAC1),
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/images/star.png',
                      height: 14,
                      width: 14,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      ratings ?? '',
                      style: TextStyle(
                        color: Color(0xff1DBAC1),
                        fontSize: 16,
                        height: 1.5,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      totalratings ?? '',
                      style: TextStyle(
                        color: Color(0xff1E2661),
                        fontSize: 16,
                        height: 1.5,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGender(IconData icon, int color, String title, String value) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(color),
                      fontSize: 56,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 5,
                    color: Color(color),
                    width: 50,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    value,
                    style: TextStyle(
                      color: Color(0xff1E2661),
                      height: 1.5,
                      fontSize: 15
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),

        ],
      ),
    );
  }

}


