import 'dart:convert';

import 'package:btakapps/screens/report.dart';
import 'package:btakapps/screens/taskdetailsoverview_jobadmin.dart';
import 'package:btakapps/screens/technician_my_request.dart';
import 'package:btakapps/screens/user_dashboard.dart';
import 'package:btakapps/screens/userdashboard.dart';
import 'package:btakapps/screens/teammember_userreport.dart';
import 'package:btakapps/screens/request_sent.dart';
import 'package:btakapps/screens/forget_pass.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;


import 'allticketjobadmin.dart';
import 'job_admin_home.dart';
import 'job_admin_profile.dart';
import 'job_admin_profile1.dart';
import 'jobadmin_mytask.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:btakapps/apiBase/base.dart';

class LoginActivity extends StatefulWidget {

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginActivity> {


  bool _isLoading = false;
  TextEditingController username = new TextEditingController();
  TextEditingController pass = new TextEditingController();


  Future _login(String username,String pass) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    print(username+pass);

    Map<String, String> body = {
      'username': username,
      'password': pass,
    };

    final response =
    await http.post(BaseUrl.baseurl+"user_login.json",body: json.encode(body));

    print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    int status = jsonresp['data'][0]['status'];
    //print(token);

    var user = jsonresp['data'][0]['User'];

    //var jsons = jsonDecode(user);
    //var officeid = jsons['office_id'];
    print(user);
    //print(officeid);
    //pref.setString('userData', user);
    print(user['full_name']);

    pref.setString('user_id', user['id']);
    pref.setString('office_id', user['office_id']);
    pref.setString('department_id', user['department_id']);
    pref.setString('isjobadmin', user['is_jobadmin']);
    pref.setString('full_name', user['full_name']);

    if(status == 1){

      bool role = jsonresp['data'][0]['is_jobadmin'];

      if(role){
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
            HomePage()));
      }else{
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
        UserDash()));
      }

    }else{

      Navigator.pop(context);
      Flushbar(
        message:  "Login Failed",
        duration:  Duration(seconds: 3),
      )..show(context);
    }

//    DataUser duser = new DataUser.fromJson(jsonresp);
//
//    print('Status : ${duser.status}');
    //var status = datauser.message.toString();
    //print(datauser.message+"body"+response.body);




  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Color(0xffF6F7F8),
        body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                        color: Color(0xff3D4FF4),
                        child: Image.asset(
                            'assets/images/illustration.png'
                        )
                    ),
                    Container(
                      margin: EdgeInsets.only(top:300),
                      height: 420.0,
                      color: Colors.transparent,
                      child: new Container(
                          decoration: new BoxDecoration(
                              color: Color(0xffF6F7F8),
                              borderRadius: new BorderRadius.only(
                                topLeft: const Radius.circular(25.0),
                                topRight: const Radius.circular(25.0),
                              )
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 28,left: 35),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "B",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 36.0,
                                          fontFamily: 'Cera_Pro_Medium',
                                          color: Color(0xff6C5DD3)
                                      ),
                                    ),
                                    Text(
                                      "I",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 36.0,
                                          fontFamily: 'Cera_Pro_Medium',
                                          color: Color(0xff6C5DD3)
                                      ),
                                    ),
                                    Text(
                                      "T",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 36.0,
                                          fontFamily: 'Cera_Pro_Medium',
                                          color: Color(0xff2DCED6)
                                      ),
                                    ),
                                    Text(
                                      "A",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 36.0,
                                          fontFamily: 'Cera_Pro_Medium',
                                          color: Color(0xff2DCED6)
                                      ),
                                    ),
                                    Text(
                                      "C",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 36.0,
                                          fontFamily: 'Cera_Pro_Medium',
                                          color: Color(0xff2DCED6)
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5,left: 35),
                                child: Text(
                                  "Welcome to BITAC Service App",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 30.0,
                                      fontFamily: 'Cera_Pro_Medium',
                                      color: Color(0xff1E2661)
                                  ),
                                ),
                              ),

                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                  width: 320,
                                  padding: EdgeInsets.all(6.0),
                                  child: TextField(
                                    controller: username,
                                    autocorrect: true,
                                    decoration: InputDecoration(
                                      hintText: 'Username',
                                      hintStyle: TextStyle(color: Colors.grey),
                                      filled: true,
                                      fillColor: Color(0xffF8F8FD),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                                      ),
                                    ),)
                              ),
                              Container(
                                  width: 320,
                                  padding: EdgeInsets.all(6.0),
                                  child: TextField(
                                    controller: pass,
                                    obscureText: true,
                                    keyboardType: TextInputType.visiblePassword,
                                    decoration: InputDecoration(
                                      hintText: 'Password',
                                      hintStyle: TextStyle(color: Colors.grey),
                                      filled: true,
                                      fillColor: Color(0xffF8F8FD),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                                      ),
                                    ),)
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 20),
                                  child: GestureDetector(
                                    child: Text(
                                      "Forget Password",
                                      style: TextStyle(
                                          fontSize: 13.0,
                                          fontFamily: 'Cera_Pro_Medium',
                                          color: Color(0xff2DCED6)
                                      ),
                                    ),
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ForgetPassword()));
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              SizedBox(
                                width: 327,
                                height: 48,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Color(0xff2DCED6))
                                  ),
                                  onPressed: () {
                                    FocusScopeNode currentFocus = FocusScope.of(context);

                                    if (!currentFocus.hasPrimaryFocus) {
                                      currentFocus.unfocus();
                                    }

                                    setState(() {
                                      _login(username.text,pass.text);
                                      showLoaderDialog(context);
                                    });

                                  },
                                  color: Color(0xff2DCED6),
                                  textColor: Colors.white,
                                  child: Text("Login with btac account".toUpperCase(),
                                      style: TextStyle(fontSize: 14)),
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Dont have a account yet?",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Cera_Pro_Medium',
                                    color: Color(0xff878DBA)
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              GestureDetector(
                                child: Text(
                                  "Sign up with btac account",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      fontFamily: 'Cera_Pro_Medium',
                                      color: Color(0xff2DCED6)
                                  ),
                                ),
                                onTap: (){
//                                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
//                                      CreateNewTask1()));
                                },
                              ),

                            ],
                          )
                      ),
                    ),
                    Container(
                      height: 55,
                      width: 55,
                      margin: EdgeInsets.only(top: 270,left: 30),
                      child: Image.asset('assets/images/loginellipse.png'),
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

DataUser dataFromJson(String str) => DataUser.fromJson(json.decode(str));

String dataToJson(DataUser data) => json.encode(data.toJson());

class Login {
  List<DataUser> data;

  Login({this.data});

  Login.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<DataUser>();
      json['data'].forEach((v) {
        data.add(new DataUser.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataUser {
  int status;
  String message;
  bool isJobadmin;
  UserData user;

  DataUser({this.status, this.message, this.isJobadmin, this.user});

  DataUser.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    isJobadmin = json['is_jobadmin'];
    user = json['User'] != null ? new UserData.fromJson(json['User']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['is_jobadmin'] = this.isJobadmin;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}

class UserData {
  String id;
  String fullName;
  String userTypeId;
  String phone;
  String email;
  String isJobadmin;
  String officeId;
  String departmentId;

  UserData(
      {this.id,
        this.fullName,
        this.userTypeId,
        this.phone,
        this.email,
        this.isJobadmin,
        this.officeId,
        this.departmentId});

  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    userTypeId = json['user_type_id'];
    phone = json['phone'];
    email = json['email'];
    isJobadmin = json['is_jobadmin'];
    officeId = json['office_id'];
    departmentId = json['department_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['user_type_id'] = this.userTypeId;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['is_jobadmin'] = this.isJobadmin;
    data['office_id'] = this.officeId;
    data['department_id'] = this.departmentId;
    return data;
  }
}

showLoaderDialog(BuildContext context){
  AlertDialog alert=AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
        Container(margin: EdgeInsets.only(left: 7),child:Text("Loading..." )),
      ],),
  );
  showDialog(barrierDismissible: false,
    context:context,
    builder:(BuildContext context){
      return alert;
    },
  );
}

