import 'dart:async';
import 'package:flutter/material.dart';
import 'login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Splash Screen',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5),
            ()=>Navigator.pushReplacement(context,
            MaterialPageRoute(builder:
                (context) =>
                LoginActivity()
            )
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body : Stack(
          children: [
            Container(
                color: Color(0xff6C5DD3)),
            Container(
              //TODO: Step 1 - Add background.png to this Container as a background image.
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('assets/images/graphics.png'),
                      fit: BoxFit.cover,
                    )
                )
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Column(
                    children: [
                      Container(
                        height: 113.14,
                        width: 113.14,
                        child: Image.asset('assets/images/bitaclogo.png'),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Bitac",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 54.0,
                              fontFamily: 'Cera_Pro_Medium',
                              color: Colors.white
                          ),
                        ),
                      ),
                    ],
                  ),
                  //
                )
              ],
            )
          ],
        )
      // color: Colors.white,
      // child:FlutterLogo(size:MediaQuery.of(context).size.height)
    );
  }
}
class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text("GeeksForGeeks")),
      body: Center(
          child:Text("Home page",textScaleFactor: 2,)
      ),
    );
  }
}
