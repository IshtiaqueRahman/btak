import 'package:flutter/material.dart';

class CustomBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: [
                           Container(
                             margin: EdgeInsets.only(left: 100),
                             child: Text(
                               "Profile",
                               style: TextStyle(
                                 fontSize: 20,
                                 color: Colors.white,
                                 height: 1.5,
                               ),
                             ),
                           ),
                         ],
                ),

                Row(
                  children: <Widget>[
                    Container(
                      height: 24,
                      width: 24,
                      child: Image.asset('assets/images/bell.png'),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 25),
                      height: 5,
                      width: 5,
                      child: Image.asset('assets/images/middledot.png'),
                    )
                  ],
                )


              ],
            ),
          ),
        )
      ],
    );
  }
}

