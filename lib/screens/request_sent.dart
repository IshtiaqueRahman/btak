import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/requestsent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RequestSend extends StatefulWidget {
  String serviceid;


  RequestSend({this.serviceid});


  @override
  _RequestSendState createState() => _RequestSendState(serviceid);
}

class _RequestSendState extends State<RequestSend> {
  String serviceid;
  var status;
  var rate;

  _RequestSendState(String serviceid){
     this.serviceid = serviceid;
  }

  Future sentToRework() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var user_id = pref.get("user_id");

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "operational_type":2,
      "user_id" : user_id,
      "service_id" : serviceid,
      "rating": rate
    };

    print(body);
    final response =
    await http.post(BaseUrl.baseurl+"verify_user_service.json",body: json.encode(body));

    print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);
    Map reqMap = jsonDecode(response.body);
    var data = Request_Sent.fromJson(reqMap);

    status = data.data[0].status;
    _showToast(context);
    print(data.data[0].status);

  }

  Future markasComplete() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var user_id = pref.get("user_id");

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "operational_type":1,
      "user_id" : user_id,
      "service_id" : serviceid,
      "rating": rate
    };

    final response =
    await http.post(BaseUrl.baseurl+"verify_user_service.json",body: json.encode(body));

    print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);
    Map reqMap = jsonDecode(response.body);
    var data = Request_Sent.fromJson(reqMap);

    status = data.data[0].status;
    _showToastDone(context);
    print(data.data[0].status);

  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffF8F8FD),
        body: Stack(
          children: <Widget>[
                    //mainAxisAlignment: MainAxisAlignment.center,
                      Center(
                         child: Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                 image: AssetImage('assets/images/background.png'),
                                 fit: BoxFit.cover,
                              )
                            ),
                            width: 327,
                            height: 331,
                            child: Column(
                               children: <Widget>[
                                 SizedBox(
                                   height: 120,
                                 ),
                                 Text(
                                   "Service Done !",
                                   textAlign: TextAlign.center,
                                   style: TextStyle(
                                       fontSize: 24.0,
                                       fontFamily: 'Cera_Pro_Medium',
                                       color: Color(0xff1E2661)
                                   ),
                                 ),
                                 Container(
                                   margin: EdgeInsets.only(top: 10,left: 20,right: 20),
                                   child: Text(
                                     "Please Collect Your Product form your Delivery Adreess!",
                                     textAlign: TextAlign.center,
                                     style: TextStyle(
                                         fontSize: 16.0,
                                         fontFamily: 'Cera_Pro_Medium',
                                         color: Color(0xff878DBA)
                                     ),
                                   ),
                                 ),
                                 SizedBox(height: 20),
                                 Container(
                                   child: CustomPaint(
                                       painter: MyPainter(),
                                       child: Container(
                                         padding: EdgeInsets.all(10),
                                         child: RatingBar(
                                           itemSize: 30,
                                           initialRating: 3,
                                           direction: Axis.horizontal,
                                           allowHalfRating: true,
                                           itemCount: 5,
                                           ratingWidget: RatingWidget(
                                             full: Image.asset(
                                                 'assets/images/star.png',
                                                 height: 28,
                                                 width: 28,
                                             ),
                                             //half: Image.asset(''),
                                             empty: Image.asset(
                                                 'assets/images/disablestar.png',
                                                 height: 28,
                                                 width: 28,
                                             ),
                                           ),
                                           itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                           onRatingUpdate: (rating) {
                                             rate = rating;
                                             //print(rating);
                                           },
                                         ),
                                       )
                                   ),
                                 ),
                               ],
                            ),
                         ),
                       ),

                     Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                           Container(
                             margin: EdgeInsets.only(top: 100),
                             width: 280.84,
                             height: 280.08,
                             child: Container(
                               child: Image.asset('assets/images/image.png'),
                             ),
                           )
                        ],
                     ),

            Container(
              margin: EdgeInsets.only(bottom: 30),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                   mainAxisAlignment: MainAxisAlignment.end,
                   children: <Widget>[
                     SizedBox(
                       width: 327,
                       height: 48,
                       child: RaisedButton(
                         elevation: 0,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(18.0),
                             side: BorderSide(color: Color(0xff2DCED6))
                         ),
                         onPressed: () {
                             sentToRework();
                         },
                         color: Color(0xffFFFFFF),
                         textColor: Color(0xff2DCED6),
                         child: Text("Send To Rework".toUpperCase(),
                             style: TextStyle(fontSize: 14)),
                       ),
                     ),

                     SizedBox(
                        height: 10,
                     ),
                     SizedBox(
                       width: 327,
                       height: 48,
                       child: RaisedButton(
                         elevation: 0,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(18.0),
                             side: BorderSide(color: Color(0xff2DCED6))
                         ),
                         onPressed: () {
                           markasComplete();
                         },
                         color: Color(0xff2DCED6),
                         textColor: Colors.white,
                         child: Text("Mark As Complete".toUpperCase(),
                             style: TextStyle(fontSize: 14)),
                       ),
                     ),
                   ],
                ),
              ),
            )

                ],
            ),
       );
    }


  void _showToast(BuildContext context) {
    if(status==1){
      Fluttertoast.showToast(
        msg: "Sended to rework..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }

  void _showToastDone(BuildContext context) {
    if(status==1){
      Fluttertoast.showToast(
        msg: "Marked as complete..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }
}

const double _kRadius = 10;
const double _kBorderWidth = 0;

class MyPainter extends CustomPainter {
  MyPainter();

  @override
  void paint(Canvas canvas, Size size) {
    final rrectBorder = RRect.fromRectAndRadius(Offset.zero & size, Radius.circular(_kRadius));
    final rrectShadow = RRect.fromRectAndRadius(Offset(0, 0) & size, Radius.circular(_kRadius));

//    final shadowPaint = Paint()
//      ..strokeWidth = _kBorderWidth
//      ..color = Colors.black
//      ..style = PaintingStyle.stroke
//      ..maskFilter = MaskFilter.blur(BlurStyle.normal, 2);
    final borderPaint = Paint()
      ..strokeWidth = _kBorderWidth
      ..color = Color(0xffE0E0E0)
      ..style = PaintingStyle.stroke;

    //canvas.drawRRect(rrectShadow, shadowPaint);
    canvas.drawRRect(rrectBorder, borderPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
