import 'dart:io';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/change_password.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'create_new_task_technician2.dart';

class ChangePassword extends StatefulWidget {
  @override
  _CreateNewTaskState createState() => _CreateNewTaskState();
}

class _CreateNewTaskState extends State<ChangePassword> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1','list2','list3'];
  TextEditingController currentpass = new TextEditingController();
  TextEditingController confirmnewpass = new TextEditingController();
  TextEditingController newpassword = new TextEditingController();
  var status;
  File imageFile;

  Future savePassword(String currentpass,String newpass) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var user_id = pref.get("user_id");

    Map<String, dynamic> body = {
      "password":newpass,
      "reference_id":"BITAC-a8dcb57ae8c5",
      "current_password":currentpass,
      "user_id" : user_id
    };

    final response =
    await http.post(BaseUrl.baseurl+"user_password_change.json",body: json.encode(body));

    print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);
    Map taskListMap = jsonDecode(response.body);
    var data = Change_Password.fromJson(taskListMap);

    status = data.data[0].status;
    _showToast(context);
    print(data.data[0].status);

  }

  _openGallery(BuildContext context) async{
    var pictures = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.setState(() {
      imageFile = pictures;
    });
    Navigator.of(context).pop();
  }
  _openCamera(BuildContext context) async {
    var pictures = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState(() {
      imageFile = pictures;
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Change Password'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        margin: EdgeInsets.only(top: 25),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),
        child: SingleChildScrollView(
          child: Column(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Container(
                 margin: EdgeInsets.only(top: 15),
                 child: Row(
                   children: <Widget>[
                     Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 18,
                        width: 18,
                        child: Image.asset('assets/images/lock.png'),
                     ),
                     SizedBox(
                        width: 10,
                     ),
                     Text(
                       'Current Password',
                       style: TextStyle(
                         fontSize: 14,
                         color: Color(0xff1E2661),
                         fontFamily: 'DMSans-Bold',
                       ),
                     ),
                   ],
                 ),
               ),
               Container(
                   margin: EdgeInsets.only(top: 15,left: 15,right: 15),
                   width: 320,
                   height: 75,
                   padding: EdgeInsets.all(10.0),
                   child: TextField(
                     obscureText: true,
                     controller: currentpass,
                     autocorrect: true,
                     decoration: InputDecoration(
                       hintText: 'type current password',
                       hintStyle: TextStyle(color: Colors.grey),
                       filled: true,
                       fillColor: Color(0xffF8F8FD),
                       enabledBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                         borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                       ),
                       focusedBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                         borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                       ),
                     ),)
               ),


               Container(
                 margin: EdgeInsets.only(top: 15),
                 child: Row(
                   children: <Widget>[
                     Container(
                       margin: EdgeInsets.only(left: 10),
                       height: 18,
                       width: 18,
                       child: Image.asset('assets/images/lock.png'),
                     ),
                     SizedBox(
                       width: 10,
                     ),
                     Text(
                       'New Password',
                       style: TextStyle(
                         fontSize: 14,
                         color: Color(0xff1E2661),
                         fontFamily: 'DMSans-Bold',
                       ),
                     ),
                   ],
                 ),
               ),
               Container(
                   margin: EdgeInsets.only(top: 15,left: 15,right: 15),
                   width: 320,
                   height: 75,
                   padding: EdgeInsets.all(10.0),
                   child: TextField(
                     obscureText: true,
                     controller: newpassword,
                     autocorrect: true,
                     decoration: InputDecoration(
                       hintText: 'type new password',
                       hintStyle: TextStyle(color: Colors.grey),
                       filled: true,
                       fillColor: Color(0xffF8F8FD),
                       enabledBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                         borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                       ),
                       focusedBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                         borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                       ),
                     ),)
               ),


               Container(
                 margin: EdgeInsets.only(top: 15),
                 child: Row(
                   children: <Widget>[
                     Container(
                       margin: EdgeInsets.only(left: 10),
                       height: 18,
                       width: 18,
                       child: Image.asset('assets/images/lock.png'),
                     ),
                     SizedBox(
                       width: 10,
                     ),
                     Text(
                       'Confirm New Password',
                       style: TextStyle(
                         fontSize: 14,
                         color: Color(0xff1E2661),
                         fontFamily: 'DMSans-Bold',
                       ),
                     ),
                   ],
                 ),
               ),
               Container(
                   margin: EdgeInsets.only(top: 15,left: 15,right: 15),
                   width: 320,
                   height: 75,
                   padding: EdgeInsets.all(10.0),
                   child: TextField(
                     obscureText: true,
                     controller: confirmnewpass,
                     autocorrect: true,
                     decoration: InputDecoration(
                       hintText: 'type confirm new password',
                       hintStyle: TextStyle(color: Colors.grey),
                       filled: true,
                       fillColor: Color(0xffF8F8FD),
                       enabledBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                         borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                       ),
                       focusedBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                         borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                       ),
                     ),)
               ),

               Container(
                 margin: EdgeInsets.only(top: 35,left: 15,right: 15),
                 child: SizedBox(
                   width: 327,
                   height: 51,
                   child: RaisedButton(
                     elevation: 0,
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(15.0),
                         side: BorderSide(color: Color(0xff2DCED6))
                     ),
                     onPressed: () {
                        savePassword(currentpass.text,newpassword.text);
                     },
                     color: Color(0xff2DCED6),
                     textColor: Color(0xffFFFFFF),
                     child: Text("Submit".toUpperCase(),
                         style: TextStyle(fontSize: 14)),
                   ),
                 ),
               ),

             ],
          ),
        ),
      ),
      //body: ListShow(),
    );
  }

  _decideImage() {
    if(imageFile == null){
      return Text('No image selected');
    }else{
      return Expanded(
        child: Image.file(
          imageFile,
          width: 130,
          height: 102,
          fit: BoxFit.cover,
        ),
      );
    }
  }


  Widget getlistView(){
    int _currentTimeValue = 1;

    final _buttonOptions = [
      TimeValue(30,  "30 minutes"),
      TimeValue(60,  "1 hour"),
      TimeValue(120, "2 hours"),
      TimeValue(240, "4 hours"),
      TimeValue(480, "8 hours"),
      TimeValue(720, "12 hours"),
    ];

    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(12.0),
      children: _buttonOptions.map((timeValue) => RadioListTile(
        groupValue: _currentTimeValue,
        title: Text(timeValue._value),
        value: timeValue._key,
        onChanged: (val) {
        setState(() {
          debugPrint('VAL = $val');
          _currentTimeValue = val;
          }
          );
        },
      )).toList(),
    );
  }

  Widget getlistView1(){
    int _currentTimeValue = 1;

    final _buttonOptions = [
      TimeValue(30,  "30 minutes"),
      TimeValue(60,  "1 hour"),
      TimeValue(120, "2 hours"),
      TimeValue(240, "4 hours"),
      TimeValue(480, "8 hours"),
      TimeValue(880, "12 hours"),
      TimeValue(1620, "12 hours"),
      TimeValue(2420, "12 hours"),
      TimeValue(4800, "12 hours"),
    ];

    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(12.0),
      children: _buttonOptions.map((timeValue) => RadioListTile(
        groupValue: _currentTimeValue,
        title: Text(timeValue._value),
        value: timeValue._key,
        onChanged: (val) {
          setState(() {
            debugPrint('VAL = $val');
            _currentTimeValue = val;
          }
          );
        },
      )).toList(),
    );
  }

  void _showToast(BuildContext context) {
    if(status==1){
      Fluttertoast.showToast(
        msg: "Successfully changed password..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error changing password..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }
}



class TimeValue {
  final int _key;
  final String _value;
  TimeValue(this._key, this._value);
}




Widget chip(String label, Color color) {
  return Chip(
    labelPadding: EdgeInsets.all(3.0),
    avatar: Container(
      height: 5,
      width: 5,
      child: MaterialButton(
        onPressed: () {},
        color: Colors.blue,
        textColor: Colors.white,
        padding: EdgeInsets.all(15),
        shape: CircleBorder(),
      ),
    ),
    label: Text(
      label,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    backgroundColor: color,
    elevation: 6.0,
    shadowColor: Colors.grey[60],
    padding: EdgeInsets.all(6.0),
  );
}



