import 'dart:io';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/create_service.dart';
import 'package:btakapps/screens/technician_my_request.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:btakapps/model/prioritydata.dart';

import 'package:image_picker/image_picker.dart';

import 'create_new_task_technician1.dart';

class CreateNewTask2 extends StatefulWidget {
  final List<String> isChecked;
  final List<dynamic> listid;
  String serviceName,category;
  int deptid;

  CreateNewTask2({Key key, this.isChecked,this.listid,this.serviceName,this.deptid,this.category});

  @override
  _CreateNewTaskState createState() => _CreateNewTaskState(isChecked,listid,serviceName,deptid,category);
}

class _CreateNewTaskState extends State<CreateNewTask2> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1','list2','list3'];
  int selectedValue;
  List<String> isChecked = List();
  List<dynamic> listid;
  File imageFile;
  String serviceName,category;
  List<RadioModel> sampleData = new List<RadioModel>();
  List priorityName = List();
  var departmentid,categoryid;
  var priority;
  var status;
  int deptid;
  int _defaultChoiceIndex=0;
  TextEditingController comment = new TextEditingController();

  List<int> intArr = [0xFF26FF6673,0xFF263DA7F2,0xFF265972FF];
  List<int> inttextColor = [0xffFF6673,0xff3DA7F2,0xff5972FF];

  _CreateNewTaskState(List<String> isChecked,List<dynamic> listid,String serviceName,int deptid,String category){
     this.isChecked = isChecked;
     this.listid = listid;
     this.serviceName = serviceName;
     this.deptid = deptid;
     this.category = category;
     //print(serviceName);

  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getData();
    //print(isChecked);
    for(int i=0; i<isChecked.length; i++){
      sampleData.add(new RadioModel(false, isChecked[i]));
    }
//    sampleData.add(new RadioModel(false,'April 18'));
//    sampleData.add(new RadioModel(false,'April 17'));
//    sampleData.add(new RadioModel(false,'April 16'));
//    sampleData.add(new RadioModel(false,'April 15'));

  }

  Future getData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    //print(officeId);

    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      'office_id': officeId,
      'department_id': departmentId
    };

    final response =
    await http.get(BaseUrl.baseurl+"get_priority_type.json");

    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    Map userMap = jsonDecode(response.body);
    var priority = Priority.fromJson(userMap);

    //print(token);

    for(int i=0; i<response.body.length; i++){

      //print(priority.priorityTypes[i].priorityType.name);
//      if(i==0){
//        chip(priority.priorityTypes[0].priorityType.name, Color(0xFF26FF6673),Color(0xffFF6673));
//      }else if(i==1){
//        chip(priority.priorityTypes[1].priorityType.name, Color(0xFF263DA7F2),Color(0xff3DA7F2));
//      }else if(i==2){
//        chip(priority.priorityTypes[2].priorityType.name, Color(0xFF265972FF),Color(0xff5972FF));
//      }

      setState(() {
        priorityName.add(priority.priorityTypes[i].priorityType.name);
      });

      //print(name);
    }



//    DataUser duser = new DataUser.fromJson(jsonresp);
//
//    print('Status : ${duser.status}');
    //var status = datauser.message.toString();
    //print(datauser.message+"body"+response.body);




  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Create New Service'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: GestureDetector(
          child: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          onTap: (){
            isChecked.clear();
            Navigator.push(context, new MaterialPageRoute(builder: (context) => new CreateNewTask1()));

          },
        ),
      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        margin: EdgeInsets.only(top: 25),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),
        child: SingleChildScrollView(
          child: Column(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Container(
                      margin: EdgeInsets.only(left: 10),
                      height: 18,
                      width: 18,
                      child: Image.asset('assets/images/notes_icon.png'),
                   ),
                   SizedBox(
                      width: 10,
                   ),
                   Text(
                     'Service Name',
                     style: TextStyle(
                       fontSize: 14,
                       color: Color(0xff1E2661),
                       fontFamily: 'DMSans-Bold',
                     ),
                   ),
                 ],
               ),
               Container(
                   margin: EdgeInsets.only(top: 15,left: 35),
                   child: Wrap(
                     children: [
                       Text(
                          serviceName ?? '',
                          style: TextStyle(
                             fontSize: 16
                          ),
                       ),
                     ],
                   )
               ),

               SizedBox(
                 height: 30,
               ),

               Row(
                 children: <Widget>[
                   Container(
                     margin: EdgeInsets.only(left: 10),
                     height: 18,
                     width: 18,
                     child: Image.asset('assets/images/service.png'),
                   ),
                   SizedBox(
                     width: 10,
                   ),
                   Text(
                     'Service List',
                     style: TextStyle(
                       fontSize: 14,
                       color: Color(0xff1E2661),
                       fontFamily: 'DMSans-Bold',
                     ),
                   ),
                 ],
               ),

               SizedBox(
                 height: 15,
               ),

               Column(
                 children: [
                     ListView.builder(
                         shrinkWrap: true,
                         itemCount: sampleData.length,
                         itemBuilder: (BuildContext context, int index) {
                           return new InkWell(
                             //highlightColor: Colors.red,
                             splashColor: Colors.blueAccent,
                             onTap: () {
                               setState(() {
                                 sampleData.forEach((element) =>
                                 element.isSelected = false);
                                 sampleData[index].isSelected = true;
                               });
                             },
                             child: new RadioItem(sampleData[index]),
                           );
                         }
                   )
                   //getlistView()
                 ],
               ),

               SizedBox(
                 height: 20,
               ),

               Row(
                 children: <Widget>[
                   Container(
                     margin: EdgeInsets.only(left: 10),
                     height: 18,
                     width: 18,
                     child: Image.asset('assets/images/notes_icon.png'),
                   ),
                   SizedBox(
                     width: 10,
                   ),
                   Text(
                     'Overall Comments',
                     style: TextStyle(
                       fontSize: 14,
                       color: Color(0xff1E2661),
                       fontFamily: 'DMSans-Bold',
                     ),
                   ),
                 ],
               ),

               Container(
                 margin: EdgeInsets.only(left: 25,right: 25,top: 15),
                 child: TextField(
                   controller: comment,
                   minLines: 5,
                   maxLines: 8,
                   autocorrect: false,
                   decoration: InputDecoration(
                     hintText: 'Write your status here',
                     filled: true,
                     fillColor: Color(0xFF0D444444),
                     enabledBorder: OutlineInputBorder(
                       borderRadius: BorderRadius.all(Radius.circular(10.0)),
                       borderSide: BorderSide(color: Color(0xFF0D444444)),
                     ),
                     focusedBorder: OutlineInputBorder(
                       borderRadius: BorderRadius.all(Radius.circular(10.0)),
                       borderSide: BorderSide(color: Color(0xFF0D444444)),
                     ),
                   ),
                 ),
               ),

               SizedBox(
                 height: 20,
               ),

               Row(
                 children: <Widget>[
                   Container(
                     margin: EdgeInsets.only(left: 10),
                     height: 18,
                     width: 18,
                     child: Image.asset('assets/images/reminder_bell'
                         '.png'),
                   ),
                   SizedBox(
                     width: 10,
                   ),
                   Text(
                     'Set Priority',
                     style: TextStyle(
                       fontSize: 14,
                       color: Color(0xff1E2661),
                       fontFamily: 'DMSans-Bold',
                     ),
                   ),
                 ],
               ),

               SizedBox(
                 height: 22,
               ),

               Container(
                 height: 100,
                 child: Wrap(
                     spacing: 6.0,
                     runSpacing: 6.0,
                     children: <Widget>[
                       chip("", Color(0xFF26FF6673),Color(0xffFF6673)),
                       //chip(priorityName[1], Color(0xFF263DA7F2),Color(0xff3DA7F2)),
                       //chip(priorityName[0], Color(0xFF265972FF),Color(0xff5972FF)),
                     ]
                 ),
               ),

               SizedBox(
                 height: 22,
               ),

               Row(
                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                 children: <Widget>[
                   SizedBox(
                     width: 132,
                     height: 48,
                     child: RaisedButton(
                       elevation: 0,
                       shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(12.0),
                           side: BorderSide(color: Color(0xffD4D4D4))
                       ),
                       onPressed: () {
                         //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyRental()));
                       },
                       color: Color(0xffD4D4D4),
                       textColor: Color(0xffFFFFFF),
                       child: Text("Previous".toUpperCase(),
                           style: TextStyle(fontSize: 14)),
                     ),
                   ),
                   SizedBox(
                     width: 132,
                     height: 48,
                     child: RaisedButton(
                       elevation: 0,
                       shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(12.0),
                           side: BorderSide(color: Color(0xff2DCED6))
                       ),
                       onPressed: () {
                         setState(() {
                           saveData();

                         });

                         //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyRental()));
                       },
                       color: Color(0xff2DCED6),
                       textColor: Color(0xffFFFFFF),
                       child: Text("Confirm".toUpperCase(),
                           style: TextStyle(fontSize: 14)),
                     ),
                   ),
                 ],
               ),

               SizedBox(
                 height: 15,
               ),

             ],
          ),
        ),
      ),
      //body: ListShow(),
    );
  }

  Widget getlistView(){
    int _currentTimeValue = 1;

    final _buttonOptions = [
      TimeValue(30,  "30 minutes"),
      TimeValue(60,  "1 hour"),
      TimeValue(120, "2 hours"),
      TimeValue(240, "4 hours"),
      TimeValue(480, "8 hours"),
      TimeValue(720, "12 hours"),
    ];

    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(12.0),
      children: _buttonOptions.map((timeValue) => RadioListTile(
        groupValue: _currentTimeValue,
        title: Text(timeValue._value),
        value: timeValue._key,
          onChanged: _onChanged
      )).toList(),
    );
  }


  _onChanged(int value) {
    setState(() {
      selectedValue = value == selectedValue ? null : value;
    });
  }

  Future saveData() async{

    //print(_defaultChoiceIndex);

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var userid = pref.get("user_id");
    var departmentIdSelf = pref.get("department_id");

    if(category=="Ac"){
      categoryid = 1;
    }else{
      categoryid = 2;
    }
    if(serviceName=="Ac"){
       departmentid = 1;
    }else{
       departmentid = 2;
    }


    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "service_title" : serviceName,
      "bn_service_title" : "দুর্ভাগ্যক্রমে আমার ফ্যান আজ বন্ধ",
      "user_id" : userid,
      "office_id" : officeId,
      "department_id" : departmentIdSelf,
      "requisition_department_id" : departmentid,
      "priority_type_id" : _defaultChoiceIndex,
      "comments" : "Plese work immidiately",
      "service_category_id" : categoryid,
      "service_type" : listid,
      "other_comments" : comment.text.toString()
    };

    print(body);
    final response = await http.post(BaseUrl.baseurl+"new_service_create.json",body: json.encode(body));

    Map userMap = jsonDecode(response.body);
    var user = CreateService.fromJson(userMap);


    status = user.data[0].status;
    _showToast(context);



    print(status);
//    try {
//      for(int i=0; i<response.body.length; i++){
//            print(user.data[i].message);
//            print(user.data[i].status);
//          }
//    } catch (e) {
//      print(e);
//    } finally {}


    //print(response.body);

    final jsonresp = json.decode(response.body);
  }



  Widget chip(String label, Color color, Color textcolor) {
    print(label);
    if(priority!=null){
      return new Container();
    }else{
      return Row(
        children: [
          Container(
            height: 100,
            width: 280,
            child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: priorityName.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.only(left: 5,right: 5),
                        child: ChoiceChip(
                          avatar: Container(
                            height: 5,
                            width: 5,
                            child: MaterialButton(
                              onPressed: () {

                              },
                              color: Color(inttextColor[index]),
                              padding: EdgeInsets.all(15),
                            ),
                          ),
                          label: Text(priorityName[index], style: TextStyle(
                            color: Color(inttextColor[index]),
                          ),),
                          selected: _defaultChoiceIndex == index,
                          selectedColor: Color(inttextColor[index]).withOpacity(0.5),
                          onSelected: (bool selected) {
                            setState(() {
                              _defaultChoiceIndex = selected ? index : 0;
                              print(_defaultChoiceIndex);
                            });
                          },
                          backgroundColor: Color(intArr[index]),
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      );
                    },
                 ),
          ),
        ],
      );

//      return RawChip(
//        labelPadding: EdgeInsets.all(3.0),
//        avatar: Container(
//          height: 5,
//          width: 5,
//          child: MaterialButton(
//            onPressed: () {
//
//            },
//            color: textcolor,
//            padding: EdgeInsets.all(15),
//          ),
//        ),
//        label: Text(
//          label,
//          style: TextStyle(
//            color: textcolor,
//          ),
//        ),
//        backgroundColor: color,
//        padding: EdgeInsets.all(6.0),
//      );
    }

  }

  void _showToast(BuildContext context) {
    if(status==1){
      Fluttertoast.showToast(
          msg: "Successfully created service..",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
      );
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyARequest()));

    }else{
      Fluttertoast.showToast(
          msg: "Error creating service..",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
      );
    }

  }

}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Container(
        margin: EdgeInsets.only(left: 5,top: 10),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Container(
              height: 22.0,
              width: 22.0,
              child: Image.asset('assets/images/checkbox.png')
            ),
            new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: Expanded(
                child: new Text(
                  _item.text,
                  overflow: TextOverflow.clip,
                  maxLines: 2,
                   style: TextStyle(
                    fontSize: 14,
                ),),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String text;

  RadioModel(this.isSelected, this.text);
}


class TimeValue {
  final int _key;
  final String _value;
  TimeValue(this._key, this._value);
}







