import 'dart:convert';

import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/create_task.dart';
import 'package:btakapps/model/prioritydata.dart';
import 'package:btakapps/model/technician.dart';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/model/trip.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'create_new_task_technician.dart';

class TeamMemberUserReport extends StatefulWidget {

  String taskid,serviceid;

  TeamMemberUserReport({this.taskid,this.serviceid});


  @override
  _TeamMemberUserReportState createState() => _TeamMemberUserReportState(taskid,serviceid);
}

class _TeamMemberUserReportState extends State<TeamMemberUserReport> {
  List<TechnicianClass> technicianList = List();
  var _selectedDate;
  String taskid,serviceid;

  _TeamMemberUserReportState(String taskid, String serviceid){
     this.taskid = taskid;
     this.serviceid = serviceid;
  }


  @override
  void initState() {
    super.initState();
    getList();
  }



  Future getList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    print(userid);

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id" : officeId,
      "department_id" : departmentId

    };

    final response =
    await http.post(BaseUrl.baseurl+"technician_list.json",body: json.encode(body));

    Map technicianListMap = jsonDecode(response.body);
    var technicianListData = Technician_List.fromJson(technicianListMap);

    for(int i=0; i<technicianListData.data[0].results.length; i++){
      var id = technicianListData.data[0].results[i].technician.id;
      var name = technicianListData.data[0].results[i].technician.name;
      var designation = technicianListData.data[0].results[i].technician.designation;
      var jobinhand = technicianListData.data[0].results[i].technician.totalJobInHands;
      var userprofile = technicianListData.data[0].results[i].technician.userProfile;

      setState(() {
        technicianList.add(new TechnicianClass(id,name,designation,jobinhand,userprofile));
      });
    }

    print(technicianListData.data[0].results[0].technician.name);


  }

  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Team Member'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        margin: EdgeInsets.only(top: 25),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),
        child: Expanded(
          child: Container(
            child:  ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return new ExpandableListView(technicianList[index].id,technicianList[index].name,technicianList[index].designation,technicianList[index].jobinhand,technicianList[index].userprofile,taskid,serviceid);
              },
              itemCount: technicianList.length,
            ),
          ),
        ),
      ),
    );
  }
}


class ExpandableListView extends StatefulWidget {
  final String name,designation,jobinhand,userprofile,id;
  final String taskid,serviceid;


  //const ExpandableListView({Key key, this.name, this.designation, this.jobinhand}) : super(key: key);
  ExpandableListView(this.id,this.name, this.designation, this.jobinhand,this.userprofile,this.taskid,this.serviceid){}

  @override
  _ExpandableListViewState createState() => new _ExpandableListViewState(this.id,this.name,this.designation,this.jobinhand,this.userprofile,this.taskid,this.serviceid);
}


class _ExpandableListViewState extends State<ExpandableListView> {
  bool expandFlag = false;
  String id,name,designation,jobinhand,userprofile,taskid,serviceid;
  var _selectedDate,_selectedTime;
  List priorityName = List();
  var priority;
  var status;
  List<int> intArr = [0xFF26FF6673,0xFF263DA7F2,0xFF265972FF];
  List<int> inttextColor = [0xffFF6673,0xff3DA7F2,0xff5972FF];
  int _defaultChoiceIndex=0;
  TextEditingController instruction = new TextEditingController();
  var formattedTime;
  String date;

  String MIN_DATETIME = '2010-05-12';
  String MAX_DATETIME = '2021-11-25';
  String INIT_DATETIME = '2019-05-17';
  DateTime _dateTime;
  String _format = '"yyyy-MM-dd';
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;


  String MIN_TIME = '2010-05-12 10:47:00';
  String MAX_TIME = '2032-11-25 22:45:10';
  String INIT_TIME = '2021-01-11 18:13:15';
  DateTime _time;
  String _formattime = 'HH:mm';


  _ExpandableListViewState(String id,String name, String designation, String jobinhand,String userprofile, String taskid,String serviceid){
     this.id = id;
     this.name = name;
     this.designation = designation;
     this.jobinhand = jobinhand;
     this.userprofile = userprofile;
     this.taskid = taskid;
     this.serviceid = serviceid;
  }

  Future saveData(String id) async {

    var dateFormatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = dateFormatter.format(_dateTime);
    //print(_time);

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

    //print(_selectedDate+_selectedTime);

    String refid = "BITAC-a8dcb57ae8c5";
    String date = "2021-01-30 16:39";

    String formattedTime = DateFormat('kk:mm:a').format(_time);
    print(formattedTime);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "user_id": id,
      "task_id": taskid,
      "service_id" : serviceid,
      "delivery_date" : formattedDate+""+formattedTime,
      "priority_type_id" : _defaultChoiceIndex,
      "remark" : "",
      "description" : instruction.text.toString()
    };

    final response =
    await http.post(BaseUrl.baseurl+'task_assign.json',body: json.encode(body));

    print(body);
    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})

    Map taskListMap = jsonDecode(response.body);
    var taskList = Create_Task.fromJson(taskListMap);

    status = taskList.data[0].status;
    print(taskList.data[0].status);
    _showToast(context);
  }

  Future getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    //print(officeId);

    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      'office_id': officeId,
      'department_id': departmentId
    };

    final response =
    await http.get(BaseUrl.baseurl + "get_priority_type.json");

    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    Map userMap = jsonDecode(response.body);
    var priority = Priority.fromJson(userMap);

    //print(token);

    for (int i = 0; i < response.body.length; i++) {
      //print(priority.priorityTypes[i].priorityType.name);
//      if(i==0){
//        chip(priority.priorityTypes[0].priorityType.name, Color(0xFF26FF6673),Color(0xffFF6673));
//      }else if(i==1){
//        chip(priority.priorityTypes[1].priorityType.name, Color(0xFF263DA7F2),Color(0xff3DA7F2));
//      }else if(i==2){
//        chip(priority.priorityTypes[2].priorityType.name, Color(0xFF265972FF),Color(0xff5972FF));
//      }

      setState(() {
        priorityName.add(priority.priorityTypes[i].priorityType.name);
      });

      //print(name);
    }
  }


    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: new Container(
        margin: new EdgeInsets.symmetric(vertical: 1.0),
        child: new Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        height: 50.65,
                        width: 55.51,
                        child: CircleAvatar(
                          //backgroundColor: Colors.white,
                          backgroundImage: ExactAssetImage(userprofile),
                        ),
                      ),
                      Positioned(
                        left: 35,
                        child: Container(
                            margin: EdgeInsets.only(top: 60),
                            height: 20,
                            width: 20,
                            child: CircleAvatar(
                              backgroundColor: Color(0xff6F5EDA),
                              child: Text(
                                jobinhand,
                                style: TextStyle(
                                    color: Colors.white
                                ),
                              ),
                            )
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 15,top: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            child: Text(
                              name,
                              style: TextStyle(
                                fontSize: 16,
                                color: Color(0xff223E5C),
                                fontFamily: 'DMSans-Bold',
                              ),
                            ),
                          ),
                          GestureDetector(
                            child: Text(
                              designation,
                              style: TextStyle(
                                fontSize: 12,
                                color: Color(0xffA6B0BF),
                                fontFamily: 'DMSans-Bold',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Spacer(),
                Container(
                  margin: EdgeInsets.only(top: 25),
                  child: SizedBox(
                    width: 67,
                    height: 28,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Color(0xff2DCED6))
                      ),
                      onPressed: () {
                        print(id);
                        setState(() {

                          //saveData();
                          expandFlag = !expandFlag;
                          if(!expandFlag){
                            saveData(id);
                            //print('called');
                          }
                          //print(expandFlag);
                        });
                        //_addToDb();
                        //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskDetails()));
                      },
                      color: Color(0xff2DCED6),
                      textColor: Colors.white,
                      child: !expandFlag ? Text("Add".toUpperCase(), style: TextStyle(fontSize: 14)) : Text("Save".toUpperCase(), style: TextStyle(fontSize: 14)),
                    ),
                  ),
                ),
              ],
            ),
            Divider(),
            ExpandableContainer(
                expanded: expandFlag,
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 2),
                            height: 18,
                            width: 18,
                            child: Image.asset('assets/images/img.png'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Deadline',
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xff1E2661),
                              fontFamily: 'DMSans-Bold',
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Date',
                                style: TextStyle(
                                    color: Color(0xff1E2661)
                                ),
                              ),

                              Container(
                                  width: 130,
                                  height: 40,
                                  margin: EdgeInsets.only(top:10,right: 10),
                                  //padding: EdgeInsets.all(6.0),
                                  child: TextFormField(
                                    //controller: email,
                                    onTap: (){
                                      _showDatePicker();
                                      //_showDatePicker(context);
                                    },
                                    //initialValue: _dateTime!=null ? _dateTime.toString() : '',
                                    autocorrect: true,
                                    decoration: InputDecoration(
                                      hintText: _dateTime!=null ? _dateTime.toString() : '',
                                      contentPadding:
                                      EdgeInsets.all(0),
                                      prefixIcon: Icon(
                                        Icons.date_range,
                                        //color: _icon,
                                      ),
                                      hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
                                      filled: true,
                                      fillColor: Color(0xffF8F8FD),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                                      ),
                                    ),)
                              ),

                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 15),
                              width: 15,
                              height: 20,
                              child: Image.asset('assets/images/arrowrght.png')
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 7),
                                child: Text(
                                  'Time',
                                  style: TextStyle(
                                      color: Color(0xff1E2661)
                                  ),
                                ),
                              ),

                              Container(
                                  width: 130,
                                  height: 40,
                                  margin: EdgeInsets.only(top:10,left: 10),
                                  //padding: EdgeInsets.all(6.0),
                                  child: TextFormField(
                                    onTap: (){
                                      _showTimePicker();
                                    },
                                    //controller: email,
                                    autocorrect: true,
                                    decoration: InputDecoration(
                                      hintText: _time!=null ? _time.toString() : '',
                                      contentPadding:
                                      EdgeInsets.all(0),
                                      prefixIcon: Icon(
                                        Icons.access_alarm,
                                        //color: _icon,
                                      ),
                                      hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
                                      filled: true,
                                      fillColor: Color(0xffF8F8FD),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                                      ),
                                    ),)
                              ),

                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,

                      ),

                      Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 2),
                            height: 18,
                            width: 18,
                            child: Image.asset('assets/images/des.png'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Job Admin Instructor',
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xff1E2661),
                              fontFamily: 'DMSans-Bold',
                            ),
                          ),
                        ],
                      ),

                      SizedBox(
                        height: 8,
                      ),
                      TextField(
                        controller: instruction,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Job admin instruction'
                        ),
                      ),

                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            width: 23.94,
                            height: 23.50,
                            child: Image.asset('assets/images/reminderbell.png'),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              'Set priority',
                              style: TextStyle(
                                fontSize: 16,
                                color: Color(0xff1E2661),
                                fontFamily: 'DMSans-Bold',
                              ),
                            ),
                          ),
                        ],
                      ),
                      Wrap(
                          spacing: 2.0,
                          runSpacing: 2.0,
                          children: <Widget>[
                            chip(priorityName[0], Color(0xFF26FF6673),Color(0xffFF6673)),
                          ]
                      ),
                    ],
                  ),
                )
            )
          ],
        ),
      ),
    );
  }

  void _showDatePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.red)),
      ),
      minDateTime: DateTime.parse(MIN_DATETIME),
      maxDateTime: DateTime.parse(MAX_DATETIME),
      initialDateTime: _dateTime,
      dateFormat: _format,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
    );
  }

  void _showTimePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.red)),
      ),
      minDateTime: DateTime.parse(MIN_TIME),
      maxDateTime: DateTime.parse(MAX_TIME),
      initialDateTime: DateTime.parse(INIT_TIME),
      dateFormat: _formattime,
      pickerMode: DateTimePickerMode.time,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _time = dateTime;
          //print(_time);

        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _time = dateTime;
          _time = dateTime;
          formattedTime = DateFormat('kk:mm:a').format(_time);
          //print(_time);
        });
      },
    );
  }

  Widget chip(String label, Color color, Color textcolor) {
    print(label);
    if(priority!=null){
      return new Container();
    }else{
      return Row(
        children: [
          Container(
            height: 100,
            width: 280,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: priorityName.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  margin: EdgeInsets.only(left: 5,right: 5),
                  child: ChoiceChip(
                    avatar: Container(
                      height: 5,
                      width: 5,
                      child: MaterialButton(
                        onPressed: () {

                        },
                        color: Color(inttextColor[index]),
                        padding: EdgeInsets.all(15),
                      ),
                    ),
                    label: Text(priorityName[index], style: TextStyle(
                      color: Color(inttextColor[index]),
                    ),),
                    selected: _defaultChoiceIndex == index,
                    selectedColor: Color(inttextColor[index]).withOpacity(0.5),
                    onSelected: (bool selected) {
                      setState(() {
                        _defaultChoiceIndex = selected ? index : 0;
                        print(_defaultChoiceIndex);
                      });
                    },
                    backgroundColor: Color(intArr[index]),
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                );
              },
            ),
          ),
        ],
      );

//      return RawChip(
//        labelPadding: EdgeInsets.all(3.0),
//        avatar: Container(
//          height: 5,
//          width: 5,
//          child: MaterialButton(
//            onPressed: () {
//
//            },
//            color: textcolor,
//            padding: EdgeInsets.all(15),
//          ),
//        ),
//        label: Text(
//          label,
//          style: TextStyle(
//            color: textcolor,
//          ),
//        ),
//        backgroundColor: color,
//        padding: EdgeInsets.all(6.0),
//      );
    }

  }

  void _showToast(BuildContext context) {
    if(status==1){
      Fluttertoast.showToast(
        msg: "Successfully created service..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error creating service..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }
}

class ExpandableContainer extends StatelessWidget {
  final bool expanded;
  final double collapsedHeight;
  final double expandedHeight;
  final Widget child;

  ExpandableContainer({
    @required this.child,
    this.collapsedHeight = 0.0,
    this.expandedHeight = 350.0,
    this.expanded = true,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return new AnimatedContainer(
      duration: new Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      width: screenWidth,
      height: expanded ? expandedHeight : collapsedHeight,
      child: new Container(
        child: child,
        //decoration: new BoxDecoration(border: new Border.all(width: 1.0, color: Colors.blue)),
      ),
    );
  }
}