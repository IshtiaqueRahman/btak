import 'dart:io';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/db/dbhelper.dart';
import 'package:btakapps/model/change_status.dart';
import 'package:btakapps/model/item_list.dart';
import 'package:btakapps/model/taskdetails.dart';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/screens/team_member.dart';
import 'package:btakapps/screens/teammember.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'change_password.dart';
import 'chat_task_details.dart';

class TaskDetailsOverview extends StatelessWidget {
  String taskid;
  // This widget is the root of your application.

  TaskDetailsOverview({this.taskid});



  @override
  Widget build(BuildContext context) {
      return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Color(0xff3D4FF4),
          appBar: AppBar(
             title: Column(
               children: [
                 Text('Task Details'),
               ],
             ),
             backgroundColor: Color(0xff3D4FF4),
             elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(taskid),
      );
  }
}

class Body extends StatefulWidget {
  String taskid;

  Body(String taskid){
     this.taskid = taskid;
  }

  @override
  _BodyState createState() => _BodyState(taskid);
}

class _BodyState extends State<Body> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1', 'list2', 'list3'];
  File imageFile;
  DateTime _setDate = DateTime.now();
  Duration initialtimer = new Duration();
  int selectitem = 1;
  String taskid;
  List<Todo> taskList = new List();
  var assigndate, deadline, startdate, taskdescription, jobadmindescription,
      tasktitle, jobstatus_typeid,jobid,progress;
  List<RadioModel> sampleData = new List<RadioModel>();
  List priorityName = List();
  List<int> intArr = [0xFF26FF6673, 0xFF263DA7F2, 0xFF265972FF];
  List<int> inttextColor = [0xffFF6673, 0xff3DA7F2, 0xff5972FF];
  int _defaultChoiceIndex = 0;
  var priority, serviceid;
  var jobstatus;
  var status;
  List<Items> itemslist = List();
  TextEditingController refuseReason = new TextEditingController();
  Items text;
  var _selectedText;
  var priorityvalue;

  _BodyState(String taskid) {
    this.taskid = taskid;
  }


  Future getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      "task_id": taskid
    };

   // print(body);
    final response =
    await http.post(
        BaseUrl.baseurl + "get_task_by_id.json", body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var data = Task_Details.fromJson(taskListMap);
//
    setState(() {
      assigndate = data.data[0].results[0].job.assignedDate;
      final DateFormat formatter = DateFormat('dd-MM-yyyy');
      DateTime dt = DateTime.parse(assigndate);
      final String formatted = formatter.format(dt);

      assigndate = formatted;


      deadline = data.data[0].results[0].job.deliveryDate;
      final DateFormat formatterdeadline = DateFormat('dd-MM-yyyy');
      DateTime dtdeadline = DateTime.parse(deadline);
      final String formatteddeadline = formatterdeadline.format(dtdeadline);

      deadline = formatteddeadline;


      startdate = data.data[0].results[0].job.jobStartDate;
      final DateFormat formatterstart = DateFormat('dd-MM-yyyy');
      DateTime dtstart = DateTime.parse(startdate);
      final String formattedstart = formatterstart.format(dtstart);

      startdate = formattedstart;

      progress = data.data[0].results[0].job.progress;
      jobid = data.data[0].results[0].job.id;
      tasktitle = data.data[0].results[0].job.taskTitle;
      taskdescription = data.data[0].results[0].job.serviceDescription;
      jobadmindescription = data.data[0].results[0].job.jobAdminInstruction;
      jobstatus = data.data[0].results[0].job.jobStatus;
      jobstatus_typeid = data.data[0].results[0].job.jobStatusTypeId;
      serviceid = data.data[0].results[0].job.ticketId;
      priorityvalue = data.data[0].results[0].job.priority;
      //priorityName.add(data.data[0].results[0].job.priority);

      print(jobstatus_typeid);


      pref.setString('serviceid', serviceid);
      pref.setString('jobid', jobstatus_typeid);

      //department = data.data[0].result.departmentName;
      //image = data.data[0].result.img;
    });
  }

  Future StartJob() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      "operational_type": "2",
      "pre_status": pref.get("jobid").toString(),
      "post_status": "5",
      "id": taskid,
      "service_id": pref.get("serviceid").toString(),
      "user_id": userid,
      "remarks": ""
    };

    print(body);

    final response =
    await http.post(
        BaseUrl.baseurl + "change_status.json", body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var changestatus = Change_Status.fromJson(taskListMap);

    status = changestatus.data[0].status;
    _showToast(context);
    //print(status);
    //
    setState(() {
      //image = data.data[0].result.img;
    });
  }

  Future RejectJob() async {
   // print(refuseReason.text.toString());
    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      "operational_type": "2",
      "pre_status": jobstatus_typeid,
      "post_status": "4",
      "id": taskid,
      "service_id": serviceid,
      "user_id": userid,
      "remarks": refuseReason.text.toString()
    };

    //print(body);
    final response =
    await http.post(
        BaseUrl.baseurl + "change_status.json", body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var changestatus = Change_Status.fromJson(taskListMap);
    status = changestatus.data[0].status;

   // print(status);
    //
    _showToastForReject(context);
    setState(() {
      //image = data.data[0].result.img;
    });
  }

  Future<String>  getDropdownadata() async {
    //print(refuseReason.text.toString());

    final response =
    await http.get(BaseUrl.baseurl + "get_refused_reasons_list.json");

    Map taskListMap = jsonDecode(response.body);
    var items = ItemList.fromJson(taskListMap);
    var reason = items.refuseReasons[0].refuseReason.name;

    for(int i=0; i<items.refuseReasons.length; i++){
      setState(() {
        itemslist.add(new Items(items.refuseReasons[i].refuseReason.name));

      });
    }
    //print(reason);
    //
  }


  void _showToast(BuildContext context) {
    if (status == 1) {
      Fluttertoast.showToast(
        msg: "Successfully started job...",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    } else {
      Fluttertoast.showToast(
        msg: "Error starting job..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }
  }

  void _showToastForReject(BuildContext context) {
    if (status == 1) {
      Fluttertoast.showToast(
        msg: "Rejected the job...",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    } else {
      Fluttertoast.showToast(
        msg: "Error..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
    getDropdownadata();
  }


  @override
  Widget build(BuildContext context) {
    var height = MediaQuery
        .of(context)
        .size;

    DateTime _fromDate = DateTime.now();
    TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());

    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];


    return SingleChildScrollView(

      child: Container(
        color: Color(0xff3D4FF4),
        child: Column(
          children: [
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.15,
              margin: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Expanded(
                      child: Text(
                            tasktitle ?? '',
                            style: TextStyle(
                                color: Color(0xffFFFFFF),
                                fontSize: 24.0,
                                fontWeight: FontWeight.normal
                            ),
                          ),

                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 25),
              width: double.infinity,
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 1.05,
              decoration: BoxDecoration(
                  color: kShadowColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0)
                  )
              ),
              child: SizedBox(

                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Wrap(
                              children: [
                                new Container(
                                  child: new Image.asset(
                                    'assets/images/clock.png',
                                    height: 17.0,
                                    width: 17.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 25),
                                  child: Text(
                                    'Assign Date',
                                    style: TextStyle(
                                        color: Color(0xff878DBA),
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            GestureDetector(
                              child: Text(
                                assigndate ?? '',
                                style: TextStyle(
                                    color: Color(0xff1E2661),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                              onTap: () {
                                _bottomSheetMore(context);
                              },
                            ),
                          ],
                        )
                    ),


                    Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Wrap(
                              children: [
                                new Container(
                                  child: new Image.asset(
                                    'assets/images/clock.png',
                                    height: 17.0,
                                    width: 17.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 25),
                                  child: Text(
                                    'Deadline',
                                    style: TextStyle(
                                        color: Color(0xff878DBA),
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            GestureDetector(
                              child: Text(
                                deadline ?? '',
                                style: TextStyle(
                                    color: Color(0xff1E2661),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                              onTap: () {
                                _bottomSheetMore(context);
                              },
                            ),
                          ],
                        )
                    ),

                    Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Wrap(
                              children: [
                                new Container(
                                  child: new Image.asset(
                                    'assets/images/clock.png',
                                    height: 17.0,
                                    width: 17.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 25),
                                  child: Text(
                                    'Start Date',
                                    style: TextStyle(
                                        color: Color(0xff878DBA),
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            GestureDetector(
                              child: Text(
                                startdate ?? '',
                                style: TextStyle(
                                    color: Color(0xff1E2661),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                              onTap: () {
                                _bottomSheetMore(context);
                              },
                            ),
                          ],
                        )
                    ),

                    SizedBox(
                      height: 5,
                    ),

                    Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            child: new Image.asset(
                              'assets/images/description.png',
                              height: 17.0,
                              width: 17.0,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              'Task Description',
                              style: TextStyle(
                                  color: Color(0xff878DBA),
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 3, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            taskdescription ?? '',
                            style: TextStyle(
                                color: Color(0xff1E2661),
                                fontSize: 14.0,
                                fontWeight: FontWeight.normal
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            child: new Image.asset(
                              'assets/images/description.png',
                              height: 17.0,
                              width: 17.0,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              'Job Admin Description',
                              style: TextStyle(
                                  color: Color(0xff878DBA),
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 3, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                                child: Text(
                                  jobadmindescription ?? '',
                                  style: TextStyle(
                                      color: Color(0xff1E2661),
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.normal
                                  ),
                                ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 15,
                    ),

                    Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          width: 23.94,
                          height: 23.50,
                          child: Image.asset('assets/images/remind.png'),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Text(
                            'Task Priority',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xff878DBA),
                              fontFamily: 'DMSans-Bold',
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 100,
                          child: Wrap(
                              spacing: 6.0,
                              runSpacing: 6.0,
                              children: <Widget>[
                                chip(priorityvalue, Color(0xFF26FF6673), Color(0xffFF6673)),
                                //chip(priorityName[1], Color(0xFF263DA7F2),Color(0xff3DA7F2)),
                                //chip(priorityName[0], Color(0xFF265972FF),Color(0xff5972FF)),
                              ]
                          ),
                        )
                      ],
                    ),

                    SizedBox(
                      height: 2,
                    ),

                    Container(
                      margin: EdgeInsets.only(bottom: 50),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: SizedBox(
                              width: 131,
                              height: 50,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: BorderSide(color: Color(0xffEB5757))
                                ),
                                onPressed: () {
                                  showDialog<void>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text("Reason list"),
                                          content: new DropdownButton<Items>(
                                            hint: Text('Select Specific Department'),
                                            value: text,
                                            onChanged: (Items newValue) {
                                              setState(() {
                                                text = newValue;
                                              });
                                            },
                                            items: itemslist.map((Items user) {
                                              return new DropdownMenuItem<Items>(
                                                value: user,
                                                child: new Text(
                                                  user.itemname,
                                                  style: new TextStyle(color: Colors.black),
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text("Submit"),
                                              onPressed: () {
                                                //print(value);
                                                RejectJob();
                                              },
                                            ),
                                          ],
                                        );
                                      }
                                  );
                                  //_buildStatusDialog();
                                },
                                color: Color(0xffF7E9ED),
                                textColor: Color(0xffEB5757),
                                child: Text("Refuse".toUpperCase(),
                                    style: TextStyle(fontSize: 14)),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: SizedBox(
                              width: 170,
                              height: 50,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: BorderSide(color: Color(0xff2DCED6))
                                ),
                                onPressed: () {
                                  if (jobstatus == "Started") {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            ChatScreen(jobid:jobid,progress:progress,jobstatus_typeid:jobstatus_typeid,taskid:jobid,serviceid:serviceid)));
                                  } else {
                                    StartJob();
                                    //print(jobstatus_typeid);
                                    // Navigator.push(context, MaterialPageRoute(
                                    //     builder: (BuildContext context) =>
                                    //         ChatScreen(jobid:jobid,progress:progress,jobstatus_typeid:jobstatus_typeid,taskid:jobid,serviceid:serviceid)));
                                  }
                                },
                                color: Color(0xff2DCED6),
                                textColor: Colors.white,
                                child: Text(jobstatus == "Started" ? "Activity"
                                    .toUpperCase() : "Start".toUpperCase(),
                                    style: TextStyle(fontSize: 14)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )


//                          Flexible(
//                            child: Container(
//                              padding: EdgeInsets.zero,
//                              child: taskList.isEmpty
//                                  ? Container()
//                                  : ListView.builder(
//                                  scrollDirection: Axis.horizontal,
//                                  itemCount: taskList.length,
//                                  itemBuilder: (ctx, index) {
//                                if (index == taskList.length) return null;
//                                return Flexible(
//                                  child: Image.asset(
//                                      taskList[index].title,
//                                      height: 50,
//                                      width: 50,
//                                  ),
//                                );
//                                // return ListTile(
//                                //   title: Container(
//                                //       height: 50,
//                                //       width: 50,
//                                //       child: Image.asset(taskList[index].title)),
//                                // );
//                              }),
//                            ),
//                          ),
//                        Container(
//                        height: 0.2,
//                        color: Colors.grey,
//                      ),

                  ],
                ),
              ),
            ),
            //child: Text('This is text'),
          ],
        ),
      ),
    );
  }


  Widget chip(String labelvalue, Color color, Color textcolor) {
    int _selectedIndex;
    //print(labelvalue);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 100,
            width: 280,
            child: Chip(
              avatar: CircleAvatar(
                backgroundColor: Color(labelvalue == "Low" ? inttextColor[0] : labelvalue ==
                            "Medium" ? inttextColor[1] : inttextColor[2]),
              ),
              label: Text(labelvalue ?? '', style: TextStyle(
                      color: Color(labelvalue == "Low" ? inttextColor[0] : labelvalue ==
                          "Medium" ? inttextColor[1] : inttextColor[2]),
                    ),),
            )





//            child: ListView.builder(
//              shrinkWrap: true,
//              scrollDirection: Axis.horizontal,
//              itemCount: 1,
//              itemBuilder: (BuildContext context, int index) {
//                return Container(
//                  margin: EdgeInsets.only(left: 5, right: 5),
//                  child: ChoiceChip(
//                    avatar: Container(
//                      height: 5,
//                      width: 5,
//                      child: MaterialButton(
//                        onPressed: () {
//
//                        },
//                        color: Color(label == "Low" ? inttextColor[0] : label ==
//                            "Medium" ? inttextColor[1] : inttextColor[2]),
//                        padding: EdgeInsets.all(15),
//                      ),
//                    ),
//                    label: Text(priorityName[index] ?? '', style: TextStyle(
//                      color: Color(label == "Low" ? inttextColor[0] : label ==
//                          "Medium" ? inttextColor[1] : inttextColor[2]),
//                    ),),
//                    selected: _defaultChoiceIndex == index,
//                    //selectedColor: Color(inttextColor[index]).withOpacity(0.2),
//                    onSelected: (bool selected) {
//                      setState(() {
//                        _defaultChoiceIndex = selected ? index : 0;
//                        print(_defaultChoiceIndex);
//                      });
//                    },
//                    backgroundColor: Color(
//                        label == "Low" ? inttextColor[0] : label == "Medium"
//                            ? inttextColor[1]
//                            : inttextColor[2]),
//                    //labelStyle: TextStyle(color: Colors.white),
//                  ),
//                );
//              },
//            ),
          ),
        ],
      );

//      return RawChip(
//        labelPadding: EdgeInsets.all(3.0),
//        avatar: Container(
//          height: 5,
//          width: 5,
//          child: MaterialButton(
//            onPressed: () {
//
//            },
//            color: textcolor,
//            padding: EdgeInsets.all(15),
//          ),
//        ),
//        label: Text(
//          label,
//          style: TextStyle(
//            color: textcolor,
//          ),
//        ),
//        backgroundColor: color,
//        padding: EdgeInsets.all(6.0),
//      );

  }

  void _buildStatusDialog() {
    //String _selectedText = "SDD";
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Reason list"),
            content: new DropdownButton<Items>(
              hint: Text('Select Specific Department'),
              value: text,
              onChanged: (Items newValue) {
                setState(() {
                  text = newValue;
                });
              },
              items: itemslist.map((Items user) {
                return new DropdownMenuItem<Items>(
                  value: user,
                  child: new Text(
                    user.itemname,
                    style: new TextStyle(color: Colors.black),
                  ),
                );
              }).toList(),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Submit"),
                onPressed: () {
                   //print(value);
                   RejectJob();
                },
              ),
            ],
          );
        }
        );
    }
}


class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}




class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  bool _value = true;

  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Container(
        margin: EdgeInsets.only(left: 25,top: 10),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        Center(
        child: InkWell(
          child: Container(
          decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xff2DD8CF)),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: _value
                ? Icon(
              Icons.check,
              size: 13.0,
              color: Colors.white,
            )
                : Icon(
              Icons.check_box_outline_blank,
              size: 10.0,
              color: Color(0xff2DD8CF),
            ),
          ),
        ),
      )),

            new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(_item.text,style: TextStyle(
                  fontSize: 14
              ),),
            )
          ],
        ),
      ),
    );
  }
}

void _bottomSheetMore(BuildContext context) {
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}
class Items {
  final String itemname;

  Items(this.itemname);
}



class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}




