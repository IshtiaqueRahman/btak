import 'dart:convert';
import 'package:btakapps/screens/techinician_profile.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_edit_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';

import 'job_admin_profile.dart';


class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  bool _enabled = false;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Container(
            color: Color(0xffF8F8FD),
            child: Stack(
              children: <Widget>[
                Container(
                  height: 220,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                  ),
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: <Widget>[
                           Stack(
                             children: <Widget>[
                                   Container(
                                      margin: EdgeInsets.only(top: 30),
                                      height: 75,
                                      width: 75,
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                       // backgroundImage: ExactAssetImage('assets/images/background.png'),
                                      ),
                                    ),
                                    Positioned(
                                      top: 45,
                                      left: 50,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 30),
                                        height: 25,
                                        width: 25,
                                        child: CircleAvatar(
                                          backgroundImage: ExactAssetImage('assets/images/camera.png'),
                                        ),
                                      ),
                                    ),
                               ],
                           ),
                         ],
                       ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomAppBarEditWidget(),
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                      ),
                      SizedBox(height: 50),
                      _buildStatistic(),
                      Padding(
                          padding: EdgeInsets.all(16),
                          child : Column(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "Edit Profile",
                                        style: TextStyle(
                                          color: Color(0xff1E2661),
                                          fontSize: 20,
                                          height: 1.5,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 8,right: 8),
                                    height: 150.0,
                                    color: Colors.transparent,
                                    child: new Container(
                                        decoration: new BoxDecoration(
                                            color: Color(0xffFFFFFF),
                                            borderRadius: new BorderRadius.only(
                                              topLeft: const Radius.circular(12.0),
                                              topRight: const Radius.circular(12.0),
                                              bottomLeft: const Radius.circular(12.0),
                                              bottomRight: const Radius.circular(12.0)
                                            )
                                        ),

                                      child: ListView(
                                        physics: const NeverScrollableScrollPhysics(),
                                        padding: EdgeInsets.zero,
                                        children: ListTile.divideTiles( //          <-- ListTile.divideTiles
                                            context: context,
                                            tiles: [
                                              ListTile(
                                                selected:true,
                                                leading: Container(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset('assets/images/trailimage.png'),
                                                ),
                                                title: Text("Sim card status",
                                                  style: TextStyle(color: Color(0xff1E2661),fontSize: 18),
                                                ),
                                                subtitle: Text("Registered",
                                                  style: TextStyle(color: Color(0xff00D455), fontSize: 12),
                                                ),
                                                trailing: Container(
                                                  height: 12,
                                                  width: 10,
                                                  child: Image.asset('assets/images/right.png'),
                                                ),
                                                onTap: (){},
                                              ),
                                              ListTile(
                                                selected:true,
                                                leading: Container(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset('assets/images/trailimage.png'),
                                                ),
                                                title: Text("Roaming status",
                                                  style: TextStyle(color: Color(0xff1E2661),fontSize: 18),
                                                ),
                                                subtitle: Text("Active",
                                                  style: TextStyle(color: Color(0xff00D455), fontSize: 12),
                                                ),
                                                trailing: Container(
                                                  height: 12,
                                                  width: 10,
                                                  child: Image.asset('assets/images/right.png'),
                                                ),
                                                onTap: (){},
                                              ),

                                            ]
                                        ).toList(),
                                      ),

                                    ),
                                  ),

                                  SizedBox(
                                    height: 35,
                                  ),

                                  SizedBox(
                                    width: 327,
                                    height: 48,
                                    child: RaisedButton(
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(15.0),
                                          side: BorderSide(color: Color(0xff2DCED6))
                                      ),
                                      onPressed: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TechnicianProfile()));
                                      },
                                      color: Color(0xff2DCED6),
                                      textColor: Color(0xffFFFFFF),
                                      child: Text("Save Changes".toUpperCase(),
                                          style: TextStyle(fontSize: 14)),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ],
                          )

                      ),
                    ],
                  ),
                ),
              ],
            )

        ),
      ),
      //backgroundColor: AppColors.backgroundColor,

    );
  }

  Widget _buildStatistic() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 24),
      padding: EdgeInsets.all(10),
      child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(height: 5),
                Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Full Name",
                      style: TextStyle(
                        color: Color(0xff878DBA),
                        fontSize: 16,
                        height: 1.5,
                      ),
                    ),
                  ),
                ),
                Container(
                    width: 320,
                    height: 60,
                    padding: EdgeInsets.all(6.0),
                    child: TextField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Full Name',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Color(0xffF8F8FD),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                        ),
                      ),)
                ),
                SizedBox(height: 2),

                Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Email",
                      style: TextStyle(
                        color: Color(0xff878DBA),
                        fontSize: 16,
                        height: 1.5,
                      ),
                    ),
                  ),
                ),
                Container(
                    width: 320,
                    height: 60,
                    padding: EdgeInsets.all(6.0),
                    child: TextField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Email',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Color(0xffF8F8FD),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                        ),
                      ),)
                ),

                SizedBox(height: 2),

                Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Birthday",
                      style: TextStyle(
                        color: Color(0xff878DBA),
                        fontSize: 16,
                        height: 1.5,
                      ),
                    ),
                  ),
                ),
                Container(
                    width: 320,
                    height: 60,
                    padding: EdgeInsets.all(6.0),
                    child: TextField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        prefix: Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: Container(
                             alignment: Alignment.center,
                             height: 16,
                             width: 16,
                             decoration: BoxDecoration(
                                image: DecorationImage(
                                   image: AssetImage('assets/images/path.png'),
                                   fit: BoxFit.cover,
                                 ), //DecorationImage
                               ), //BoxDecoration
                            ),
                        ),
                        hintText: 'Birthday',
                        contentPadding: EdgeInsets.only(left: 15),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Color(0xffF8F8FD),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                        ),
                      ),)
                ),

                SizedBox(height: 10),
              ],
            ),
          ],
        ),
    );
  }
}


class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

extension ColorExtension on String {
  toColor() {
    var hexColor = this.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      return Color(int.parse("0x$hexColor"));
    }
  }
}