import 'package:btakapps/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class MyNotification extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
      return Scaffold(
          backgroundColor: kBlueColor,
          appBar: AppBar(
             title: Text('Notification'),
             backgroundColor: kBlueColor,
            elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(),
      );
  }
}

class Body extends StatelessWidget {
  Future<List<User>> _getUsers() async {
    List<User> users = [];
    var response = await http.get('https://api.randomuser.me/?results=20');

    var jsonData = jsonDecode(response.body);

    var usersData = jsonData["results"];

    for (var user in usersData) {
      User newUser = User(user["name"]["first"] + user["name"]["last"],
          user["email"], user["picture"]["large"], user["phone"]);

      users.add(newUser);
    }

    return users;
  }
  @override
  Widget build(BuildContext context) {
    return Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
          ),
           Expanded(
              child: Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30)
                   )
               ),

                child: Container(
                  child: FutureBuilder(
                     future: _getUsers(),
                     builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if(snapshot.data == null){
                          return Container(
                             child: Center(
                               child: Text('loading'),
                             ),
                          );
                        }else{
                           return ListView.builder(
                               itemCount: snapshot.data.length,
                               itemBuilder: (BuildContext context, int index){
                                  return ListTile(
                                    title: Text(snapshot.data[index].fullName),
                                    subtitle: Text(snapshot.data[index].mobileNumber),
                                    leading: Container(
                                      height: 34,
                                      width: 34,
                                      decoration: BoxDecoration(
                                          color: Colors.yellow,
                                          border: Border.all(color: Colors.yellow),
                                          borderRadius: BorderRadius.all(Radius.circular(8)),
                                          image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/ic_data.png'),
                                          fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                        //backgroundImage:
                                        //NetworkImage(snapshot.data[index].imageUrl)),
                                    trailing: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                         Text('July 25,2020'),
                                         Container(
                                            child: Text("In Progress", style: TextStyle(color: Colors.lightGreenAccent)),
                                         )
                                      ],

                                    ),
                                  );
                               }
                           );
                        }
                     }
                  ),
                )
             )
           ),
        ],
    );
  }
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}
