import 'dart:convert';

import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/addmessage.dart';
import 'package:btakapps/model/jobprogress.dart';
import 'package:btakapps/model/activity_list.dart';
import 'package:btakapps/model/message_model.dart';
import 'package:btakapps/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:btakapps/model/change_status.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatScreenJobAdmin extends StatefulWidget {
  final User user;
  String jobid,progress,jobstatus_typeid,taskid,serviceid;

  ChatScreenJobAdmin({this.user,this.jobid,this.progress,this.jobstatus_typeid,this.taskid,this.serviceid});

  @override
  _ChatScreenState createState() => _ChatScreenState(jobid,progress,jobstatus_typeid,taskid,serviceid);
}

class _ChatScreenState extends State<ChatScreenJobAdmin> {
  String jobid,user,progress,jobstatus_typeid,taskid,serviceid;
  var status;
  var currentSelectedValue;
  List<String> deviceTypes = ["Completed"];
  var _selectedText;
  var progressstatus;
  var isjobadmin;
  List<ChatList> list = List();
  TextEditingController progresstxt;
  TextEditingController message = new TextEditingController();


  _ChatScreenState(String jobid,String progress,String jobstatus_typeid,String taskid,String serviceid){
    this.jobid = jobid;
    this.progress = progress;
    this.jobstatus_typeid = jobstatus_typeid;
    this.taskid = taskid;
    this.serviceid = serviceid;
  }


  Future saveData() async{


    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "job_id":jobid,
      "progress": progresstxt.text.toString()
    };

    print(body);
    final response = await http.post(BaseUrl.baseurl+"job_progress.json",body: json.encode(body));

    Map userMap = jsonDecode(response.body);
    var progress = JobProgress.fromJson(userMap);


    progressstatus = progress.data[0].status;
    _showToast1(context);
    print(status);

  }
  Future ChangeStatus() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      "operational_type": "2",
      "pre_status": jobstatus_typeid,
      "post_status": "7",
      "id": taskid,
      "service_id": serviceid,
      "user_id": userid,
      "remarks": ""
    };

    print(body);
    final response =
    await http.post(
        BaseUrl.baseurl + "change_status.json", body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var changestatus = Change_Status.fromJson(taskListMap);
    status = changestatus.data[0].status;

    print(status);
    //
    _showToastForReject(context);
    setState(() {
      //image = data.data[0].result.img;
    });
  }

  Future sendData() async{

    //print(_defaultChoiceIndex);

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var userid = pref.get("user_id");
    var departmentIdSelf = pref.get("department_id");

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id" : officeId,
      "job_id" : jobid,
      "department_id": departmentIdSelf,
      "user_id" : userid,
      "message" : message.text.toString()
    };

    print(body);
    final response = await http.post(BaseUrl.baseurl+"add_task_activity.json",body: json.encode(body));

    Map userMap = jsonDecode(response.body);
    var messageMap = AddMessage.fromJson(userMap);


    status = messageMap.data[0].status;
    _showToast(context);



    print(status);
//    try {
//      for(int i=0; i<response.body.length; i++){
//            print(user.data[i].message);
//            print(user.data[i].status);
//          }
//    } catch (e) {
//      print(e);
//    } finally {}


    //print(response.body);

    final jsonresp = json.decode(response.body);
  }

  Future getList() async{

    list.clear();

    SharedPreferences pref = await SharedPreferences.getInstance();
    isjobadmin = pref.get("isjobadmin");

    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    print(isjobadmin);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id" : officeId,
      "job_id" : jobid,
      "department_id": departmentId
    };

    print(body);
    var response = await http.post(BaseUrl.baseurl+"get_task_activity_list.json",body: json.encode(body));
    Map taskListMap = jsonDecode(response.body);
    var activityLIst = ActivityList.fromJson(taskListMap);

    for(int i=0; i<activityLIst.data[0].results.length; i++){
       var outputDate = activityLIst.data[0].results[i].createdAt;

       var outputFormat = DateFormat('hh:mm a');
       var time = outputFormat.format(DateTime.parse(outputDate));

       var message = activityLIst.data[0].results[i].chatMessage;
       var sender = activityLIst.data[0].results[i].name;
       var jobadmincheck = activityLIst.data[0].results[i].isJobAdmin;

       setState(() {
         list.add(new ChatList(sender,time,message,jobadmincheck));
       });

    }

//
//
//    print(taskList.data[0].serviceDetails.taskList.length);
//
//    for(int i=0; i<taskList.data[0].serviceDetails.taskList.length; i++){
//
//      var id = taskList.data[0].serviceDetails.taskList[i].id;
//      var name = taskList.data[0].serviceDetails.taskList[i].technicianName;
//      var status = taskList.data[0].serviceDetails.taskList[i].jobStatus;
//
//      print(name);
//      setState(() {
//        tasks.add(new Tasks(id,name,status));
//      });
//    }
  }


  _buildMessage(ChatList chatList, bool isMe){
    final Container msg = Container(
      width: MediaQuery.of(context).size.width * 0.75,
      margin: isMe
          ? EdgeInsets.only(top: 7.0, bottom: 8.0, left: 80.0)
          : EdgeInsets.only(top:8.0, bottom: 8.0),
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
      decoration: isMe ? BoxDecoration(
          color: Color(0xff2DCED6),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              bottomLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0)
          )
      ) : BoxDecoration(
          color: Color(0xFF3D4FF4),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0)
          )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            chatList.sender,
            style: TextStyle(
                color: Color(0xffFFFFFF),
                fontSize: 13.0,
                fontWeight: FontWeight.w600
            ),
          ),
          SizedBox(height: 10.0,),

          SizedBox(height: 8.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Text(
                    chatList.text,
                    style: TextStyle(
                        color: Color(0xffFFFFFF),
                        fontSize: 13.0,
                        fontWeight: FontWeight.w600
                    )
                ),
              ),
              Text(
                chatList.time,
                style: TextStyle(
                    color: Color(0xffFFFFFF),
                    fontSize: 13.0,
                    fontWeight: FontWeight.w600
                ),
              ),
            ],
          ),
        ],
      ),
    );
    if (isMe){
      return msg;
    }
    return Row(
      children: <Widget>[
        msg,
      ],
    );
  }

  _buildMessageComposer() {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(10.0),
        child: TextFormField(
          controller: message,
          autocorrect: true,
          decoration: InputDecoration(
            hintText: 'Type Text Here...',
            suffixIcon: Container(
               margin: EdgeInsets.only(right: 10),
              child: GestureDetector(
                child: Image.asset(
                    'assets/images/message.png',
                    height: 30,
                    width: 30,
                ),
                onTap: (){
                   sendData();
                   getList();
                   //message.clear();
                   FocusScopeNode currentFocus = FocusScope.of(context);
                   if (!currentFocus.hasPrimaryFocus) {
                     currentFocus.unfocus();
                   }
                },
              ),
            ),
            hintStyle: TextStyle(color: Colors.grey),
            filled: true,
            fillColor: Colors.white70,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(color: Color(0xffD0D4E9), width: 2),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide(color: Color(0xffD0D4E9)),
            ),
          ),)
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progresstxt = new TextEditingController(text: progress);
    getList();

  }
  @override
  Widget build(BuildContext context) {
    var dropdownValue;
    return Scaffold(
      backgroundColor: Color(0xff396DED),
      appBar: AppBar(
        backgroundColor: Color(0xff396DED),
        title: Text(
          "Task Details",
          //widget.user.name,
          style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.normal
          ),
        ),
        centerTitle: true,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_horiz),
            iconSize: 30.0,
            color: Colors.white,
            onPressed: () {},
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 18),
              child: Text(
                "Fix my laptop display...",
                //widget.user.name,
                style: TextStyle(
                    color:  Colors.white,
                    fontSize: 24.0,
                    fontWeight: FontWeight.normal
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.only(left: 18,top: 10),
              child: Row(
                 mainAxisAlignment: MainAxisAlignment.start,
                 children: [
                    Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Text(
                           "Status",
                           //widget.user.name,
                           style: TextStyle(
                               color:  Color(0xff2DCED6),
                               fontSize: 14.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                         Container(
                           width: 130,
                           height: 40,
                           margin: EdgeInsets.only(top: 10),
                           padding: EdgeInsets.all(0),
                           decoration: BoxDecoration(
                               border: Border.all(color: Color(0xff6789F3)),
                               color: Colors.transparent,
                               borderRadius: BorderRadius.circular(10)),

                           // dropdown below..
                           child:  new DropdownButton<String>(
                             dropdownColor: Color(0xff6789F3),
                             underline: SizedBox(),
                             hint: Text("Status", style: TextStyle(color: Colors.white),),
                             value: _selectedText,
                             items: <String>['Complete']
                                 .map((String value) {
                               return new DropdownMenuItem<String>(
                                 value: value,
                                 child: new Text(value,style: TextStyle(color: Colors.white),),
                               );
                             }).toList(),
                             onChanged: (String val) {
                               _selectedText = val;
                               setState(() {
                                 _selectedText = val;
                                 if(_selectedText=="Complete"){
                                    print(progresstxt.text.toString());
                                    if(progresstxt.text.toString() == "100"){
                                      ChangeStatus();
                                      print('completed');
                                    }
                                 }

                               });
                             },
                           ),
                         )
                       ],
                    ),
                    SizedBox(
                       width: 15,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                     children: [
                       Text(
                         "Progress",
                         //widget.user.name,
                         style: TextStyle(
                             color:  Color(0xff2DCED6),
                             fontSize: 14.0,
                             fontWeight: FontWeight.normal
                         ),
                       ),
                       Container(
                         width: 130,
                         height: 40,
//                         margin: EdgeInsets.only(top: 10),
//                         padding: EdgeInsets.all(0),
//                         decoration: BoxDecoration(
//                             border: Border.all(color: Color(0xff6789F3)),
//                             borderRadius: BorderRadius.circular(10)),

                         // dropdown below..
                         child: TextField(
                           style: TextStyle(color: Colors.white),
                           cursorColor: Colors.white,
                           controller: progresstxt,
                           autocorrect: true,
                           decoration: InputDecoration(
                             hintStyle: TextStyle(color: Colors.white),
                             filled: true,
                             fillColor: Color(0xff6789F3),
                             enabledBorder: OutlineInputBorder(
                               borderRadius: BorderRadius.all(Radius.circular(10.0)),
                               borderSide: BorderSide(color: Color(0xff6789F3), width: 1),
                             ),
                             focusedBorder: OutlineInputBorder(
                               borderRadius: BorderRadius.all(Radius.circular(10.0)),
                               borderSide: BorderSide(color:  Color(0xff6789F3), width: 1),
                             ),
                           ),)
                       )
                     ],
                   )
                 ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: Container(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0)
                      )
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0)
                    ),
                    child: ListView.builder(
                        reverse: true,
                        padding: EdgeInsets.only(top: 14.0),
                        itemCount: list.length,
                        itemBuilder: (BuildContext context, int index) {
                          final ChatList chatlist = list[index];
                          bool isMe = false;
                          if(chatlist.jobadmincheck == "2"){
                            isMe = true;
                          }else{
                            isMe = false;
                          }
                          //final bool isMe = message.sender.id == currentUser.id;
                          return _buildMessage(chatlist, isMe);
                        }
                    ),
                  )
              ),
            ),
            _buildMessageComposer(),

            Container(
              color: Color(0xffF8F8FD),
              height: 72,
              child: Row(
                 children: [
                   Container(
                     margin: EdgeInsets.only(left: 20),
                     child: SizedBox(
                       width: 131,
                       height: 50,
                       child: RaisedButton(
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(18.0),
                             side: BorderSide(color: Color(0xffEB5757))
                         ),
                         onPressed: () {},
                         color: Color(0xffEB5757),
                         textColor: Colors.white,
                         child: Text("Cancel".toUpperCase(),
                             style: TextStyle(fontSize: 14)),
                       ),
                     ),
                   ),
                   Container(
                     margin: EdgeInsets.only(left: 10),
                     child: SizedBox(
                       width: 170,
                       height: 50,
                       child: RaisedButton(
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(18.0),
                             side: BorderSide(color: Color(0xff2DCED6))
                         ),
                         onPressed: () {
                            saveData();
                         },
                         color: Color(0xff2DCED6),
                         textColor: Colors.white,
                         child: Text("Save".toUpperCase(),
                             style: TextStyle(fontSize: 14)),
                       ),
                     ),
                   ),
                 ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showToast(BuildContext context) {
    if(status==1){
      message.clear();
      Fluttertoast.showToast(
        msg: "Message send succefull..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error sending message..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }

  void _showToast1(BuildContext context) {
    if(progressstatus==1){
      Fluttertoast.showToast(
        msg: "Progress added..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error adding progress..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }

  void _showToastForReject(BuildContext context) {
    if (status == 1) {
      Fluttertoast.showToast(
        msg: "Status changed...",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    } else {
      Fluttertoast.showToast(
        msg: "Error..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }
  }
}

class ChatList {
  final String sender;
  final String time;
  final String text;
  final String jobadmincheck;

  ChatList(this.sender, this.time, this.text, this.jobadmincheck);
}

