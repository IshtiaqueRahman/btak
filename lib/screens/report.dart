import 'dart:convert';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/db/dbhelper.dart';
import 'package:btakapps/model/reports.dart';
import 'package:btakapps/model/todo.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Report extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
      return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Color(0xff3D4FF4),
          appBar: AppBar(
             title: Column(
               children: [
                 Text('Report'),
               ],
             ),
             backgroundColor: Color(0xff3D4FF4),
             elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(),
      );
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String title = 'DropDownButton';
  String val;
  File imageFile;
  DateTime _setDate = DateTime.now();
  Duration initialtimer = new Duration();
  int selectitem = 1;
  List<Todo> taskList = new List();
  List<RadioModel> sampleData = new List<RadioModel>();
  var formattedTime;
  var dateTo,dateFrom;
  String date;
  var iscompleted;

  String MIN_DATETIME = '2010-05-12';
  String MAX_DATETIME = '2021-11-25';
  String INIT_DATETIME = '2019-05-17';
  DateTime _dateTime;
  String _format = '"yyyy-MM-dd';
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;


  String MIN_TIME = '2010-05-12 10:47:00';
  String MAX_TIME = '2032-11-25 22:45:10';
  String INIT_TIME = '2021-01-11 18:13:15';
  DateTime _time;
  String _formattime = 'HH:mm';

  bool _value = false;
  List<ReportModel> list = List();

  TextEditingController dateto = new TextEditingController();
  TextEditingController datefrom = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    sampleData.add(new RadioModel(false, 'A', 'April 18'));
    sampleData.add(new RadioModel(false, 'B', 'April 17'));
    sampleData.add(new RadioModel(false, 'C', 'April 16'));
    sampleData.add(new RadioModel(false, 'D', 'April 15'));

  }

  Future<List<ReportModel>> Search() async {
    list.clear();

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "date_form" : dateFrom,
      "date_to" : dateTo,
      "office_id":officeId,
      "department_id":departmentId,
      "is_completed_task" : iscompleted

    };

    print(body);
    var response = await http.post(BaseUrl.baseurl+"assign_task_status_report.json",body: json.encode(body));
    Map taskListMap = jsonDecode(response.body);
    var reportList = ReportList.fromJson(taskListMap);

    for(int i=0; i<reportList.data[0].results.length; i++){
      var name = reportList.data[0].results[i].tasks.name;
      var tasktitle = reportList.data[0].results[i].tasks.taskTitle;
      var jobstatus = reportList.data[0].results[i].tasks.jobStatus;

      setState(() {
        list.add(new ReportModel(name,tasktitle,jobstatus));
      });

      return list;
    }

}

  @override
  Widget build(BuildContext context) {


    var height = MediaQuery.of(context).size;

    DateTime _fromDate = DateTime.now();
    TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());
    
    // Future<void> _showDatePicker(BuildContext context) async {
    //   final picked = await showDatePicker(
    //     context: context,
    //     initialDate: _fromDate,
    //     firstDate: DateTime(2015, 1),
    //     lastDate: DateTime(2100),
    //   );
    //   if (picked != null && picked != _fromDate) {
    //     setState(() {
    //       _fromDate = picked;
    //     });
    //   }
    // }
    //
    // Future<void> _showTimePicker(BuildContext context) async {
    //   final picked = await showTimePicker(
    //     context: context,
    //     initialTime: _fromTime,
    //   );
    //   if (picked != null && picked != _fromTime) {
    //     setState(() {
    //       _fromTime = picked;
    //     });
    //   }
    // }

    Widget datetime() {
      return CupertinoDatePicker(
        initialDateTime: DateTime.now(),
        onDateTimeChanged: (DateTime newdate) {
          print(newdate);
        },
        use24hFormat: true,
        maximumDate: new DateTime(3500, 12, 30),
        minimumYear: 2020,
        maximumYear: 3500,
        minuteInterval: 1,
        mode: CupertinoDatePickerMode.dateAndTime,
      );
    }

    Widget time() {
      return CupertinoTimerPicker(
        mode: CupertinoTimerPickerMode.hms,
        minuteInterval: 1,
        secondInterval: 1,
        initialTimerDuration: initialtimer,
        onTimerDurationChanged: (Duration changedtimer) {
          setState(() {
            initialtimer = changedtimer;
          });
        },
      );
    }

    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];

    return SingleChildScrollView(

      child:  Container(
        color: Color(0xff3D4FF4),
        child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 1.4,
                  decoration: BoxDecoration(
                      color: Color(0xffFFFFFF),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0)
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 7),
                                    child: Text(
                                      'Start Date',
                                      style: TextStyle(
                                          color: Color(0xff1E2661)
                                      ),
                                    ),
                                  ),


                                  Container(
                                      width: 130,
                                      height: 40,
                                      margin: EdgeInsets.only(top:10,right: 10),
                                      //padding: EdgeInsets.all(6.0),
                                      child: TextFormField(
                                        controller: datefrom,
                                        onTap: (){
                                          _showFromDatePicker();
                                          //_showDatePicker(context);
                                        },
                                        //initialValue: _dateTime!=null ? _dateTime.toString() : '',
                                        autocorrect: true,
                                        decoration: InputDecoration(
                                          hintText: dateFrom!=null ? dateFrom.toString() : 'Start Date',
                                          contentPadding:
                                          EdgeInsets.all(0),
                                          prefixIcon: Icon(
                                            Icons.date_range,
                                            //color: _icon,
                                          ),
                                          hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
                                          filled: true,
                                          fillColor: Color(0xffF8F8FD),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                            borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                            borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                                          ),
                                        ),)
                                  ),

                                ],
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 15),
                                  width: 15,
                                  height: 20,
                                  child: Image.asset('assets/images/arrowrght.png')
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 7),
                                    child: Text(
                                      'End Date',
                                      style: TextStyle(
                                          color: Color(0xff1E2661)
                                      ),
                                    ),
                                  ),

                                  Container(
                                      width: 130,
                                      height: 40,
                                      margin: EdgeInsets.only(top:10,right: 10),
                                      //padding: EdgeInsets.all(6.0),
                                      child: TextFormField(
                                        controller: dateto,
                                        onTap: (){
                                          _showToDatePicker();
                                          //_showDatePicker(context);
                                        },
                                        //initialValue: _dateTime!=null ? _dateTime.toString() : '',
                                        autocorrect: true,
                                        decoration: InputDecoration(
                                          hintText: dateTo!=null ? dateTo.toString() : 'End Date',
                                          contentPadding:
                                          EdgeInsets.all(0),
                                          prefixIcon: Icon(
                                            Icons.date_range,
                                            //color: _icon,
                                          ),
                                          hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
                                          filled: true,
                                          fillColor: Color(0xffF8F8FD),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                            borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                            borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                                          ),
                                        ),)
                                  ),

                                ],
                              ),
                            ],
                          ),
                        ),

                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _value = !_value;
                                            if(_value){
                                               iscompleted = "1";
                                            }else{
                                              iscompleted = "2";
                                            }
                                            print(_value);
                                          });
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xffFFFFFF), border: Border.all(color: Color(0xffDEDEDE))),
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: _value
                                                ? Icon(
                                              Icons.check,
                                              size: 15.0,
                                              color: Colors.blue,
                                            )
                                                : Icon(
                                              Icons.check_box_outline_blank,
                                              size: 5.0,
                                              color: Color(0xffFFFFFF),
                                            ),
                                          ),
                                        )),
                              ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Text(
                            'Completed withing deadline',
                            style: TextStyle(
                                color: Color(0xff1E2661)
                            ),
                          ),
                        ),
                      ],
                    ),

                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          width: 327,
                          height: 48,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Color(0xff2DCED6))
                            ),
                            onPressed: () {
                               Search();
                            },
                            color: Color(0xff2DCED6),
                            textColor: Colors.white,
                            child: Text("Search".toUpperCase(),
                                style: TextStyle(fontSize: 14)),
                          ),
                        ),

                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          height: 45,
                          color: Color(0xffF0F0F0),
                          child: Row(
                            children: [
                               Expanded(
                                 flex: 3,
                                 child: Container(
                                   margin: EdgeInsets.only(left: 25),
                                   child: Text(
                                     'Sl',
                                     style: TextStyle(
                                         color: Color(0xff878DBA),
                                         fontSize: 16.0,
                                         fontWeight: FontWeight.normal
                                     ),
                                   ),
                                 ),
                               ),

                               Expanded(
                                 flex: 5,
                                 child: Text(
                                   'Technician',
                                   style: TextStyle(
                                       color: Color(0xff878DBA),
                                       fontSize: 16.0,
                                       fontWeight: FontWeight.normal
                                   ),
                                 ),
                               ),

                               Expanded(
                                 flex: 4,
                                 child: Text(
                                   'Status',
                                   style: TextStyle(
                                       color: Color(0xff878DBA),
                                       fontSize: 16.0,
                                       fontWeight: FontWeight.normal
                                   ),
                                 ),
                               ),

                             ],
                          ),
                        ),

                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: list.length,
                      itemBuilder: (context, index) {
                        var pos = index + 1;
                        return ListTile(
                          title: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(left: 8),
                                      child:  Text(pos.toString())
                                  ),

                                  Container(
                                          margin: EdgeInsets.only(left: 50),
                                          child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: [
                                                              Text(
                                                                list[index].fullName ?? '',
                                                                maxLines: 4,
                                                                overflow: TextOverflow.ellipsis,
                                                              ),
                                                            ],
                                                          ),

                                                             Container(
                                                               width: 130,
                                                               child: Row(
                                                                     mainAxisAlignment: MainAxisAlignment.start,
                                                                     children: [
                                                                       Expanded(
                                                                         child: Text(
                                                                                 list[index].tasktitle ?? '',
                                                                                 maxLines: 5,
                                                                               ),
                                                                       ),
                                                                         ]

                                                                       ),
                                                             ),
                                                                  ]

                                                               ),

                                        ),


                                  Container(
                                    child:  RaisedButton(
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(5.0),
                                          side: BorderSide(color: list[index].jobstatus == "Pending" ? Color(0xffEB5757) : list[index].jobstatus == "Completed" ? Color(0xff27AE60) : Color(0xffA880E3))),
                                      onPressed: () {},
                                      color: list[index].jobstatus == "Pending" ? Color(0xffFCE6E6) : list[index].jobstatus == "Completed" ? Color(0xffDFF3E7) : Color(0xffF2ECFB),
                                      textColor: list[index].jobstatus == "Pending" ? Color(0xffEB5757) : list[index].jobstatus == "Completed" ? Color(0xff27AE60) : Color(0xffA880E3),
                                      child: Text(list[index].jobstatus.toUpperCase(), style: TextStyle(fontSize: 11)),
                                    ),
                                  ),
                                ],
                              ),


                            ],
                          ),
                        );
                         },
                       )
                      ],
                    ),
                  )
                ),
                //child: Text('This is text'),
               ],
            ),
         ),
      );
  }

  void _showToDatePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.red)),
      ),
      minDateTime: DateTime.parse(MIN_DATETIME),
      maxDateTime: DateTime.parse(MAX_DATETIME),
      initialDateTime: _dateTime,
      dateFormat: _format,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
          DateFormat formatter = DateFormat('yyyy-MM-dd');
          dateTo = formatter.format(_dateTime);
        });
      },
    );
  }

  void _showFromDatePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.red)),
      ),
      minDateTime: DateTime.parse(MIN_DATETIME),
      maxDateTime: DateTime.parse(MAX_DATETIME),
      initialDateTime: _dateTime,
      dateFormat: _format,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
          DateFormat formatter = DateFormat('yyyy-MM-dd');
          dateFrom = formatter.format(_dateTime);
        });
      },
    );
  }

  _decideImage() {
    if(imageFile == null){
      return Text('No image selected');
    }else{
      return Expanded(
        child: Image.file(
          imageFile,
          width: 130,
          height: 102,
          fit: BoxFit.cover,
        ),
      );
    }
  }

   _pickDateDialog(BuildContext context) {
    DateTime _selectedDate;
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        //which date will display when user open the picker
        firstDate: DateTime(1950),
        //what will be the previous supported year in picker
        lastDate: DateTime
            .now()) //what will be the up to supported date in picker
        .then((pickedDate) {
      //then usually do the future job
      if (pickedDate == null) {
        //if user tap cancel then this function will stop
        return;
      }
      setState(() {
        //for rebuilding the ui
        _selectedDate = pickedDate;
      });
    });
  }

  Widget _myListView(BuildContext context) {
    return Wrap(
      children: [
        Container(
          margin: EdgeInsets.only(left: 35),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text('Sun'),
              ),
              ListTile(
                title: Text('Moon'),
              ),
              ListTile(
                title: Text('Star'),
              ),
            ],
          ),
        ),
      ],
    );
  }



}

class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  bool _value = true;

  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Container(
        margin: EdgeInsets.only(left: 25,top: 10),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
              children: [
                InkWell(
                child: Container(
                decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xff2DD8CF)),
                child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: _value
                    ? Icon(
                  Icons.check,
                  size: 5.0,
                  color: Colors.white,
                )
                    : Icon(
                  Icons.check_box_outline_blank,
                  size: 5.0,
                  color: Color(0xff2DD8CF),
                ),
          ),
              )),
              ],
            ),

            new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(_item.text,style: TextStyle(
                  fontSize: 14
              ),),
            )
          ],
        ),
      ),
    );
  }
}

void _bottomSheetMore(BuildContext context) {
}

class ReportModel {
  final String fullName;

  final String tasktitle;

  final String jobstatus;

  ReportModel(this.fullName, this.tasktitle, this.jobstatus);
}

class UserDetailPage extends StatelessWidget {
  final ReportModel user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}




