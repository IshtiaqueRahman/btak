import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff6C5DD3),
      appBar: AppBar(
        title: Text('Notification'),
        backgroundColor: Color(0xff6C5DD3),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),

      body: Container(
         margin: EdgeInsets.only(top: 25),
         padding: EdgeInsets.all(20),
         decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),

        child: ListShow(),
      ),
      //body: ListShow(),
    );
  }
}

class ListShow extends StatefulWidget {
  static String tag = 'contactlist-page';

  @override  State<StatefulWidget> createState() {
    return new _ContactsListState();
  }
}

List<Contact> contacts = [
  Contact(fullName: 'Pratap Kumar', email: 'pratap@example.com'),
  Contact(fullName: 'Jagadeesh', email: 'Jagadeesh@example.com'),
  Contact(fullName: 'Srinivas', email: 'Srinivas@example.com'),
  Contact(fullName: 'Narendra', email: 'Narendra@example.com'),
  Contact(fullName: 'Sravan ', email: 'Sravan@example.com'),
  Contact(fullName: 'Ranganadh', email: 'Ranganadh@example.com'),
  Contact(fullName: 'Karthik', email: 'Karthik@example.com'),
  Contact(fullName: 'Saranya', email: 'Saranya@example.com'),
  Contact(fullName: 'Mahesh', email: 'Mahesh@example.com'),
];
class _ContactsListState extends State<ListShow> {
  TextEditingController searchController = new TextEditingController();
  String filter;

  @override  initState() {
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
  }

  @override  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool isButtonPressed = false;
    return Scaffold(
        backgroundColor: Colors.white,
        body: new Column(
          children: <Widget>[
            SizedBox(
              height: 10.0,
            ),

               Row(
                 children: <Widget>[
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          side: BorderSide(color: isButtonPressed ? Color(0xffD5D6DB) : Color(0xff2DCED6))),
                      onPressed: () {
                        setState(() {
                          isButtonPressed = !isButtonPressed;
                          print(isButtonPressed);
                        });
                      },
                      color: isButtonPressed ? Color(0xffFFFFFF) : Color(0xff2DCED6),
                      elevation: 0,
                      textColor: isButtonPressed ? Color(0xff878DBA) : Colors.white,
                      child: Text("All".toUpperCase(),
                          style: TextStyle(fontSize: 14)),
                    ),
                    SizedBox(
                      width: 7,
                    ),
                   RaisedButton(
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(8.0),
                         side: BorderSide(color: isButtonPressed ? Color(0xffD5D6DB) : Color(0xff2DCED6))),
                     onPressed: () {
                       setState(() {
                         isButtonPressed =!isButtonPressed;
                       });
                     },
                     color: isButtonPressed ? Color(0xffFFFFFF) : Color(0xff2DCED6),
                     elevation: 0,
                     textColor: isButtonPressed ? Color(0xff878DBA) : Colors.white,
                     child: Text("Request".toUpperCase(),
                         style: TextStyle(fontSize: 14)),
                   ),
                   SizedBox(
                     width: 7,
                   ),
                   RaisedButton(
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(8.0),
                         side: BorderSide(color: isButtonPressed ? Color(0xffD5D6DB) : Color(0xff2DCED6))),
                     onPressed: () {
                       setState(() {
                         isButtonPressed =!isButtonPressed;
                       });
                     },
                     color: isButtonPressed ? Color(0xffFFFFFF) : Color(0xff2DCED6),
                     elevation: 0,
                     textColor: isButtonPressed ? Color(0xff878DBA) : Colors.white,
                     child: Text("My Jobs".toUpperCase(),
                         style: TextStyle(fontSize: 14)),
                   ),
                  ],
               ),
//            RaisedButton(
//              onPressed: (){
//                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => RequestSend()));
//              },
//              child: Text("Custom Dialog"),
//
//            ),
            Expanded(
                 child: FutureBuilder(
                      future: _getUsers(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if(snapshot.data == null){
                          return Container(
                            child: Center(
                              child: Text('loading'),
                            ),
                          );
                        }else{
                          return ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, int index){
                                return Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(snapshot.data[index].fullName),
                                      subtitle: Text(snapshot.data[index].mobileNumber),
                                      leading: Container(
                                        height: 34,
                                        width: 34,
                                        decoration: BoxDecoration(
                                          color: Colors.yellow,
                                          border: Border.all(color: Colors.yellow),
                                          borderRadius: BorderRadius.all(Radius.circular(8)),
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/ic_data.png'),
                                              fit: BoxFit.contain
                                          ),
                                        ),
                                      ),
                                      //backgroundImage:
                                      //NetworkImage(snapshot.data[index].imageUrl)),
                                      trailing : Container(
                                         height: 11,
                                         width: 10,
                                         child: Image.asset('assets/images/right.png')
                                      )

                                    ),
                                  ],
                                );
                              }
                          );
                        }
                      }
                  ),

            ),
          ],
        ));
  }

  void _onTapItem(BuildContext context, Contact post) {
    Scaffold.of(context).showSnackBar(
        new SnackBar(content: new Text("Tap on " + ' - ' + post.fullName)));
  }

  Future<List<User>> _getUsers() async {
    List<User> users = [];
    var response = await http.get('https://api.randomuser.me/?results=20');

    var jsonData = jsonDecode(response.body);

    var usersData = jsonData["results"];

    for (var user in usersData) {
      User newUser = User(user["name"]["first"] + user["name"]["last"],
          user["email"], user["picture"]["large"], user["phone"]);

      users.add(newUser);
    }

    return users;
  }
}


class Contact {
  final String fullName;
  final String email;

  const Contact({this.fullName, this.email});
}


class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

