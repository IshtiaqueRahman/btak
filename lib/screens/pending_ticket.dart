import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/pending_ticket_service.dart';
import 'package:btakapps/screens/ticket_details_jobadmin.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class JobAdminPending extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Pending Ticket'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),

      body: Body(),
    );
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool _color1,_color2,_color3;
  List<User> users = [];
  List<User> usersold = [];
  var date,previousdate;
  var serviceid;
  var isjobassigned;

  final List<Color> colors = <Color>[Colors.greenAccent, Colors.deepPurpleAccent,Colors.blueGrey,Colors.yellowAccent,Colors.pinkAccent,Colors.orangeAccent,Colors.deepOrangeAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent,Colors.blueAccent];


  Future<List<User>> _getNewList() async {
    var jobassignedval;
    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id":officeId,
      "department_id":departmentId
    };


    var response = await http.post(BaseUrl.baseurl+"pending_service_lists.json",body: json.encode(body));
    Map pendingTickerMap = jsonDecode(response.body);

    //print(response.body);
    print(body);

    var pendingTicketlist = PendingTicketService.fromJson(pendingTickerMap);

    date = DateFormat.yMMMd().format(DateTime.now());
    previousdate = DateFormat.yMMMd().format(DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day - 1));

    for(int i=0; i<pendingTicketlist.data[0].results[0].latestServices.length; i++){

      var id = pendingTicketlist.data[0].results[0].latestServices[i].services.id;
      var name = pendingTicketlist.data[0].results[0].latestServices[i].services.userName;
      var isjobassigned = pendingTicketlist.data[0].results[0].latestServices[i].services.isJobAssign;
      var department = pendingTicketlist.data[0].results[0].latestServices[i].services.departmentName;
      var image = pendingTicketlist.data[0].results[0].latestServices[i].services.userProfile;
      var servicetitle = pendingTicketlist.data[0].results[0].latestServices[i].services.serviceTitle;

      print(isjobassigned);
      users.add(new User(id, name, department, image, servicetitle,isjobassigned));
    }

    return users;
  }


  Future<List<User>> _getOldList() async {
    var jobassignedval;
    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id":officeId,
      "department_id":departmentId
    };


    var response = await http.post(BaseUrl.baseurl+"pending_service_lists.json",body: json.encode(body));
    Map pendingTickerMap = jsonDecode(response.body);

    //print(response.body);
    print(body);

    var OldTicketlist = PendingTicketService.fromJson(pendingTickerMap);


    var usersData = OldTicketlist.data[0].results[0].oldServices;

    for(int i=0; i<OldTicketlist.data[0].results[0].oldServices.length; i++){

      var id = OldTicketlist.data[0].results[0].oldServices[i].services.id;
      var isjobassigned = OldTicketlist.data[0].results[0].oldServices[i].services.isJobAssign;
      var name = OldTicketlist.data[0].results[0].oldServices[i].services.userName;
      var department = OldTicketlist.data[0].results[0].oldServices[i].services.departmentName;
      var image = OldTicketlist.data[0].results[0].oldServices[i].services.userProfile;
      var servicetitle = OldTicketlist.data[0].results[0].oldServices[i].services.serviceTitle;

      print(isjobassigned);
      usersold.add(new User(id,name, department, image, servicetitle,isjobassigned));
    }

    return usersold;
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _color1 = true;
    _color2 = true;
    _color3 = true;

  }


  @override
  Widget build(BuildContext context) {

    return Column(
      children: <Widget>[
        SizedBox(
          height: 15,
        ),

        Expanded(
            child: Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)
                    )
                ),

                child: Container(
                  child : SingleChildScrollView(
                    child: Column(
                        children: [
                           FutureBuilder(
                              future: _getNewList(),
                              builder: (BuildContext context, AsyncSnapshot snapshot) {
                                if(snapshot.data == null){
                                  return Container(
                                    child: Center(
                                      child: Text('loading'),
                                    ),
                                  );
                                }else{
                                  return SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              margin : EdgeInsets.only(left: 5),
                                              child: Text(
                                                'Today',
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  color: Color(0xff1E2661),
                                                  fontFamily: 'DMSans-Bold',
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin : EdgeInsets.only(right: 5),
                                              child: Text(
                                                date ?? '',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Color(0xff878DBA),
                                                  fontFamily: 'DMSans-Bold',
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        ListView.builder(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: snapshot.data.length,
                                            itemBuilder: (BuildContext context, int index){
                                              return Container(
                                                child: Stack(
                                                  children: [
                                                    Card(
                                                      child: Column(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          Column(
                                                            children: <Widget>[
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: <Widget>[
                                                                  Container(
                                                                    margin : EdgeInsets.only(left: 15,top: 15),
                                                                    height: 15,
                                                                    width: 15,
                                                                    decoration: BoxDecoration(
                                                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              'assets/images/time.png'),
                                                                          fit: BoxFit.contain
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                      margin : EdgeInsets.only(left: 15,top: 15),
                                                                      child: Text('you paragraph here')
                                                                  ),

                                                                  Spacer(),

                                                                  GestureDetector(
                                                                    child: Row(
                                                                      children: [
                                                                        Text(
                                                                          'Reject',
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            color: Color(0xffEB5757),
                                                                            fontFamily: 'DMSans-Bold',
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          margin : EdgeInsets.only(right: 10,left: 5),
                                                                          child: Image.asset(
                                                                            'assets/images/cancel.png',
                                                                            height: 28,
                                                                            width: 28,
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    onTap: (){
                                                                      removeItem(index);
                                                                    },
                                                                  )
                                                                ],
                                                              ),
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Container(
                                                                    margin : EdgeInsets.only(left: 15,top: 7),
                                                                    child: Text(
                                                                      snapshot.data[index].serviceName ?? '',
                                                                      style: TextStyle(
                                                                        fontSize: 16,
                                                                        color: Color(0xff1E2661),
                                                                        fontFamily: 'DMSans-Bold',
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                          Divider(),
                                                          ListTile(
                                                            title: Text(snapshot.data[index].fullName ?? ''),
                                                            subtitle: Text(snapshot.data[index].department ?? ''),
                                                            leading: Container(
                                                              height: 34,
                                                              width: 34,
                                                              decoration: BoxDecoration(
                                                                //color: Colors.yellow,
                                                                //border: Border.all(color: Colors.yellow),
                                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                image: DecorationImage(
                                                                    image: NetworkImage(snapshot.data[index].imageUrl)
//                                                            image: AssetImage(
//                                                                'assets/images/ic_data.png'),
//                                                            fit: BoxFit.contain
                                                                ),
                                                              ),
                                                            ),
                                                            //backgroundImage:
                                                            //NetworkImage(snapshot.data[index].imageUrl)),
                                                            trailing: Column(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: <Widget>[
                                                                RaisedButton(
                                                                  elevation: 0,
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(5.0),
                                                                      side: BorderSide(color: Color(0xff2DCED6))),
                                                                  onPressed: () {
                                                                    serviceid = snapshot.data[index].id;
                                                                    isjobassigned = snapshot.data[index].isjobassigned;
                                                                    print(serviceid);
                                                                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                                                                        TicketDetails(serviceid : serviceid,isjobassigned : isjobassigned)));
                                                                  },
                                                                  color: Color(0xff2DCED6),
                                                                  textColor: Color(0xffFFFFFF),
                                                                  child: Text("Details".toUpperCase(),
                                                                      style: TextStyle(fontSize: 11)),
                                                                ),
                                                              ],

                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(top: 35),
                                                      decoration: BoxDecoration(
                                                          color: colors[index],
                                                          borderRadius: BorderRadius.all(Radius.circular(20))
                                                      ),
                                                      height: 100,
                                                      width: 5,
                                                    ),
                                                  ],

                                                ),
                                              );
                                            }
                                        ),


                                      ],
                                    ),
                                  );
                                }
                              }
                          ),

                           SizedBox(
                              height: 20,
                           ),
                           FutureBuilder(
                              future: _getOldList(),
                              builder: (BuildContext context, AsyncSnapshot snapshot) {
                                if(snapshot.data == null){
                                  return Container(
                                    child: Center(
                                      child: Text(''),
                                    ),
                                  );
                                }else{
                                  return SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              margin : EdgeInsets.only(left: 5),
                                              child: Text(
                                                'Old Task',
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  color: Color(0xff1E2661),
                                                  fontFamily: 'DMSans-Bold',
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin : EdgeInsets.only(right: 5),
                                              child: Text(
                                                previousdate ?? '',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Color(0xff878DBA),
                                                  fontFamily: 'DMSans-Bold',
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        ListView.builder(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: snapshot.data.length,
                                            itemBuilder: (BuildContext context, int index){
                                              return Container(
                                                child: Stack(
                                                  children: [
                                                    Card(
                                                      child: Column(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          Column(
                                                            children: <Widget>[
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: <Widget>[
                                                                  Container(
                                                                    margin : EdgeInsets.only(left: 15,top: 15),
                                                                    height: 15,
                                                                    width: 15,
                                                                    decoration: BoxDecoration(
                                                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              'assets/images/time.png'),
                                                                          fit: BoxFit.contain
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                      margin : EdgeInsets.only(left: 15,top: 15),
                                                                      child: Text('you paragraph here')
                                                                  ),

                                                                  Spacer(),

                                                                  GestureDetector(
                                                                    child: Row(
                                                                      children: [
                                                                        Text(
                                                                          'Reject',
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            color: Color(0xffEB5757),
                                                                            fontFamily: 'DMSans-Bold',
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          margin : EdgeInsets.only(right: 10,left: 5),
                                                                          child: Image.asset(
                                                                            'assets/images/cancel.png',
                                                                            height: 28,
                                                                            width: 28,
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    onTap: (){
                                                                      removeItem(index);
                                                                    },
                                                                  )
                                                                ],
                                                              ),
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Container(
                                                                    margin : EdgeInsets.only(left: 15,top: 7),
                                                                    child: Text(
                                                                      snapshot.data[index].serviceName ?? '',
                                                                      style: TextStyle(
                                                                        fontSize: 16,
                                                                        color: Color(0xff1E2661),
                                                                        fontFamily: 'DMSans-Bold',
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                          Divider(),
                                                          ListTile(
                                                            title: Text(snapshot.data[index].fullName ?? ''),
                                                            subtitle: Text(snapshot.data[index].department ?? ''),
                                                            leading: Container(
                                                              height: 34,
                                                              width: 34,
                                                              decoration: BoxDecoration(
                                                                //color: Colors.yellow,
                                                                //border: Border.all(color: Colors.yellow),
                                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                image: DecorationImage(
                                                                    image: NetworkImage(snapshot.data[index].imageUrl)
//                                                            image: AssetImage(
//                                                                'assets/images/ic_data.png'),
//                                                            fit: BoxFit.contain
                                                                ),
                                                              ),
                                                            ),
                                                            //backgroundImage:
                                                            //NetworkImage(snapshot.data[index].imageUrl)),
                                                            trailing: Column(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: <Widget>[
                                                                RaisedButton(
                                                                  elevation: 0,
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(5.0),
                                                                      side: BorderSide(color: Color(0xff2DCED6))),
                                                                  onPressed: () {
                                                                    serviceid = snapshot.data[index].id;
                                                                    isjobassigned = snapshot.data[index].isjobassigned;
                                                                    print(serviceid);
                                                                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                                                                        TicketDetails(serviceid : serviceid,isjobassigned : isjobassigned)));
                                                                  },
                                                                  color: Color(0xff2DCED6),
                                                                  textColor: Color(0xffFFFFFF),
                                                                  child: Text("Details".toUpperCase(),
                                                                      style: TextStyle(fontSize: 11)),
                                                                ),
                                                              ],

                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(top: 35),
                                                      decoration: BoxDecoration(
                                                          color: colors[index],
                                                          borderRadius: BorderRadius.all(Radius.circular(20))
                                                      ),
                                                      height: 100,
                                                      width: 5,
                                                    ),
                                                  ],

                                                ),
                                              );
                                            }
                                        ),


                                      ],
                                    ),
                                  );
                                }
                              }
                          ),
                         ],
                      ),
                  )

                )
            )
        ),
      ],
    );
  }

  void removeItem(int index) {
    setState(() {
      users = List.from(users)
        ..removeAt(index);
    });
  }
}

class User {
  final String fullName;
  final String id;
  final String department;

  final String imageUrl;

  final String serviceName;
  final String isjobassigned;

  User(this.id, this.fullName, this.department, this.imageUrl, this.serviceName, this.isjobassigned);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}
