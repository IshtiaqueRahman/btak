import 'dart:convert';
import 'dart:io';

import 'package:btakapps/apiBase/base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'create_new_task_technician1.dart';

class CreateNewTask extends StatefulWidget {
  @override
  _CreateNewTaskState createState() => _CreateNewTaskState();
}

class _CreateNewTaskState extends State<CreateNewTask> {
  String title = 'DropDownButton';
  String serviceName;
  //List _list = ['Computer','Electrical','HR'];

  File imageFile;
  List datalist = List(); //edited line
  List<Item> _list = List();
  String text;
  int deptid;
  var deptname;
  TextEditingController service = new TextEditingController();
  Item departmentName;

  @override
  void initState() {
   // print("called primary");
    super.initState();
    this.getSWData();
    //departmentName = _list[0].value;

  }

//
  Future<String> getSWData() async {
    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
    };

    final response =
    await http.post(BaseUrl.baseurl+"department_list.json",body: json.encode(body));

    final jsonresp = json.decode(response.body.toString());


    //dynamic _mapCompany = jsonDecode(response.body.toString());
    datalist = jsonresp["data"][0]["departments"];
    for(int i=0; i<datalist.length; i++){
        //print(datalist[i]['name']);

        setState(() {
          //_list.add();
          _list.add(new Item(datalist[i]['id'], datalist[i]['name']));
        });


       //print(_list.toString());
    }




    return "Sucess";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Create New Service'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        margin: EdgeInsets.only(top: 25),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 18,
                    width: 18,
                    child: Image.asset('assets/images/reqname.png'),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Service Name',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff1E2661),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ],
              ),
              Container(
                  width: 320,
                  height: 60,
                  margin: EdgeInsets.only(top: 10,left: 25),
                  padding: EdgeInsets.all(6.0),
                  child: TextField(
                    controller: service,
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: 'Service Name',
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Color(0xffF5F5F5),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                      ),
                    ),)
              ),

              SizedBox(
                height: 22,
              ),

              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 18,
                    width: 18,
                    child: Image.asset('assets/images/deptselect.png'),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Select Department',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff1E2661),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10,left: 25),
                height: 42,
                width: 272,
                decoration: BoxDecoration(
                    color: Color(0xffF5F5F5),
                    border: Border.all(
                        color: Color(0xffF5F5F5)
                    ),
                    borderRadius: BorderRadius.circular(8.0)
                ),
                child: Center(
                  child: DropdownButtonHideUnderline(
                    child:  new DropdownButton<Item>(
                      hint: Text('Select Specific Department'),
                      value: departmentName,
                      onChanged: (Item newValue) {
                        setState(() {
                          departmentName = newValue;
                        });
                      },
                      items: _list.map((Item user) {
                        return new DropdownMenuItem<Item>(
                          value: user,
                          child: new Text(
                            user.value,
                            style: new TextStyle(color: Colors.black),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  ),
              ),

              SizedBox(
                height: 35,
              ),

              Container(
                margin: EdgeInsets.only(right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                      width: 122,
                      height: 48,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            side: BorderSide(color: Color(0xff2DCED6))
                        ),
                        onPressed: () {
                          //print(serviceName);
                          //print(departmentName);
                          deptid = int.parse(departmentName.ids);
                          deptname = departmentName.value;
                          Navigator.push(context, new MaterialPageRoute(builder: (context) => new CreateNewTask1(serviceName: service.text.toString(),departmentName: deptname,deptid: deptid)));
                        },
                        color: Color(0xff2DCED6),
                        textColor: Color(0xffFFFFFF),
                        child: Text("Next".toUpperCase(),
                            style: TextStyle(fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 15,
              ),

            ],
          ),
        ),
      ),
      //body: ListShow(),
    );
  }

  int getid(String value) {
    int deptid;
    for(int i=0; i<datalist.length; i++){
      if(datalist[i]['name']==value.trim()){
        //print(servicedetails.data[0].serviceDetails.taskList[i].id);
        deptid = datalist[i].id;
      }
    }

    return deptid;
  }
}





Widget chip(String label, Color color) {
  return Chip(
    labelPadding: EdgeInsets.all(3.0),
    avatar: Container(
      height: 5,
      width: 5,
      child: MaterialButton(
        onPressed: () {},
        color: Colors.blue,
        textColor: Colors.white,
        padding: EdgeInsets.all(15),
        shape: CircleBorder(),
      ),
    ),
    label: Text(
      label,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    backgroundColor: color,
    elevation: 6.0,
    shadowColor: Colors.grey[60],
    padding: EdgeInsets.all(6.0),
  );
}

class Item {
  final String ids;
  final String value;
  Item(this.ids, this.value);
}
