import 'package:btakapps/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class UserMyARequest extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
      return Scaffold(
        backgroundColor: Color(0xff3D4FF4),
          appBar: AppBar(
             title: Text('My Task'),
            backgroundColor: Color(0xff3D4FF4),
            elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(),
      );
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool _color1,_color2,_color3;
  Future<List<User>> _getUsers() async {
    List<User> users = [];
    var response = await http.get('https://api.randomuser.me/?results=20');

    var jsonData = jsonDecode(response.body);

    var usersData = jsonData["results"];

    for (var user in usersData) {
      User newUser = User(user["name"]["first"] + user["name"]["last"],
          user["email"], user["picture"]["large"], user["phone"]);

      users.add(newUser);
    }

    return users;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _color1 = true;
    _color2 = true;
    _color3 = true;
  }


  @override
  Widget build(BuildContext context) {

    return Column(
      children: <Widget>[
        SizedBox(
           height: 15,
        ),
        Container(
          width: 327,
          height: 37,
          decoration: BoxDecoration(
            border: Border.all(
              color: Color(0xff3BFFFFFF),
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              GestureDetector(
                child : _color1 ? Container(
                  child: Text(
                    'All',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 58,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'All',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                    setState(() {
                      _color1 = !_color1;
                    });
                },
              ),
              GestureDetector(
                child : _color1 ? Container(
                  child: Text(
                    'Pending',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 62,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'Pending',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                  setState(() {
                    _color1 = !_color1;
                  });
                },
              ),
              GestureDetector(
                child : _color2 ? Container(
                  child: Text(
                    'Ongoing',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 62,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'Ongoing',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                    setState(() {
                      _color2 = !_color2;
                    });
                },
              ),

              GestureDetector(
                child : _color3 ? Container(
                  child: Text(
                    'Completed',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 80,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'Completed',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                    setState(() {
                      _color3 = !_color3;
                    });
                },
              ),

            ],
          ),
        ),
        SizedBox(
          height: 50.0,
        ),
        Expanded(
            child: Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)
                    )
                ),

                child: Container(
                  child: FutureBuilder(
                      future: _getUsers(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if(snapshot.data == null){
                          return Container(
                            child: Center(
                              child: Text('loading'),
                            ),
                          );
                        }else{
                          return SingleChildScrollView(
                            child: Stack(
                              children: [
                                Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text("My Request",
                                            style: TextStyle(fontSize: 20,color: Color(0xff1E2661))),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount: snapshot.data.length,
                                        itemBuilder: (BuildContext context, int index){
                                          return Card(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                Column(
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        Container(
                                                          margin : EdgeInsets.only(left: 15,top: 15),
                                                          height: 15,
                                                          width: 15,
                                                          decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                                            image: DecorationImage(
                                                                image: AssetImage(
                                                                    'assets/images/settings.png'),
                                                                fit: BoxFit.contain
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                            margin : EdgeInsets.only(left: 15,top: 15),
                                                            child: Text('you paragraph here')
                                                        ),

                                                        Spacer(),
                                                        Padding(
                                                          padding: const EdgeInsets.only(top: 10,right: 15),
                                                          child: Container(
                                                            child: CircularPercentIndicator(
                                                              radius: 50.0,
                                                              lineWidth: 5.0,
                                                              percent: 1.0,
                                                              center: new Text("100%"),
                                                              progressColor: Color(0xff2DCED6),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        Container(
                                                          margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                          height: 15,
                                                          width: 15,
                                                          decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.all(Radius.circular(8)),
                                                            image: DecorationImage(
                                                                image: AssetImage(
                                                                    'assets/images/time.png'),
                                                                fit: BoxFit.contain
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                            margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                            child: Text('Tomorrow')
                                                        ),
                                                        Container(
                                                            margin : EdgeInsets.only(left: 5,top: 5,bottom: 15),
                                                            child: Text('-')
                                                        ),
                                                        Container(
                                                            margin : EdgeInsets.only(left: 5,top: 5,bottom: 15),
                                                            child: Text('12.45 pm')
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                      child: Text("Computer doesn't start",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color: Color(0xff1E2661),
                                                              fontFamily: 'DMSans-Bold',
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Divider(),
                                                ListTile(
                                                  title: Row(
                                                    children: [
                                                      Container(
                                                        height : 5,
                                                        width: 5,
                                                        decoration: BoxDecoration(
                                                            color: Colors.orange,
                                                            shape: BoxShape.circle
                                                        ),
                                                      ),
                                                      Container(
                                                        margin : EdgeInsets.only(left: 5),
                                                        child: Text("Medium Priority",
                                                            style: TextStyle(fontSize: 11,color: Colors.orange)),
                                                      ),
                                                    ],
                                                  ),

                                                  //backgroundImage:
                                                  //NetworkImage(snapshot.data[index].imageUrl)),
                                                  trailing: Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      RaisedButton(
                                                        elevation: 0,
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(5.0),
                                                            side: BorderSide(color: Color(0xffA880E3))),
                                                        onPressed: () {},
                                                        color: Color(0xffF2ECFB),
                                                        textColor: Color(0xffA880E3),
                                                        child: Text("In Progress".toUpperCase(),
                                                            style: TextStyle(fontSize: 11)),
                                                      ),
                                                    ],

                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        }
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 85),
                                  decoration: BoxDecoration(
                                      color: Colors.greenAccent,
                                      borderRadius: BorderRadius.all(Radius.circular(20))
                                  ),
                                  height: 100,
                                  width: 5,
                                ),
                              ],

                            ),
                          );
                        }
                      }
                  ),
                )
            )
        ),
      ],
    );
  }
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}
