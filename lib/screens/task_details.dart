import 'dart:io';
import 'package:btakapps/db/dbhelper.dart';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/screens/team_member.dart';
import 'package:btakapps/screens/teammember.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';

class TaskDetails extends StatelessWidget {
  String taskid;
  // This widget is the root of your application.

  TaskDetails({this.taskid});

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          backgroundColor: Color(0xff6C5DD3),
          appBar: AppBar(
             title: Text('Task Assigning'),
             backgroundColor: Color(0xff6C5DD3),
            elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(taskid),
      );
  }
}

class Body extends StatefulWidget {
  String taskid;

  Body(String taskid){
     this.taskid = taskid;
  }


  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1','list2','list3'];
  File imageFile;
  DateTime _setDate = DateTime.now();
  Duration initialtimer = new Duration();
  int selectitem = 1;
  List<Todo> taskList = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DatabaseHelper.instance.queryAllRows().then((value) {
      setState(() {
        value.forEach((element) {
          taskList.add(Todo(id: element['id'], title: element["title"]));
          //print(taskList.length);
        });
      });
    }).catchError((error) {
      print(error);
    });
  }
  @override
  Widget build(BuildContext context) {
    DateTime _fromDate = DateTime.now();
    TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());
    

    Widget datetime() {
      return CupertinoDatePicker(
        initialDateTime: DateTime.now(),
        onDateTimeChanged: (DateTime newdate) {
          print(newdate);
        },
        use24hFormat: true,
        maximumDate: new DateTime(3500, 12, 30),
        minimumYear: 2020,
        maximumYear: 3500,
        minuteInterval: 1,
        mode: CupertinoDatePickerMode.dateAndTime,
      );
    }

    Widget time() {
      return CupertinoTimerPicker(
        mode: CupertinoTimerPickerMode.hms,
        minuteInterval: 1,
        secondInterval: 1,
        initialTimerDuration: initialtimer,
        onTimerDurationChanged: (Duration changedtimer) {
          setState(() {
            initialtimer = changedtimer;
          });
        },
      );
    }

    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];
    return Container(
        color: Color(0xff6C5DD3),
        child:  Expanded(
          child: Container(
            margin: EdgeInsets.only(top: 50),
            width: double.infinity,
            height: 800,
            decoration: BoxDecoration(
                color: kShadowColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0)
                )
            ),
            child: SingleChildScrollView(
              child: SizedBox(
                height: 800,
                child: Column(
                  children: <Widget>[
                  SizedBox(
                  height: 10,
                ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Wrap(
                          children: [
                            new Container(
                              child: new Image.asset(
                                'assets/images/clock.png',
                                height: 17.0,
                                width: 17.0,
                                fit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                              child: Text(
                                'Start Date',
                                style: TextStyle(
                                    color: Color(0xff878DBA),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                            ),
                          ],
                        ),

                        GestureDetector(
                          child: Text(
                            '19 January at 10:00',
                            style: TextStyle(
                                color: Color(0xff1E2661),
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal
                            ),
                          ),
                          onTap: (){
                            _bottomSheetMore(context);
                          },
                        ),
                      ],
                    )
                ),
                  Container(
                    height: 0.2,
                    color: Colors.grey,
                  ),
                  Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Wrap(
                            children: [
                              new Container(
                                child: new Image.asset(
                                  'assets/images/clock.png',
                                  height: 17.0,
                                  width: 17.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 25),
                                child: Text(
                                  'End Date',
                                  style: TextStyle(
                                      color: Color(0xff878DBA),
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.normal
                                  ),
                                ),
                              ),
                            ],
                          ),

                          GestureDetector(
                              child: Text(
                                '02 February,2014',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: Color(0xff1E2661),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                              onTap: () {

                                //_showDatePicker(context);
                                // DatePicker.showDatePicker(context,
                                // showTitleActions: true,
                                // minTime: DateTime(2018, 3, 5),
                                // maxTime: DateTime(5000, 6, 7), onChanged: (date) {
                                // print('change $date');
                                // }, onConfirm: (date) {
                                // print('confirm $date');
                                // }, currentTime: DateTime.now(), locale: LocaleType.en);
                                // },
                                showModalBottomSheet(

                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                      BorderRadius.vertical(
                                          top: Radius.circular(20.0))),
                                  backgroundColor: Colors.white,
                                  context: context,
                                  isScrollControlled: true,
                                  builder: (context) =>
                                      Container(
                                        height: 400,
                                        child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 18),
                                            child: datetime()),
                                      ),
                                );
                              }
                          ),
                        ],
                      )
                  ),
                  Container(
                    height: 0.2,
                    color: Colors.grey,
                  ),
                  Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Wrap(
                            children: [
                              new Container(
                                child: new Image.asset(
                                  'assets/images/clock.png',
                                  height: 17.0,
                                  width: 17.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 25),
                                child: Text(
                                  'Hour',
                                  style: TextStyle(
                                      color: Color(0xff878DBA),
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.normal
                                  ),
                                ),
                              ),
                            ],
                          ),

                          GestureDetector(
                            child: Text(
                              '1 Hour',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Color(0xff1E2661),
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal
                              ),
                            ),
                            onTap: (){
                              // _showTimePicker(context);
                              showModalBottomSheet(

                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.vertical(
                                        top: Radius.circular(20.0))),
                                backgroundColor: Colors.white,
                                context: context,
                                isScrollControlled: true,
                                builder: (context) =>
                                    Container(
                                      height: 400,
                                      child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 18),
                                          child: time()),
                                    ),
                              );
                            },
                          ),
                        ],
                      )
                  ),
                  Container(
                    height: 0.2,
                    color: Colors.grey,
                  ),
                  Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Wrap(
                            children: [
                              new Container(
                                child: new Image.asset(
                                  'assets/images/progress.png',
                                  height: 17.0,
                                  width: 17.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 25),
                                child: Text(
                                  'Task',
                                  style: TextStyle(
                                      color: Color(0xff878DBA),
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.normal
                                  ),
                                ),
                              ),
                            ],
                          ),

                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 10.0),
                            decoration: BoxDecoration(
                              color: Color(0xff2DCED6),
                              borderRadius: BorderRadius.circular(15.0),
                              border: Border.all(
                                  color: Color(0xff2DCED6), style: BorderStyle.solid, width: 0.80),
                            ),
                            child: DropdownButton(
                              items: _dropdownValues
                                  .map((value) => DropdownMenuItem(
                                child: Text(value,style: TextStyle(
                                    color: Colors.white
                                ),),
                                value: value,
                              ))
                                  .toList(),
                              onChanged: (String value) {},
                              isExpanded: false,
                              value: _dropdownValues.first,
                            ),

                          ),

                        ],
                      )
                  ),
                  Container(
                    height: 0.2,
                    color: Colors.grey,
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          child: new Image.asset(
                            'assets/images/attachment.png',
                            height: 20.0,
                            width: 20.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            'Attachment',
                            style: TextStyle(
                                color: Color(0xff878DBA),
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          height: 25.0,
                          width: 25.0,
                          color: Colors.transparent,
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
                              child: new Center(
                                child: new Text("5",
                                  style: TextStyle(fontSize: 11, color: Colors.white),
                                  textAlign: TextAlign.center,),
                              )),
                        ),

                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 25),
                          child: FDottedLine(
                            color: Color(0xffD4D4D4),
                            corner: FDottedLineCorner.all(6.0),

                            /// add widget
                            child: Container(
                              color: Color(0xffFFFFFF),
                              width: 130,
                              height: 102,
                              child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          _decideImage(),
                                        ],
                                      )

                                    ],
                                  )
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: FDottedLine(
                            color: Color(0xffD4D4D4),
                            corner: FDottedLineCorner.all(6.0),

                            /// add widget
                            child: Container(
                              color: Color(0xffFFFFFF),
                              width: 130,
                              height: 102,
                              child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          _decideImage(),
                                        ],
                                      )

                                    ],
                                  )
                              ),
                            ),
                          ),
                        ),
                      ),


                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          child: new Image.asset(
                            'assets/images/progress.png',
                            height: 17.0,
                            width: 17.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            'Action from admin',
                            style: TextStyle(
                                color: Color(0xff878DBA),
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20,top: 3,right: 20),
                    child: Text(
                      'Task Descriptions are the statements of scope for each of the project activities.Are written in the format of “action.',
                      style: TextStyle(
                          color: Color(0xff1E2661),
                          fontSize: 14.0,
                          fontWeight: FontWeight.normal
                      ),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          child: new Image.asset(
                            'assets/images/progress.png',
                            height: 17.0,
                            width: 17.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 120),
                          child: Text(
                            'Assigned to',
                            style: TextStyle(
                                color: Color(0xff878DBA),
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal
                            ),
                          ),
                        ),
                        GestureDetector(
                          child: Container(
                            height: 35,
                            width: 35,
                            child: Image.asset('assets/images/plus.png'),
                          ),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TeamMemberUserReport()));
                          },
                        ),
                      ],
                    )
                ),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.zero,
                        child: taskList.isEmpty
                            ? Container()
                            : ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: taskList.length,
                            itemBuilder: (ctx, index) {
                          if (index == taskList.length) return null;
                          return Flexible(
                            child: Image.asset(
                                taskList[index].title,
                                height: 50,
                                width: 50,
                            ),
                          );
                          // return ListTile(
                          //   title: Container(
                          //       height: 50,
                          //       width: 50,
                          //       child: Image.asset(taskList[index].title)),
                          // );
                        }),
                      ),
                    ),
                  Container(
                  height: 0.2,
                  color: Colors.grey,
                ),

                ],
            ),
              ),
          ),
          //child: Text('This is text'),
        ),
       ),
      );
  }

  _decideImage() {
    if(imageFile == null){
      return Text('No image selected');
    }else{
      return Expanded(
        child: Image.file(
          imageFile,
          width: 130,
          height: 102,
          fit: BoxFit.cover,
        ),
      );
    }
  }

   _pickDateDialog(BuildContext context) {
    DateTime _selectedDate;
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        //which date will display when user open the picker
        firstDate: DateTime(1950),
        //what will be the previous supported year in picker
        lastDate: DateTime
            .now()) //what will be the up to supported date in picker
        .then((pickedDate) {
      //then usually do the future job
      if (pickedDate == null) {
        //if user tap cancel then this function will stop
        return;
      }
      setState(() {
        //for rebuilding the ui
        _selectedDate = pickedDate;
      });
    });
  }



}




void _bottomSheetMore(BuildContext context) {
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}




