import 'dart:io';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/db/dbhelper.dart';
import 'package:btakapps/model/service_details.dart';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/screens/team_member.dart';
import 'package:btakapps/screens/teammember.dart';
import 'package:btakapps/screens/chat_task_details_jobadmin.dart';
import 'package:btakapps/screens/chat_task_details.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TaskListJobAdmin extends StatelessWidget {
  // This widget is the root of your application.
  List listid = List();
  String serviceid;

  TaskListJobAdmin({this. listid,this.serviceid});

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Color(0xff3D4FF4),
          appBar: AppBar(
             title: Column(
               children: [
                 Text('Task List'),
               ],
             ),
             backgroundColor: Color(0xff3D4FF4),
             elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(listid,serviceid),
      );
  }
}

class Body extends StatefulWidget {
  List isCheckedList = List();
  String serviceid;

  Body(List _listid,String serviceid){
    isCheckedList = _listid;
    this.serviceid = serviceid;
    print(isCheckedList.toString());
  }

  @override
  _BodyState createState() => _BodyState(isCheckedList,serviceid);
}

class _BodyState extends State<Body> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1','list2','list3'];
  File imageFile;
  DateTime _setDate = DateTime.now();
  Duration initialtimer = new Duration();
  int selectitem = 1;
  List<Todo> taskList = new List();
  List<RadioModel> sampleData = new List<RadioModel>();
  List isCheckedList = List();
  String serviceid;
  List<Tasks> tasks = List();

  String id;

  _BodyState(List isCheckedList, String serviceid){
     this.isCheckedList = isCheckedList;
     this.serviceid = serviceid;
  }
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
    sampleData.add(new RadioModel(false, 'A', 'April 18'));
    sampleData.add(new RadioModel(false, 'B', 'April 17'));
    sampleData.add(new RadioModel(false, 'C', 'April 16'));
    sampleData.add(new RadioModel(false, 'D', 'April 15'));


    DatabaseHelper.instance.queryAllRows().then((value) {
      setState(() {
        value.forEach((element) {
          taskList.add(Todo(id: element['id'], title: element["title"]));
          //print(taskList.length);
        });
      });
    }).catchError((error) {
      print(error);
    });
  }


  Future<List<Tasks>> getList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      //"user_id": 1,
      "service_id": serviceid
    };

    print(body);
    var response = await http.post(BaseUrl.baseurl+"service_details.json",body: json.encode(body));
    Map taskListMap = jsonDecode(response.body);
    var taskList = ServiceDetailsClass.fromJson(taskListMap);


    print(taskList.data[0].serviceDetails.taskList.length);

    for(int i=0; i<taskList.data[0].serviceDetails.taskList.length; i++){

      var id = taskList.data[0].serviceDetails.taskList[i].id;
      var name = taskList.data[0].serviceDetails.taskList[i].technicianName;
      var status = taskList.data[0].serviceDetails.taskList[i].jobStatus;
      var jobstatusid = taskList.data[0].serviceDetails.taskList[i].jobStatusTypeId;

      print(name);
      setState(() {
        tasks.add(new Tasks(id,name,status,jobstatusid));
      });

    }
  }


  @override
  Widget build(BuildContext context) {


    var height = MediaQuery.of(context).size;

    DateTime _fromDate = DateTime.now();
    TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());
    
    // Future<void> _showDatePicker(BuildContext context) async {
    //   final picked = await showDatePicker(
    //     context: context,
    //     initialDate: _fromDate,
    //     firstDate: DateTime(2015, 1),
    //     lastDate: DateTime(2100),
    //   );
    //   if (picked != null && picked != _fromDate) {
    //     setState(() {
    //       _fromDate = picked;
    //     });
    //   }
    // }
    //
    // Future<void> _showTimePicker(BuildContext context) async {
    //   final picked = await showTimePicker(
    //     context: context,
    //     initialTime: _fromTime,
    //   );
    //   if (picked != null && picked != _fromTime) {
    //     setState(() {
    //       _fromTime = picked;
    //     });
    //   }
    // }

    Widget datetime() {
      return CupertinoDatePicker(
        initialDateTime: DateTime.now(),
        onDateTimeChanged: (DateTime newdate) {
          print(newdate);
        },
        use24hFormat: true,
        maximumDate: new DateTime(3500, 12, 30),
        minimumYear: 2020,
        maximumYear: 3500,
        minuteInterval: 1,
        mode: CupertinoDatePickerMode.dateAndTime,
      );
    }

    Widget time() {
      return CupertinoTimerPicker(
        mode: CupertinoTimerPickerMode.hms,
        minuteInterval: 1,
        secondInterval: 1,
        initialTimerDuration: initialtimer,
        onTimerDurationChanged: (Duration changedtimer) {
          setState(() {
            initialtimer = changedtimer;
          });
        },
      );
    }

    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];

//    final europeanCountries = ['J00750', 'BB0750', 'J00750', 'J00750',
//      'J00750', 'J00750', 'J00750', 'J00750', 'J00750'];
//    final name = ['Alamin', 'Sumon\niqbal', 'Alamin\nkhan', 'Moinuddin',
//      'Masum\nahmed', 'Sumon\nchowdhury', 'Iqbal\nmahmud', 'Kalam\nmahum', 'Jamal\nahmed'];

    return SingleChildScrollView(

      child:  Container(
        color: Color(0xff3D4FF4),
        child: Column(
              children: [
                Container(
                  //height: MediaQuery.of(context).size.height * 0.04,
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top:15 , left: 20),
                        child: Text(
                          'Ticket :',
                          style: TextStyle(
                              color: Color(0xffFFFFFF),
                              fontSize: 24.0,
                              fontWeight: FontWeight.normal
                          ),
                        ),
                      ),

                  Container(
                    margin: EdgeInsets.only(top:15 , left: 15),
                    child: Text(
                      serviceid,
                      style: TextStyle(
                          color: Color(0xff2DCED6),
                          fontSize: 24.0,
                          fontWeight: FontWeight.normal
                      ),
                    ),
                  )

                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 1.4,
                  decoration: BoxDecoration(
                      color: kShadowColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0)
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Row(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           mainAxisAlignment: MainAxisAlignment.spaceAround,
                           children: [
                             Text(
                               'Task No',
                               style: TextStyle(
                                   color: Color(0xff878DBA),
                                   fontSize: 16.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),

                             Text(
                               'Technician',
                               style: TextStyle(
                                   color: Color(0xff878DBA),
                                   fontSize: 16.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),

                             Text(
                               'Status',
                               style: TextStyle(
                                   color: Color(0xff878DBA),
                                   fontSize: 16.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),

                             Text(
                               'Assign',
                               style: TextStyle(
                                   color: Color(0xff878DBA),
                                   fontSize: 16.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),
                           ],
                        ),

                        tasks.length != 0 ?  ListView.builder(
                      shrinkWrap: true,
                      itemCount: tasks.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(tasks[index].taskno??''),
                                  Wrap(
                                    children: [
                                          Expanded(
                                            child: Text(
                                                tasks[index].technician??'',
                                                maxLines: 4,
                                                overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                    ],   
                                  ),
                                  Container(
                                    child: RaisedButton(
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(5.0),
                                          side: BorderSide(color: tasks[index].status == "Pending" ? Color(0xffEB5757) : tasks[index].status == "Completed" ? Color(0xff27AE60) : Color(0xffA880E3))),
                                      onPressed: () {
                                        String taskid = tasks[index].taskno;
                                        String jobstatustypeid = tasks[index].jobstatusid;
                                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ChatScreenJobAdmin(taskid:taskid,serviceid:serviceid,jobstatus_typeid:jobstatustypeid,jobid:taskid)));
                                      },
                                      color: tasks[index].status == "Pending" ? Color(0xffFCE6E6) : tasks[index].status == "Completed" ? Color(0xffDFF3E7) : Color(0xffF2ECFB),
                                      textColor: tasks[index].status == "Pending" ? Color(0xffEB5757) : tasks[index].status == "Completed" ? Color(0xff27AE60) : Color(0xffA880E3),
                                      child: Text(tasks[index].status.toUpperCase(), style: TextStyle(fontSize: 11)),
                                    ),
                                  ),
                                  GestureDetector(
                                    child: Container(
                                       height: 29,
                                       width: 29,
                                       child: Image.asset('assets/images/assign.png'),
                                    ),
                                    onTap: (){
                                      String taskid = tasks[index].taskno;

                                      print(tasks[index].taskno);
                                       if(tasks[index].status.toUpperCase() == "ASSIGNED"){
                                         _showToast(context);
                                       }else{
                                         Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TeamMemberUserReport(taskid:taskid,serviceid:serviceid)));
                                       }

                                    },
                                  )

                                ],
                              ),


                            ],
                          ),
                        );
                         },
                       )  : new  SizedBox(
                          height: MediaQuery.of(context).size.height / 1.8,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      ],
                    ),
                  )
                ),
                //child: Text('This is text'),
               ],
            ),
         ),
      );
  }

  _decideImage() {
    if(imageFile == null){
      return Text('No image selected');
    }else{
      return Expanded(
        child: Image.file(
          imageFile,
          width: 130,
          height: 102,
          fit: BoxFit.cover,
        ),
      );
    }
  }

   _pickDateDialog(BuildContext context) {
    DateTime _selectedDate;
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        //which date will display when user open the picker
        firstDate: DateTime(1950),
        //what will be the previous supported year in picker
        lastDate: DateTime
            .now()) //what will be the up to supported date in picker
        .then((pickedDate) {
      //then usually do the future job
      if (pickedDate == null) {
        //if user tap cancel then this function will stop
        return;
      }
      setState(() {
        //for rebuilding the ui
        _selectedDate = pickedDate;
      });
    });
  }

  Widget _myListView(BuildContext context) {
    return Wrap(
      children: [
        Container(
          margin: EdgeInsets.only(left: 35),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text('Sun'),
              ),
              ListTile(
                title: Text('Moon'),
              ),
              ListTile(
                title: Text('Star'),
              ),
            ],
          ),
        ),
      ],
    );
  }

  String getId(String name) {
    for(int i=0; i<tasks.length; i++){
      if(tasks[i].technician == name.trim()){
        //print();
         id = tasks[i].taskno.toString();
      }
    }

    return id;
  }

  void _showToast(BuildContext context) {
    Fluttertoast.showToast(
        msg: "Already assigned..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }
  }

class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  bool _value = true;

  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Container(
        margin: EdgeInsets.only(left: 25,top: 10),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        Center(
        child: InkWell(
          child: Container(
          decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xff2DD8CF)),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: _value
                ? Icon(
              Icons.check,
              size: 13.0,
              color: Colors.white,
            )
                : Icon(
              Icons.check_box_outline_blank,
              size: 10.0,
              color: Color(0xff2DD8CF),
            ),
          ),
        ),
      )),

            new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(_item.text,style: TextStyle(
                  fontSize: 14
              ),),
            )
          ],
        ),
      ),
    );
  }
}

void _bottomSheetMore(BuildContext context) {
}

class Tasks {
  final String taskno;
  final String technician;
  final String status;
  final String jobstatusid;

  Tasks(this.taskno, this.technician, this.status, this.jobstatusid);
}

class UserDetailPage extends StatelessWidget {
  final Tasks user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.technician),
      ),
    );
  }
}




