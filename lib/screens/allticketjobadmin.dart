import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/service_lists.dart';
import 'package:btakapps/screens/request_sent.dart';
import 'package:btakapps/screens/tasklist_jobadmin.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class AllticketJobAdmin extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
      return Scaffold(
          backgroundColor: Color(0xff3D4FF4),
          appBar: AppBar(
             title: Text('All Service'),
             backgroundColor: Color(0xff3D4FF4),
            elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(),
      );
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool _color1,_color2,_color3,_color4;
  List<TicketLists> ticketlist = List();
  List<TicketLists> ticketFilterlist = List();
  var serviceid;

  Future<List<TicketLists>> getList() async {

    ticketlist.clear();

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");
    List<TicketLists> users = [];

    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      "office_id":officeId,
      "department_id":departmentId
    };

    print(body);
    var response = await http.post(BaseUrl.baseurl+"service_lists.json",body: json.encode(body));
    Map taskListMap = jsonDecode(response.body);
    var taskList = Service_Lists.fromJson(taskListMap);

    //print(taskList.data[0].results.length);
    for(int i=0; i<taskList.data[0].results.length; i++){

      var id = taskList.data[0].results[i].services.id;
      var date = taskList.data[0].results[i].services.createdAt;
      var title = taskList.data[0].results[i].services.serviceTitle;
      var priority = taskList.data[0].results[i].services.priority;
      var name = taskList.data[0].results[i].services.userName;
      var status = taskList.data[0].results[i].services.status;
      var department = taskList.data[0].results[i].services.departmentName;
      var progress = taskList.data[0].results[i].services.progress;
      var is_service_complete = taskList.data[0].results[i].services.isVerifyByJobAdmin;
      var ticket_status_type_id = taskList.data[0].results[i].services.ticketStatusTypeId;

      var dateFormatter = new DateFormat('yyyy-MM-dd');

      var percent = double.parse(taskList.data[0].results[i].services.progress) / 100;

     // print(percent);

      var image = taskList.data[0].results[i].services.userProfile;

      //print(name);
      setState(() {
        ticketlist.add(new TicketLists(id,date,title,priority,name,department,progress,image,percent,status,ticket_status_type_id,is_service_complete));
      });
    }
    return ticketlist;
  }


  Future<List<TicketLists>> getFilteredList(String id) async {

    print('callled');

    ticketlist.clear();

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");
    List<TicketLists> users = [];

    Map<String, dynamic> body = {
      "reference_id": "BITAC-a8dcb57ae8c5",
      "office_id":officeId,
      "department_id":departmentId
    };

   // print(body);
    var response = await http.post(BaseUrl.baseurl+"service_lists.json",body: json.encode(body));
    Map taskListMap = jsonDecode(response.body);
    var taskList = Service_Lists.fromJson(taskListMap);

    //print(taskList.data[0].results.length);
    for(int i=0; i<taskList.data[0].results.length; i++){

      var id = taskList.data[0].results[i].services.id;
      var date = taskList.data[0].results[i].services.createdAt;
      var title = taskList.data[0].results[i].services.serviceTitle;
      var priority = taskList.data[0].results[i].services.priority;
      var name = taskList.data[0].results[i].services.userName;
      var status = taskList.data[0].results[i].services.status;
      var department = taskList.data[0].results[i].services.departmentName;
      var progress = taskList.data[0].results[i].services.progress;
      var ticket_status_type_id = taskList.data[0].results[i].services.ticketStatusTypeId;
      var is_service_complete = taskList.data[0].results[i].services.isVerifyByJobAdmin;
      var dateFormatter = new DateFormat('yyyy-MM-dd');
      var percent = double.parse(taskList.data[0].results[i].services.progress) / 100;
     // print(percent);
      var image = taskList.data[0].results[i].services.userProfile;

      ticketlist.add(new TicketLists(id,date,title,priority,name,department,progress,image,percent,status,ticket_status_type_id,is_service_complete));

    }

    setState(() {

      for(int j=0; j<ticketlist.length; j++){

        if(ticketlist[j].ticket_status_type_id.contains(id)){

          int index = j;
          //ticketlist.clear();
          //print(j);

          var ids = taskList.data[0].results[index].services.createdAt;
          var dates = taskList.data[0].results[index].services.createdAt;
          var titles = taskList.data[0].results[index].services.serviceTitle;
          var prioritys = taskList.data[0].results[index].services.priority;
          var names = taskList.data[0].results[index].services.userName;
          var statuss = taskList.data[0].results[index].services.status;
          var departments = taskList.data[0].results[index].services.departmentName;
          var progresss = taskList.data[0].results[index].services.progress;
          var is_service_complete = taskList.data[0].results[index].services.isVerifyByJobAdmin;
          var ticket_status_type_ids = taskList.data[0].results[index].services.ticketStatusTypeId;
          var dateFormatter = new DateFormat('yyyy-MM-dd');
          var percents = double.parse(taskList.data[0].results[index].services.progress) / 100;
          //print(percent);
          var images = taskList.data[0].results[index].services.userProfile;

          setState(() {

            ticketlist.add(new TicketLists(ids,dates,titles,prioritys,names,departments,progresss,images,percents,statuss,ticket_status_type_ids,is_service_complete));
            print(ticketlist.length);
          });

        }
      }
    });
    return ticketlist;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _color1 = true;
    _color2 = true;
    _color3 = true;
    _color4 = true;
    getList();
  }


  @override
  Widget build(BuildContext context) {

    return Column(
      children: <Widget>[
        SizedBox(
           height: 15,
        ),
        Container(
          height: 50,
          margin: EdgeInsets.only(left: 15,right: 15),
          child: TextField(
            style: TextStyle(color: Color(0xff99FFFFFF)),
            autocorrect: true,
            decoration: InputDecoration(
              hintText: 'Search Requestor...',
              prefixIcon: Icon(Icons.search,color: Color(0xff99FFFFFF),),
              hintStyle: TextStyle(color: Color(0xff99FFFFFF)),
              filled: true,
              fillColor: Color(0xff5263F5),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                borderSide: BorderSide(color: Color(0xff3BFFFFFF), width: 1),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                borderSide: BorderSide(color: Color(0xff3BFFFFFF), width: 1),
              ),
            ),),
        ),
        SizedBox(
          height: 15.0,
        ),
        Container(
          width: 327,
          height: 37,
          decoration: BoxDecoration(
            border: Border.all(
              color: Color(0xff3BFFFFFF),
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              GestureDetector(
                child : _color1 ? Container(
                  child: Text(
                    'All',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 58,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'All',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                   setState(() {
                      getList();
                      _color1 = !_color1;
                      _color2 = true;
                      _color3 = true;
                      _color4 = true;
                    });
                },
              ),
              GestureDetector(
                child : _color2 ? Container(
                  child: Text(
                    'Pending',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 62,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'Pending',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                  setState(() {
                    getFilteredList("4");
                    _color2 = !_color2;
                    _color1 = true;
                    _color3 = true;
                    _color4 = true;
                  });
                },
              ),
              GestureDetector(
                child : _color3 ? Container(
                  child: Text(
                    'Ongoing',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 62,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'Ongoing',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                    setState(() {
                      getFilteredList("3");
                      _color3 = !_color3;
                      _color2 = true;
                      _color1 = true;
                      _color4 = true;
                    });
                },
              ),

              GestureDetector(
                child : _color4 ? Container(
                  child: Text(
                    'Completed',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFFFFFF),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ) : Container(
                  height: 35,
                  width: 80,
                  decoration: BoxDecoration(
                      color: Color(0xff33FFFFFF),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0),
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0)
                      )
                  ),

                  child: Center(
                    child: Text(
                      'Completed',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ),
                ),
                onTap: (){
                    setState(() {
                      getFilteredList("6");
                      _color4 = !_color4;
                      _color2 = true;
                      _color3 = true;
                      _color1 = true;
                    });
                },
              ),

            ],
          ),
        ),
        SizedBox(
          height: 30.0,
        ),
        Expanded(
            child: Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)
                    )
                ),

                child: Container(
                        child: SingleChildScrollView(
                            child: Stack(
                              children: [
                                Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,

                                      children: [
                                        Text("All Service",
                                            style: TextStyle(fontSize: 20,color: Color(0xff1E2661))),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: ticketlist.length,
                                        itemBuilder: (BuildContext context, int index){
                                          return GestureDetector(
                                            onTap: (){
                                              serviceid = ticketlist[index].id;
                                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskListJobAdmin(serviceid:serviceid)));
                                            },
                                            child: Card(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  Column(
                                                    children: <Widget>[
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            margin : EdgeInsets.only(left: 15,top: 5),
                                                            height: 5,
                                                            width: 5,
                                                            decoration: BoxDecoration(
                                                              color: ticketlist[index].priority == "Medium" ? Colors.blueAccent : ticketlist[index].priority=="High" ? Colors.pinkAccent : Colors.deepPurple,
                                                              border: ticketlist[index].priority == "Medium" ? Border.all(color: Colors.blueAccent) : ticketlist[index].priority=="High" ? Border.all(color: Colors.pinkAccent) : Border.all(color: Colors.deepPurple),
                                                              borderRadius: BorderRadius.all(Radius.circular(8)),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin : EdgeInsets.only(left: 5,top: 5),
                                                            child: Text(ticketlist[index].priority, style: ticketlist[index].priority=="Medium" ? TextStyle(fontSize: 13,color: Color(0xff3DA7F2)) : ticketlist[index].priority=="High" ? TextStyle(fontSize: 13,color: Color(0xffFF6673)) : TextStyle(fontSize: 13,color: Color(0xff5972FF))),
                                                          )
                                                        ],
                                                      ),
                                                      Divider(),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: <Widget>[
                                                          Container(
                                                            margin : EdgeInsets.only(left: 15,top: 15),
                                                            height: 15,
                                                            width: 15,
                                                            decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.all(Radius.circular(8)),
                                                              image: DecorationImage(
                                                                  image: AssetImage(
                                                                      'assets/images/settings.png'),
                                                                  fit: BoxFit.contain
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                              margin : EdgeInsets.only(left: 15,top: 15),
                                                              child: Text("SER"+ticketlist[index].id)
                                                          ),

                                                          Spacer(),
                                                          Padding(
                                                            padding: const EdgeInsets.only(top: 10,right: 15),
                                                            child: Container(
                                                              child: CircularPercentIndicator(
                                                                radius: 50.0,
                                                                lineWidth: 5.0,
                                                                percent: ticketlist[index].percent,
                                                                center: new Text(ticketlist[index].progress),
                                                                progressColor: Color(0xff2DCED6),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: <Widget>[
                                                          Container(
                                                            margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                            height: 15,
                                                            width: 15,
                                                            decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.all(Radius.circular(8)),
                                                              image: DecorationImage(
                                                                  image: AssetImage(
                                                                      'assets/images/time.png'),
                                                                  fit: BoxFit.contain
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                              margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                              child: Text('Assign Date:')
                                                          ),
                                                          Container(
                                                              margin : EdgeInsets.only(left: 5,top: 5,bottom: 15),
                                                              child: Text('-')
                                                          ),
                                                          Container(
                                                              margin : EdgeInsets.only(left: 5,top: 5,bottom: 15),
                                                              child: Text(ticketlist[index].date)
                                                          )
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            margin : EdgeInsets.only(left: 15),
                                                            child: Text(
                                                              ticketlist[index].title,
                                                              style: TextStyle(
                                                                  fontSize: 16.0,
                                                                  color: Color(0xff1E2661)
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                  Divider(),
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Container(
                                                              height: 34,
                                                              width: 34,
                                                              decoration: BoxDecoration(
                                                                //color: Colors.yellow,
                                                                //border: Border.all(color: Colors.yellow),
                                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                image: DecorationImage(
                                                                    image: NetworkImage(ticketlist[index].image)
//                                                          image: AssetImage(
//                                                              'assets/images/ic_data.png'),
//                                                          fit: BoxFit.contain
                                                                ),
                                                              ),
                                                            ),
                                                             Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  Text(ticketlist[index].name, style: TextStyle(fontSize: 15,color: Color(0xff1E2661))),
                                                                  Text(ticketlist[index].department, style: TextStyle(fontSize: 13))
                                                                ],
                                                             )
                                                          ],
                                                        ),

                                                        Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            Row(
                                                                children : [
                                                                  ticketlist[index].status == "Completed" && ticketlist[index].is_service_complete == "1" ? RaisedButton(
                                                                    elevation: 0,
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius: BorderRadius.circular(5.0),
                                                                        side: BorderSide(color: Color(0xffEB5757))),
                                                                    onPressed: () {
                                                                      serviceid = ticketlist[index].id;
                                                                      print(serviceid);
                                                                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                                                                          RequestSend(serviceid:serviceid)));
                                                                    },
                                                                    color: Color(0xffFCE6E6),
                                                                    textColor: Color(0xffEB5757),
                                                                    child: Text('Verify', style: TextStyle(fontSize: 11)),
                                                                  ) : new Container(),

                                                                  Container(
                                                                    margin : EdgeInsets.only(left: 5,right: 10),
                                                                    child: RaisedButton(
                                                                      elevation: 0,
                                                                      shape: RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(5.0),
                                                                          side: BorderSide(color: ticketlist[index].status == "Pending" ? Color(0xffEB5757) : ticketlist[index].status == "Completed" ? Color(0xff27AE60) : Color(0xffA880E3))),
                                                                      onPressed: () {},
                                                                      color: ticketlist[index].status == "Pending" ? Color(0xffFCE6E6) : ticketlist[index].status == "Completed" ? Color(0xffDFF3E7) : Color(0xffF2ECFB),
                                                                      textColor: ticketlist[index].status == "Pending" ? Color(0xffEB5757) : ticketlist[index].status == "Completed" ? Color(0xff27AE60) : Color(0xffA880E3),
                                                                      child: Text(ticketlist[index].status.toUpperCase(), style: TextStyle(fontSize: 11)),
                                                                    ),
                                                                  ),
                                                                ]
                                                            ),
                                                          ],

                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        }
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 85),
                                  decoration: BoxDecoration(
                                      color: Colors.greenAccent,
                                      borderRadius: BorderRadius.all(Radius.circular(20))
                                  ),
                                  height: 100,
                                  width: 5,
                                ),
                              ],

                            ),
                          )
                 )
            )

        ),
      ],
    );
  }
}

class TicketLists {
  final String id;

  final String date;
  final String status;
  final String title;

  final String name,department,progress,image,priority,ticket_status_type_id,is_service_complete;
  double percent;

  TicketLists(this.id, this.date, this.title, this.priority,this.name, this.department, this.progress, this.image, this.percent,this.status, this.ticket_status_type_id,this.is_service_complete);
}

class UserDetailPage extends StatelessWidget {
  final TicketLists user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name),
      ),
    );
  }
}
