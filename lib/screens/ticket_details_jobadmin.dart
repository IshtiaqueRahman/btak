import 'dart:io';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/db/dbhelper.dart';
import 'package:btakapps/model/categorywise_service.dart';
import 'package:btakapps/model/create_job.dart';
import 'package:btakapps/model/service_details.dart';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/model/listfromcategory.dart';
import 'package:btakapps/model/servicecategory.dart';
import 'package:btakapps/screens/tasklist_jobadmin.dart';
import 'package:btakapps/screens/team_member.dart';
import 'package:btakapps/screens/teammember.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TicketDetails extends StatelessWidget {
  String serviceid,isjobassigned;
  // This widget is the root of your application.

  TicketDetails({this.serviceid,this.isjobassigned});

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Color(0xff3D4FF4),
          appBar: AppBar(
             title: Column(
               children: [
                 Text('Service Details'),
               ],
             ),
             backgroundColor: Color(0xff3D4FF4),
             elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(serviceid,isjobassigned),
      );
  }
}

class Body extends StatefulWidget {
  String serviceid,isjobassigned;

  Body(String serviceid,String isjobassigned){
     this.serviceid = serviceid;
     this.isjobassigned = isjobassigned;
  }


  @override
  _BodyState createState() => _BodyState(serviceid,isjobassigned);
}

class _BodyState extends State<Body> {
  String title = 'DropDownButton';
  String val;
  List _list = List();
  File imageFile;
  DateTime _setDate = DateTime.now();
  Duration initialtimer = new Duration();
  int selectitem = 1;
  List<Todo> taskList = new List();
  List<RadioModel> sampleData = new List<RadioModel>();
  var servicedetails;

  var date,departmentname,description,priority_type,servicetitle;
  List tasklist = List();
  var user;
  List _listdata = List();
  List<String> isChecked = [];
  List<String> idList = [];
  List _listid = List();

  String serviceid,isjobassigned;

  _BodyState(String serviceid, String isjobassigned){
     this.serviceid = serviceid;
     this.isjobassigned = isjobassigned;
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    getData();
    getCategory();
    getlistView(0);

    sampleData.add(new RadioModel(false, 'A', 'April 18'));
    sampleData.add(new RadioModel(false, 'B', 'April 17'));
    sampleData.add(new RadioModel(false, 'C', 'April 16'));
    sampleData.add(new RadioModel(false, 'D', 'April 15'));


    DatabaseHelper.instance.queryAllRows().then((value) {
      setState(() {
        value.forEach((element) {
          taskList.add(Todo(id: element['id'], title: element["title"]));
          //print(taskList.length);
        });
      });
    }).catchError((error) {
      //print(error);
    });
  }

  Future getData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

   // print(userid);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "service_id":serviceid
    };

    print(body);
    final response =
    await http.post(BaseUrl.baseurl+"service_details.json",body: json.encode(body));

    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    Map serviceDetialMap = jsonDecode(response.body);
    servicedetails = ServiceDetailsClass.fromJson(serviceDetialMap);

    setState(() {
        date = servicedetails.data[0].serviceDetails.requestDate.toString();
        servicetitle = servicedetails.data[0].serviceDetails.serviceTitle.toString();
        departmentname = servicedetails.data[0].serviceDetails.departmentName.toString();
        description = servicedetails.data[0].serviceDetails.description.toString();
        serviceid = servicedetails.data[0].serviceDetails.id.toString();
        priority_type = servicedetails.data[0].serviceDetails.priorityTypeId.toString();

        print(departmentname);

        for(int i=0; i<response.body.length; i++){
           tasklist.add(servicedetails.data[0].serviceDetails.taskList[i].task.toString());

           print(servicedetails.data[0].serviceDetails.taskList[i].task.toString());
           getid(servicedetails.data[0].serviceDetails.taskList[i].task.toString());
           isChecked.add(servicedetails.data[0].serviceDetails.taskList[i].task.toString());
        }
    });
    //print(servicedetails.data[0].serviceDetails.taskList[0].task.toString());

  }

  Future getCategory() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

   // print(userid);

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
    };

    final response =
    await http.post(BaseUrl.baseurl+"service_category.json",body: json.encode(body));

    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    Map categoryMap = jsonDecode(response.body);
    var categorylist = CategoryService.fromJson(categoryMap);

    setState(() {
      for(int i=0; i<categorylist.data[0].results.length; i++){
        _list.add(categorylist.data[0].results[i].serviceCategory.name);
      }
    });
    //print(servicedetails.data[0].serviceDetails.taskList[0].task.toString());
  }


  Future getlistView(int position) async{
    _listdata.clear();
    // print(val);
    int _currentTimeValue = 1;
    List _list = List();

    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      'lang': "en",
    };

    final response = await http.post(BaseUrl.baseurl+"service_list_category_wise.json",body: json.encode(body));

    //print(response.body);

    final jsonresp = json.decode(response.body);

    Map userMap = jsonDecode(response.body);
    user = CategorywiseService.fromJson(userMap);

    for(int i=0; i<response.body.length; i++){
      setState(() {

        //getData(user);
        _listdata.add(user.data[0].services[position].serviceList[i].name);


        // print(user.data[0].services[position].serviceList[i].name);
        //_listdata.add(user.data[0].services[position].serviceList[i].name);
        // var a=user.data[0].services[position].serviceList[i].id;
      });

    }

    return _listdata;

//    if(val != null){
//      return ListView(
//        primary: false,
//        shrinkWrap: true,
//        padding: EdgeInsets.all(12.0),
//        children: _listdata.map((timeValue) => RadioListTile(
//          groupValue: _currentTimeValue,
//          title: Text(timeValue._value),
//          value: timeValue._key,
//          onChanged: (val) {
//            setState(() {
//              debugPrint('VAL = $val');
//              _currentTimeValue = val;
//            }
//            );
//          },
//        )).toList(),
//      );
//    }else{
//      return new Container();
//    }

  }

  Future getlist(int categoryid) async{
    _listdata.clear();
    // print(val);
    int _currentTimeValue = 1;
    List _list = List();

    Map<String, dynamic> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      "category_id" : categoryid,
      "service_id": serviceid
    };

    print(body);
    final response = await http.post(BaseUrl.baseurl+"service_list_depend_category.json",body: json.encode(body));

    //print(response.body);

    final jsonresp = json.decode(response.body);

    Map userMap = jsonDecode(response.body);
    user = ListFromCategory.fromJson(userMap);

    for(int i=0; i<user.data[0].results.length; i++){
      setState(() {

        //getData(user);
        _listdata.add(user.data[0].results[i].serviceList.name);


        // print(user.data[0].services[position].serviceList[i].name);
        //_listdata.add(user.data[0].services[position].serviceList[i].name);
        // var a=user.data[0].services[position].serviceList[i].id;
      });

    }

    return _listdata;

//    if(val != null){
//      return ListView(
//        primary: false,
//        shrinkWrap: true,
//        padding: EdgeInsets.all(12.0),
//        children: _listdata.map((timeValue) => RadioListTile(
//          groupValue: _currentTimeValue,
//          title: Text(timeValue._value),
//          value: timeValue._key,
//          onChanged: (val) {
//            setState(() {
//              debugPrint('VAL = $val');
//              _currentTimeValue = val;
//            }
//            );
//          },
//        )).toList(),
//      );
//    }else{
//      return new Container();
//    }

  }

  Future saveData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");
    var office_id = pref.get("office_id");
    var departmentid = pref.get("department_id");

    // print(userid);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "service_id" : serviceid,
      "user_id" : userid,
      "office_id" : office_id,
      "requisition_department_id" : departmentid,
      "priority_type_id" : priority_type,
      "service_type" : _listid,
      "other_comments" : "Other Commets"
    };

    print(body);

    final response =
    await http.post(BaseUrl.baseurl+"new_job_create.json",body: json.encode(body));

    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    Map jobcreateMap = jsonDecode(response.body);
    var jobcreate = JobCreate.fromJson(jobcreateMap);

    print(jobcreate.data[0].status);
    //print(servicedetails.data[0].serviceDetails.taskList[0].task.toString());

  }

  String getDataForId(String name){

    for(int i=0; i<user.data[0].services[0].serviceList.length; i++){
      if(user.data[0].services[0].serviceList[i].name==name.trim()){
        //print();
        _listid.add(user.data[0].services[0].serviceList[i].id);
      }
    }



  }

  String getid(String name){

    //print(servicedetails.data[0].serviceDetails.taskList[0].id);

    for(int i=0; i<servicedetails.data[0].serviceDetails.taskList.length; i++){
      if(servicedetails.data[0].serviceDetails.taskList[i].task==name.trim()){
        print(servicedetails.data[0].serviceDetails.taskList[i].id);
        _listid.add(servicedetails.data[0].serviceDetails.taskList[i].id);
      }
    }
  }

  @override
  Widget build(BuildContext context) {


    var height = MediaQuery.of(context).size;

    DateTime _fromDate = DateTime.now();
    TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());
    
    // Future<void> _showDatePicker(BuildContext context) async {
    //   final picked = await showDatePicker(
    //     context: context,
    //     initialDate: _fromDate,
    //     firstDate: DateTime(2015, 1),
    //     lastDate: DateTime(2100),
    //   );
    //   if (picked != null && picked != _fromDate) {
    //     setState(() {
    //       _fromDate = picked;
    //     });
    //   }
    // }
    //
    // Future<void> _showTimePicker(BuildContext context) async {
    //   final picked = await showTimePicker(
    //     context: context,
    //     initialTime: _fromTime,
    //   );
    //   if (picked != null && picked != _fromTime) {
    //     setState(() {
    //       _fromTime = picked;
    //     });
    //   }
    // }

    Widget datetime() {
      return CupertinoDatePicker(
        initialDateTime: DateTime.now(),
        onDateTimeChanged: (DateTime newdate) {
         // print(newdate);
        },
        use24hFormat: true,
        maximumDate: new DateTime(3500, 12, 30),
        minimumYear: 2020,
        maximumYear: 3500,
        minuteInterval: 1,
        mode: CupertinoDatePickerMode.dateAndTime,
      );
    }

    Widget time() {
      return CupertinoTimerPicker(
        mode: CupertinoTimerPickerMode.hms,
        minuteInterval: 1,
        secondInterval: 1,
        initialTimerDuration: initialtimer,
        onTimerDurationChanged: (Duration changedtimer) {
          setState(() {
            initialtimer = changedtimer;
          });
        },
      );
    }

    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];
    return SingleChildScrollView(

      child:  Container(
        color: Color(0xff3D4FF4),
        child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.04,
                  margin: EdgeInsets.only(top: 10),
                  child: Text(
                    servicetitle ?? '',
                    style: TextStyle(
                        color: Color(0xffFFFFFF),
                        fontSize: 24.0,
                        fontWeight: FontWeight.normal
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 3,
                  decoration: BoxDecoration(
                      color: kShadowColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0)
                      )
                  ),
                  child: Expanded(
                    child: SizedBox(
                      height: double.infinity,
                      child: Column(
                          children: <Widget>[
                          SizedBox(
                          height: 10,
                        ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Wrap(
                                  children: [
                                    new Container(
                                      child: new Image.asset(
                                        'assets/images/clock.png',
                                        height: 17.0,
                                        width: 17.0,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 25),
                                      child: Text(
                                        'Request Date',
                                        style: TextStyle(
                                            color: Color(0xff878DBA),
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                                GestureDetector(
                                  child: Text(
                                    date ?? '',
                                    style: TextStyle(
                                        color: Color(0xff1E2661),
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal
                                    ),
                                  ),
                                  onTap: (){
                                    _bottomSheetMore(context);
                                  },
                                ),
                              ],
                            )
                        ),
                          Container(
                            height: 0.2,
                            color: Colors.grey,
                          ),
                          Container(
                              padding: EdgeInsets.all(20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Wrap(
                                    children: [
                                      new Container(
                                        child: new Image.asset(
                                          'assets/images/department.png',
                                          height: 17.0,
                                          width: 17.0,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 25),
                                        child: Text(
                                          'Department',
                                          style: TextStyle(
                                              color: Color(0xff878DBA),
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),

                                  GestureDetector(
                                      child: Text(
                                        departmentname ?? '',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color: Color(0xff1E2661),
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal
                                        ),
                                      ),
                                      onTap: () {

                                        //_showDatePicker(context);
                                        // DatePicker.showDatePicker(context,
                                        // showTitleActions: true,
                                        // minTime: DateTime(2018, 3, 5),
                                        // maxTime: DateTime(5000, 6, 7), onChanged: (date) {
                                        // print('change $date');
                                        // }, onConfirm: (date) {
                                        // print('confirm $date');
                                        // }, currentTime: DateTime.now(), locale: LocaleType.en);
                                        // },
                                        showModalBottomSheet(

                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.vertical(
                                                  top: Radius.circular(20.0))),
                                          backgroundColor: Colors.white,
                                          context: context,
                                          isScrollControlled: true,
                                          builder: (context) =>
                                              Container(
                                                height: 400,
                                                child: Padding(
                                                    padding: const EdgeInsets.symmetric(
                                                        horizontal: 18),
                                                    child: datetime()),
                                              ),
                                        );
                                      }
                                  ),
                                ],
                              )
                          ),
                          Container(
                            height: 0.2,
                            color: Colors.grey,
                          ),


                            SizedBox(
                            height: 5,
                          ),

                          Container(
                            padding: EdgeInsets.all(20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  child: new Image.asset(
                                    'assets/images/description.png',
                                    height: 17.0,
                                    width: 17.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20),
                                  child: Text(
                                    'Description',
                                    style: TextStyle(
                                        color: Color(0xff878DBA),
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 20,top: 3,right: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  description ?? '',
                                  style: TextStyle(
                                      color: Color(0xff1E2661),
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.normal
                                  ),
                                ),
                              ],
                            ),
                          ),

                            SizedBox(
                              height: 15,
                            ),

                            Container(
                              height: 0.2,
                              color: Colors.grey,
                            ),

                            Container(
                              padding: EdgeInsets.all(20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Wrap(
                                    children: [
                                      new Container(
                                        child: new Image.asset(
                                          'assets/images/service.png',
                                          height: 17.0,
                                          width: 17.0,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 25),
                                        child: Text(
                                          'Task',
                                          style: TextStyle(
                                              color: Color(0xff878DBA),
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),


                                ],
                              ),
                            ),

                            Container(
                                child: Wrap(
                                  children: [
                                    _myListView(context)
                                  ],
                                )
                            ),


                               Container(
                                 margin: EdgeInsets.only(left: 35),
                                 child: Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                   children: [
                                     SizedBox(
                                      width: 145,
                                      height: 38,
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(18.0),
                                            side: BorderSide(color: Color(0xff2DCED6))
                                        ),
                                        onPressed: () {},
                                        color: Color(0xff2DCED6),
                                        textColor: Colors.white,
                                        child: Text("Add more task".toUpperCase(),
                                            style: TextStyle(fontSize: 14)),
                                      ),
                              ),
                                   ],
                                 ),
                               ),

                            SizedBox(
                              height: 20,
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 10,left: 35),
                                  height: 42,
                                  width: 250,
                                  decoration: BoxDecoration(
                                      color: Color(0xff0D444444),
                                      border: Border.all(
                                          color: Color(0xff0D444444)
                                      ),
                                      borderRadius: BorderRadius.circular(8.0)
                                  ),
                                  child: Center(
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        value: val,
                                        hint: Text('Select Service Category'),
                                        style: TextStyle(
                                            color: Colors.black38
                                        ),
                                        elevation: 5,
                                        icon: Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: Icon(Icons.arrow_drop_down)),
                                        iconSize: 20.0,
                                        dropdownColor: Color(0xffF5F5F5),
                                        onChanged: (value) {
                                          setState(() {
                                            val = value;
                                            if(val == "Ac"){
                                              getlist(1);
                                            }
                                            else if(val == "TV"){
                                              getlist(2);
                                            }
                                          });
                                        },
                                        items: _list.map((value){
                                          return DropdownMenuItem(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),


                            SizedBox(
                               height: 20,
                            ),


                                    Expanded(
                                      child: ListView(
                                            shrinkWrap: true,
                                            children: <Widget>[
                                              ..._listdata
                                                  .map(
                                                    (item) => CheckboxListTile(
                                                  title: Text(item),
                                                  value: isChecked.contains(item),
                                                  onChanged: (bool value) {
                                                    if (value) {
                                                      setState(() {
                                                        isChecked.add(item);
                                                        getDataForId(item);
                                                      });
                                                    } else {
                                                      setState(() {
                                                        isChecked.remove(item);
                                                      });
                                                    }

                                                  },
                                                ),
                                              )
                                                  .toList()
                                            ],
                                          ),
                                    ),


                                    //getlistView()

                            Container(
                            padding: EdgeInsets.all(20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Container(
                                  child: new Image.asset(
                                    'assets/images/progress.png',
                                    height: 17.0,
                                    width: 17.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),

                                Container(
                                    margin: EdgeInsets.only(right: 120),
                                    child: Text(
                                      'Assigned to',
                                      style: TextStyle(
                                          color: Color(0xff878DBA),
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.normal
                                      ),
                                    ),
                                  ),
                                GestureDetector(
                                  child: Container(
                                    height: 35,
                                    width: 35,
                                    child: Image.asset('assets/images/assignedto.png'),
                                  ),
                                  onTap: (){
                                    if(isChecked.isEmpty){
                                      saveData();
                                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskListJobAdmin(listid:_listid,serviceid:serviceid)));
                                    }else{
                                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskListJobAdmin(listid:_listid,serviceid:serviceid)));
                                    }

                                  },
                                ),
                              ],
                            )
                        ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                SizedBox(
                                  width: 132,
                                  height: 48,
                                  child: RaisedButton(
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(12.0),
                                        side: BorderSide(color: Color(0xff2DCED6))
                                    ),
                                    onPressed: () {
                                      //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyRental()));
                                    },
                                    color: Color(0xffE5F5F9),
                                    textColor: Color(0xff2DCED6),
                                    child: Text("Received".toUpperCase(),
                                        style: TextStyle(fontSize: 14)),
                                  ),
                                ),
                                SizedBox(
                                  width: 132,
                                  height: 48,
                                  child: isjobassigned== "2" ? RaisedButton(
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(12.0),
                                        side: BorderSide(color: Color(0xffEB5757))
                                    ),
                                    onPressed: () {
                                      //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyRental()));
                                    },
                                    color: Color(0xffF7E9ED),
                                    textColor: Color(0xffEB5757),
                                    child: Text("Reject".toUpperCase(),
                                        style: TextStyle(fontSize: 14)),
                                  ) : new Container()
                                ),
                              ],
                            ),

                             SizedBox(
                               height: 20,
                             )
//                          Flexible(
//                            child: Container(
//                              padding: EdgeInsets.zero,
//                              child: taskList.isEmpty
//                                  ? Container()
//                                  : ListView.builder(
//                                  scrollDirection: Axis.horizontal,
//                                  itemCount: taskList.length,
//                                  itemBuilder: (ctx, index) {
//                                if (index == taskList.length) return null;
//                                return Flexible(
//                                  child: Image.asset(
//                                      taskList[index].title,
//                                      height: 50,
//                                      width: 50,
//                                  ),
//                                );
//                                // return ListTile(
//                                //   title: Container(
//                                //       height: 50,
//                                //       width: 50,
//                                //       child: Image.asset(taskList[index].title)),
//                                // );
//                              }),
//                            ),
//                          ),
//                        Container(
//                        height: 0.2,
//                        color: Colors.grey,
//                      ),

                        ],
                    ),
                      ),
                  ),
                ),
                //child: Text('This is text'),
               ],
            ),
         ),
      );
  }

  _decideImage() {
    if(imageFile == null){
      return Text('No image selected');
    }else{
      return Expanded(
        child: Image.file(
          imageFile,
          width: 130,
          height: 102,
          fit: BoxFit.cover,
        ),
      );
    }
  }

   _pickDateDialog(BuildContext context) {
    DateTime _selectedDate;
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        //which date will display when user open the picker
        firstDate: DateTime(1950),
        //what will be the previous supported year in picker
        lastDate: DateTime
            .now()) //what will be the up to supported date in picker
        .then((pickedDate) {
      //then usually do the future job
      if (pickedDate == null) {
        //if user tap cancel then this function will stop
        return;
      }
      setState(() {
        //for rebuilding the ui
        _selectedDate = pickedDate;
      });
    });
  }

  Widget _myListView(BuildContext context) {
    return Wrap(
      children: [
     ListView.builder(
       shrinkWrap: true,
    itemCount: tasklist.length,
      itemBuilder: (context, int i) => Column(
        children: [
          new ListTile(
            title: new Text(tasklist[i].toString()),
            onTap: (){},
            onLongPress: (){
              print(
                Text("Long Pressed"),
              );
            },
          ),
        ],
      ),
    )
      ],
    );
  }




}

class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  bool _value = true;

  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Container(
        margin: EdgeInsets.only(left: 25,top: 10),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        Center(
        child: InkWell(
          child: Container(
          decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xff2DD8CF)),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: _value
                ? Icon(
              Icons.check,
              size: 13.0,
              color: Colors.white,
            )
                : Icon(
              Icons.check_box_outline_blank,
              size: 10.0,
              color: Color(0xff2DD8CF),
            ),
          ),
        ),
      )),

            new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(_item.text,style: TextStyle(
                  fontSize: 14
              ),),
            )
          ],
        ),
      ),
    );
  }
}

void _bottomSheetMore(BuildContext context) {
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}




