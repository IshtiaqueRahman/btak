import 'dart:convert';
import 'dart:io';

import 'package:btakapps/apiBase/base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:btakapps/model/forget_password.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'create_new_task_technician1.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  String title = 'DropDownButton';
  String serviceName,departmentName;
  //List _list = ['Computer','Electrical','HR'];

  File imageFile;
  List datalist = List(); //edited line
  List _list = List();
  String text;
  var status;
  TextEditingController mobileno = new TextEditingController();

  @override
  void initState() {
    // print("called primary");
    super.initState();
  }

//
  Future<String> _ForgetPass(String mobileno) async {
    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "mobile_number" : mobileno
    };

    final response =
    await http.post(BaseUrl.baseurl+"user_forget_password.json",body: json.encode(body));

    final jsonresp = json.decode(response.body.toString());

    Map taskListMap = jsonDecode(response.body);
    var data = Forget_Password.fromJson(taskListMap);

    status = data.data[0].status;
    _showToast(context);
    //print(taskList.data[0].status);

    return "Sucess";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Forgot Password'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        margin: EdgeInsets.only(top: 25),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 18,
                    width: 18,
                    child: Image.asset('assets/images/reqname.png'),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Service Name',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff1E2661),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                ],
              ),
              Container(
                  width: 320,
                  height: 60,
                  margin: EdgeInsets.only(top: 10,left: 25),
                  padding: EdgeInsets.all(6.0),
                  child: TextField(
                    controller: mobileno,
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: 'Phone Number',
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Color(0xffF5F5F5),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
                      ),
                    ),)
              ),

              SizedBox(
                height: 22,
              ),

              Container(
                margin: EdgeInsets.only(right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                      width: 122,
                      height: 48,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            side: BorderSide(color: Color(0xff2DCED6))
                        ),
                        onPressed: () {
                          _ForgetPass(mobileno.text);
                          //print(serviceName);
                          //print(departmentName);
                          //Navigator.push(context, new MaterialPageRoute(builder: (context) => new CreateNewTask1(serviceName: service.text.toString(),departmentName: departmentName)));
                        },
                        color: Color(0xff2DCED6),
                        textColor: Color(0xffFFFFFF),
                        child: Text("Next".toUpperCase(),
                            style: TextStyle(fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 15,
              ),

            ],
          ),
        ),
      ),
      //body: ListShow(),
    );
  }

  void _showToast(BuildContext context) {
    if(status==1){
      Fluttertoast.showToast(
        msg: "Successfully changed password..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }else{
      Fluttertoast.showToast(
        msg: "Error changing password..",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }

  }
}





Widget chip(String label, Color color) {
  return Chip(
    labelPadding: EdgeInsets.all(3.0),
    avatar: Container(
      height: 5,
      width: 5,
      child: MaterialButton(
        onPressed: () {},
        color: Colors.blue,
        textColor: Colors.white,
        padding: EdgeInsets.all(15),
        shape: CircleBorder(),
      ),
    ),
    label: Text(
      label,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    backgroundColor: color,
    elevation: 6.0,
    shadowColor: Colors.grey[60],
    padding: EdgeInsets.all(6.0),
  );
}