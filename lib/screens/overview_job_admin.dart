import 'dart:core';
import 'dart:core';
import 'dart:io';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/screens/task_details.dart';
import 'package:btakapps/screens/team_member.dart';
import 'package:btakapps/screens/teammember.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';

class Overview extends StatefulWidget {
  @override
  _OverviewState createState() => _OverviewState();
}

class _OverviewState extends State<Overview> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1','list2','list3'];
  File imageFile;

  final List<String> values = <String>['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
  String selectedValue = 'A';

  var titleEdt = <TextEditingController>[];
  var descriptionEdt = <TextEditingController>[];
  var cards = <Card>[];
  List<Todo> taskList = new List();


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff6C5DD3),
       child:  Expanded(
         child: Container(
           width: double.infinity,
           height: 1000,
           decoration: BoxDecoration(
               color: kShadowColor,
               borderRadius: BorderRadius.only(
                   topLeft: Radius.circular(30.0),
                   topRight: Radius.circular(30.0)
               )
           ),
           child: SingleChildScrollView(
             child: Column(
               children: <Widget>[
                 Container(
                     padding: EdgeInsets.all(20),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         new Container(
                           child: new Image.asset(
                             'assets/images/clock.png',
                             height: 17.0,
                             width: 17.0,
                             fit: BoxFit.cover,
                           ),
                         ),
                         Text(
                           'Requested Date',
                           style: TextStyle(
                               color: Color(0xff878DBA),
                               fontSize: 16.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                         GestureDetector(
                           child: Text(
                             '19 January at 10:00',
                             style: TextStyle(
                                 color: Color(0xff1E2661),
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.normal
                             ),
                           ),
                           onTap: (){
                             _bottomSheetMore(context);
                           },
                         ),
                       ],
                     )
                 ),
                 Container(
                   height: 0.2,
                   color: Colors.grey,
                 ),
                 Container(
                     padding: EdgeInsets.all(20),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         new Container(
                           child: new Image.asset(
                             'assets/images/clock.png',
                             height: 17.0,
                             width: 17.0,
                             fit: BoxFit.cover,
                           ),
                         ),
                         Container(
                           margin: EdgeInsets.only(right: 75),
                           child: Text(
                             'Department',
                             style: TextStyle(
                                 color: Color(0xff878DBA),
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.normal
                             ),
                           ),
                         ),
                         GestureDetector(
                           child: Text(
                             'Finance',
                             textAlign: TextAlign.right,
                             style: TextStyle(
                                 color: Color(0xff1E2661),
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.normal
                             ),
                           ),
                           onTap: (){
                             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskDetails()));
                           },
                         ),
                       ],
                     )
                 ),
                 Container(
                   height: 0.2,
                   color: Colors.grey,
                 ),
                 Container(
                     padding: EdgeInsets.all(20),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         new Container(
                           child: new Image.asset(
                             'assets/images/status.png',
                             height: 17.0,
                             width: 17.0,
                             fit: BoxFit.cover,
                           ),
                         ),
                         Container(
                           margin: EdgeInsets.only(right: 100),
                           child: Text(
                             'Category',
                             style: TextStyle(
                                 color: Color(0xff878DBA),
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.normal
                             ),
                           ),
                         ),
                         Text(
                           'Submit TIN',
                           textAlign: TextAlign.right,
                           style: TextStyle(
                               color: Color(0xff1E2661),
                               fontSize: 16.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                       ],
                     )
                 ),
                 Container(
                   height: 0.2,
                   color: Colors.grey,
                 ),
                 Container(
                     padding: EdgeInsets.all(20),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         new Container(
                           child: new Image.asset(
                             'assets/images/progress.png',
                             height: 17.0,
                             width: 17.0,
                             fit: BoxFit.cover,
                           ),
                         ),
                         Container(
                           margin: EdgeInsets.only(right: 105),
                           child: Text(
                             'Task',
                             style: TextStyle(
                                 color: Color(0xff878DBA),
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.normal
                             ),
                           ),
                         ),
                         Text(
                           'Please fix my laptop',
                           textAlign: TextAlign.right,
                           style: TextStyle(
                               color: Color(0xff1E2661),
                               fontSize: 16.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                       ],
                     )
                 ),
                 Container(
                   height: 0.2,
                   color: Colors.grey,
                 ),
                 Container(
                     padding: EdgeInsets.all(20),
                     child: Column(
                       children: <Widget>[
                         Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           children: <Widget>[
                             new Container(
                               child: new Image.asset(
                                 'assets/images/clock.png',
                                 height: 17.0,
                                 width: 17.0,
                                 fit: BoxFit.cover,
                               ),
                             ),
                             Container(
                               margin: EdgeInsets.only(left: 20),
                               child: Text(
                                 'Description',
                                 style: TextStyle(
                                     color: Color(0xff878DBA),
                                     fontSize: 16.0,
                                     fontWeight: FontWeight.normal
                                 ),
                               ),
                             ),
                           ],
                         ),
                         SizedBox(
                           height: 5,
                         ),
                         Text(
                           'Task Descriptions are the statements of scope for each of the project activities.Are written in the format of “action.',
                           style: TextStyle(
                               color: Color(0xff1E2661),
                               fontSize: 14.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                       ],
                     )
                 ),

                 Container(
                   height: 0.2,
                   color: Colors.grey,
                 ),
                 SizedBox(
                   height: 15,
                 ),

                 Container(
                   margin: EdgeInsets.only(left: 20),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.start,
                     children: <Widget>[
                       new Container(
                         child: new Image.asset(
                           'assets/images/attachment.png',
                           height: 20.0,
                           width: 20.0,
                           fit: BoxFit.cover,
                         ),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 20),
                         child: Text(
                           'Attachment',
                           style: TextStyle(
                               color: Color(0xff878DBA),
                               fontSize: 16.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 15),
                         height: 25.0,
                         width: 25.0,
                         color: Colors.transparent,
                         child: Container(
                             decoration: BoxDecoration(
                                 color: Colors.green,
                                 borderRadius: BorderRadius.all(Radius.circular(10.0))),
                             child: new Center(
                               child: new Text("5",
                                 style: TextStyle(fontSize: 11, color: Colors.white),
                                 textAlign: TextAlign.center,),
                             )),
                       ),

                     ],
                   ),
                 ),
                 SizedBox(
                   height: 15,
                 ),
                 Row(
                   children: <Widget>[
                     Expanded(
                       child: Container(
                         margin: EdgeInsets.only(left: 25),
                         child: FDottedLine(
                           color: Color(0xffD4D4D4),
                           corner: FDottedLineCorner.all(6.0),

                           /// add widget
                           child: Container(
                             color: Color(0xffFFFFFF),
                             width: 130,
                             height: 102,
                             child: Container(
                                 margin: EdgeInsets.only(left: 10),
                                 child: Column(
                                   children: <Widget>[
                                     Row(
                                       children: <Widget>[
                                         _decideImage(),
                                       ],
                                     )

                                   ],
                                 )
                             ),
                           ),
                         ),
                       ),
                     ),
                     Expanded(
                       child: Container(
                         margin: EdgeInsets.only(left: 5),
                         child: FDottedLine(
                           color: Color(0xffD4D4D4),
                           corner: FDottedLineCorner.all(6.0),

                           /// add widget
                           child: Container(
                             color: Color(0xffFFFFFF),
                             width: 130,
                             height: 102,
                             child: Container(
                                 margin: EdgeInsets.only(left: 10),
                                 child: Column(
                                   children: <Widget>[
                                     Row(
                                       children: <Widget>[
                                         _decideImage(),
                                       ],
                                     )

                                   ],
                                 )
                             ),
                           ),
                         ),
                       ),
                     ),


                     SizedBox(
                       height: 15,
                     ),
                   ],
                 ),
                 Container(
                   padding: EdgeInsets.all(20),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.start,
                     children: <Widget>[
                       new Container(
                         child: new Image.asset(
                           'assets/images/progress.png',
                           height: 17.0,
                           width: 17.0,
                           fit: BoxFit.cover,
                         ),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 20),
                         child: Text(
                           'Action from admin',
                           style: TextStyle(
                               color: Color(0xff878DBA),
                               fontSize: 16.0,
                               fontWeight: FontWeight.normal
                           ),
                         ),
                       ),
                     ],
                   ),
                 ),

                 Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 15),
                        child: InkWell(
                          onTap: () async {
                            //_bottomSheetSplit(context);
                            showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.vertical(top: Radius.circular(20.0))),
                            backgroundColor: Colors.white,
                            context: context,
                            isScrollControlled: true,
                            builder: (context) => Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 18),
                            child: SOF()),
                            );
                            },

                          child: new Container(
                            width: 100.0,
                            height: 32.0,
                            decoration: new BoxDecoration(
                              color: Color(0xffE5F5F9),
                              border: new Border.all(color: Color(0xff2DCED6), width: 2.0),
                              borderRadius: new BorderRadius.circular(5.0),
                            ),
                            child: new Center(child: new Text('SPLIT', style: new TextStyle(fontSize: 11.0, color: Color(0xff2DCED6)),),),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () => print('hello'),
                        child: new Container(
                          width: 126.0,
                          height: 32.0,
                          decoration: new BoxDecoration(
                            color: Color(0xffECE6F9),
                            border: new Border.all(color: Color(0xffA880E3), width: 2.0),
                            borderRadius: new BorderRadius.circular(5.0),
                          ),
                          child: new Center(child: new Text('TRANSFER', style: new TextStyle(fontSize: 11.0, color: Color(0xffA880E3)),),),
                        ),
                      ),
                    ],
                 ),

                 Container(
                     padding: EdgeInsets.all(20),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         new Container(
                           child: new Image.asset(
                             'assets/images/progress.png',
                             height: 17.0,
                             width: 17.0,
                             fit: BoxFit.cover,
                           ),
                         ),
                         Container(
                           margin: EdgeInsets.only(right: 120),
                           child: Text(
                             'Assigned to',
                             style: TextStyle(
                                 color: Color(0xff878DBA),
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.normal
                             ),
                           ),
                         ),
                         Container(
                           height: 35,
                           width: 35,
                           child: Image.asset('assets/images/plus.png'),
                         )
                       ],
                     )
                 ),
                 Container(
                   height: 0.2,
                   color: Colors.grey,
                 ),

               ],
             ),
           ),
           //child: Text('This is text'),
         ),
       ),
    );
  }

  _decideImage() {

    if(imageFile == null){
      return Text('No image selected');
    }else{
      return Expanded(
        child: Image.file(
          imageFile,
          width: 130,
          height: 102,
          fit: BoxFit.cover,
        ),
      );
    }
  }

  void _bottomSheetMore(context) {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return new Container(
          padding: EdgeInsets.only(
            left: 5.0,
            right: 5.0,
            top: 5.0,
            bottom: 35.0,
          ),
          decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(10.0),
                  topRight: const Radius.circular(10.0))),
          child: new Wrap(
            children: <Widget>[
              new ListTile(
                title: const Text(
                  'Transfer Task',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

                SizedBox(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.only(left: 15,top: 10),
                  child: Text(
                  'Department',
                  style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  ),
                  ),
                ),

              Container(
                  width: 327,
                  margin: EdgeInsets.only(
                      top: 0,
                      left: 15,
                      right: 15
                  ),
                  padding: EdgeInsets.only(),
                  child: Center(
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: DropdownButton<String>(
                            value: selectedValue,
                            // icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 14,
                              height: 1.5,
                            ),
                            // underline: Container(
                            //   height: 2,
                            //   color: Colors.deepPurpleAccent,
                            // ),
                            onChanged: (String newValue) {
                              setState(() {
                                selectedValue = newValue;
                              });
                            },
                            items: values
                                .map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                          )))),

              Container(
                margin: EdgeInsets.only(left: 15),
                child: Text(
                  'Category',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

              Container(
                  width: 327,
                  margin: EdgeInsets.only(
                      top: 0,
                      left: 15,
                      right: 15
                  ),
                  padding: EdgeInsets.only(),
                  child: Center(
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: DropdownButton<String>(
                            value: selectedValue,
                            // icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 14,
                              height: 1.5,
                            ),
                            // underline: Container(
                            //   height: 2,
                            //   color: Colors.deepPurpleAccent,
                            // ),
                            onChanged: (String newValue) {
                              setState(() {
                                selectedValue = newValue;
                              });
                            },
                            items: values
                                .map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                          )))),

              Spacer(),
              Container(
                margin: EdgeInsets.only(top: 25),
                child: SizedBox(
                  width: 327,
                  height: 48,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color(0xff2DCED6))
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TeamMemberUserReport()));
                    },
                    color: Color(0xff2DCED6),
                    textColor: Colors.white,
                    child: Text("Assign Now".toUpperCase(),
                        style: TextStyle(fontSize: 14)),
                  ),
                ),
              ),

              SizedBox(height: 20),
              Expanded(
                child: Container(
                  child: taskList.isEmpty
                      ? Container()
                      : ListView.builder(itemBuilder: (ctx, index) {
                    if (index == taskList.length) return null;
                    return ListTile(
                      title: Text(taskList[index].title),
                      leading: Text(taskList[index].id.toString()),
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        //onPressed: () => _deleteTask(taskList[index].id),
                      ),
                    );
                  }),
                ),
              )
            ],
          ),
        );
      },
    );
  }



  Future<void> _bottomSheetSplit(context) async {


    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return new Container(
            child: Column(
              children: <Widget>[
                Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     Padding(
                       padding: const EdgeInsets.all(12.0),
                       child: Container(
                         child: Text(
                           'Split Task',
                           style: TextStyle(
                             fontSize: 14.0,
                             fontWeight: FontWeight.w700,
                           ),
                         ),
                       ),
                     ),

                     GestureDetector(
                       child: Padding(
                         padding: const EdgeInsets.all(12.0),
                         child: Container(
                            height: 25,
                            width: 25,
                            child: Image.asset('assets/images/gridadd.png'),
                         ),
                       ),
                       onTap: () async {
                        // List<PersonEntry> persons = await Navigator.push(
                        //          context,
                        //          MaterialPageRoute(
                        //            builder: (context) => SOF(),
                        //          ),
                        //        );
                        //        if (persons != null) persons.forEach(print);
                         setState(() {
                           cards.add(createCard());
                           });
                         },
                     )
                   ],
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: cards.length,
                    itemBuilder: (BuildContext context, int index) {
                      return cards[index];
                    },
                  ),
                ),
              ],
            )
        );
      },
    );
  }

  Card createCard() {
    var taskController = TextEditingController();
    var detailsController = TextEditingController();

    titleEdt.add(taskController);
    descriptionEdt.add(detailsController);

    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text('Person ${cards.length + 1}'),
          TextField(
              controller: taskController,
              decoration: InputDecoration(labelText: 'Task Name')),
          TextField(
              controller: detailsController,
              decoration: InputDecoration(labelText: 'Details')),
        ],
      ),
    );
  }
}

class SOF extends StatefulWidget {

  @override
  _SOFState createState() => _SOFState();
}

class _SOFState extends State<SOF> {
  var nameTECs = <TextEditingController>[];
  var ageTECs = <TextEditingController>[];
  var jobTECs = <TextEditingController>[];
  var cards = <Card>[];

  final List<String> values = <String>['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
  String selectedValue = 'A';


  Card createCard() {
    var nameController = TextEditingController();
    var ageController = TextEditingController();
    var jobController = TextEditingController();
    nameTECs.add(nameController);
    ageTECs.add(ageController);
    jobTECs.add(jobController);
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Person ${cards.length + 1}',style: TextStyle(
             color: Color(0xff1E2661),
             fontSize: 16
          ),),
          Align(
              alignment: Alignment.topLeft,
              child: DropdownButton<String>(
                value: selectedValue,
                // icon: Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                isExpanded: true,
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 14,
                  height: 1.5,
                ),
                // underline: Container(
                //   height: 2,
                //   color: Colors.deepPurpleAccent,
                // ),
                onChanged: (String newValue) {
                  setState(() {
                    selectedValue = newValue;
                  });
                },
                items: values
                    .map<DropdownMenuItem<String>>(
                        (String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          textAlign: TextAlign.right,
                        ),
                      );
                    }).toList(),
              )),
          TextField(
              controller: ageController,
              decoration: InputDecoration(labelText: 'Age')),
          TextField(
              controller: jobController,
              decoration: InputDecoration(labelText: 'Study/ job')),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
   // cards.add(createCard());
  }

  _onDone() {
    List<PersonEntry> entries = [];
    for (int i = 0; i < cards.length; i++) {
      var name = nameTECs[i].text;
      var age = ageTECs[i].text;
      var job = jobTECs[i].text;
      entries.add(PersonEntry(name, age, job));
    }
    Navigator.pop(context, entries);
  }

  @override
  Widget build(BuildContext context) {
      return Container(
        height: 400,
              child : Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Container(
                          child: Text(
                            'Split Task',
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),

                      GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Container(
                            height: 25,
                            width: 25,
                            child: Image.asset('assets/images/gridadd.png'),
                          ),
                        ),
                        onTap: () async {
                          // List<PersonEntry> persons = await Navigator.push(
                          //          context,
                          //          MaterialPageRoute(
                          //            builder: (context) => SOF(),
                          //          ),
                          //        );
                          //        if (persons != null) persons.forEach(print);
                          setState(() {
                            cards.add(createCard());
                          });
                        },
                      )
                    ],
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: cards.length,
                      itemBuilder: (BuildContext context, int index) {
                        return cards[index];
                      },
                    ),
                  ),
                ],
              )


         //child: createCard()
     );
  }
}


class PersonEntry {
  final String name;
  final String age;
  final String studyJob;

  PersonEntry(this.name, this.age, this.studyJob);
  @override
  String toString() {
    return 'Person: name= $name, age= $age, study job= $studyJob';
  }
}


