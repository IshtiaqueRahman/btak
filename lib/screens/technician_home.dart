import 'package:btakapps/model/trip.dart';
import 'package:btakapps/model/tripmodel.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'technician_my_request.dart';
import 'technician_my_task.dart';


class TechnicianHomePage extends StatefulWidget {
  @override
  _TechnicianHomePageState createState() => _TechnicianHomePageState();
}

class _TechnicianHomePageState extends State<TechnicianHomePage> {
  final List<Trip> tripsList = [
    Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
    Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
    Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
    Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 312,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  padding: EdgeInsets.only(top: 25),
                ),

                Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 12,
                      ),
                      CustomAppBarWidget(),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 15),
                        height: 70,
                        child: Container(
                            width: 320,
                            padding: EdgeInsets.all(10.0),
                            child: TextField(
                              style: TextStyle(color: Color(0xff99FFFFFF)),
                              autocorrect: true,
                              decoration: InputDecoration(
                                hintText: 'Search your task...',
                                prefixIcon: Icon(Icons.search,color: Color(0xff99FFFFFF),),
                                hintStyle: TextStyle(color: Color(0xff99FFFFFF)),
                                filled: true,
                                fillColor: Color(0xff1AFFFFFF),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                  borderSide: BorderSide(color: Color(0xff1AFFFFFF), width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                  borderSide: BorderSide(color: Color(0xff1AFFFFFF), width: 2),
                                ),
                              ),)
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 25, right: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text("Daily Activity (3)",
                             style: TextStyle(
                                color: Color(0xffFFFFFF),
                                fontSize: 16
                             ),
                            ),
                            new Text("Explore",
                              style: TextStyle(
                                  color: Color(0xffFFFFFF),
                                  fontSize: 16
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 25),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                         children: <Widget>[
                           _buildStatistic(),
                           _buildStatistic(),
                         ],
                      ),
                      SizedBox(
                        height: 35,
                      ),

                      Container(
                        margin: EdgeInsets.only(left: 25, right: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Recent Task",
                                  style: TextStyle(
                                      color: Color(0xff1E2661),
                                      fontSize: 20
                                  ),
                                ),
                                Text("Progress your lastest task",
                                  style: TextStyle(
                                      color: Color(0xff4E568C),
                                      fontSize: 16
                                  ),
                                ),
                              ],
                            ),
                        Container(
                          height: 35,
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffCBC9D9)),
                              borderRadius: BorderRadius.all(
                                Radius.circular(6.0) //                 <--- border radius here
                            ),
                          ),
                          child: Center(
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                //value: _chosenValue,
                                items: <String>['Google', 'Apple', 'Amazon', 'Tesla']
                                    .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                onChanged: (String value) {
                                  setState(() {
                                   // _chosenValue = value;
                                  });
                                },
                                hint: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("All task",
                                  style: TextStyle(
                                     fontSize: 14
                                  ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),

                          ],
                        ),
                      ),
                      Container(
                        child: new ListView.builder(
                            physics: const NeverScrollableScrollPhysics(), //
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: tripsList.length,
                            itemBuilder: (BuildContext context, int index) =>
                                buildTripCard(context, index)),
                           ),
                    ],
                  ),
                ),
              ],
            )

        ),
      ),
      //backgroundColor: AppColors.backgroundColor,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton:
      FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Color(0xff3D4FF4),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyARequest()));
          }
          ),
          bottomNavigationBar: Container(
          decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
          topRight: Radius.circular(20), topLeft: Radius.circular(20)),
          boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
       ],
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(20),
        topLeft: Radius.circular(20),
      ),
      child: BottomAppBar(
              shape: CircularNotchedRectangle(),
              child: Container(
                height: 56,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12,right: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.home,color: Color(0xff3D4FF4),), onPressed: () {}),
                      IconButton(icon: Icon(Icons.bookmark,
                       color: Color(0xffDAD7E0),
                      ), onPressed: () {}),
                      SizedBox(width: 40), // The dummy child
                      IconButton(icon: Icon(Icons.search,color: Color(0xffDAD7E0),), onPressed: () {}),
                      IconButton(icon: Icon(Icons.person,color: Color(0xffDAD7E0),), onPressed: () {}),
                    ],
                  ),
                ),
              )
           ),
    ),
      ),
    );
  }

  Widget _buildStatistic() {
    return Container(
      height: 123,
      width: 156,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      //margin: EdgeInsets.symmetric(horizontal: 5),
      padding: EdgeInsets.all(10),
      child: Center(
        child: Align(
          alignment: Alignment.topLeft,
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(height: 5),
                  Align(
                    alignment: AlignmentDirectional.topStart,
                    child: Container(
                      height: 34,
                      width: 34,
                      decoration: BoxDecoration(
                        color: Colors.yellow,
                        border: Border.all(color: Colors.yellow),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/images/ic_data.png'),
                            fit: BoxFit.contain
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Align(
                    alignment: AlignmentDirectional.topStart,
                    child: Text(
                      "Hr Department",
                      style: TextStyle(
                        //color: '#1E2661'.toColor(),
                        fontSize: 14,
                        height: 1.5,
                      ),
                    ),
                  ),
                  SizedBox(height: 3),
                  Row(
                    children: <Widget>[
                      Align(
                        alignment: AlignmentDirectional.topStart,
                        child: Text(
                          "June 17,2020",
                          style: TextStyle(
                            //color: '#878DBA'.toColor(),
                            fontSize: 12,
                            height: 1.5,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                          height: 3,
                          width: 3,
                          child: Image.asset('assets/images/dot.png')
                      ),
                      SizedBox(
                        width: 3,
                      ),
                      Text(
                        "(20)",
                        style: TextStyle(
                          //color: '#878DBA'.toColor(),
                          fontSize: 12,
                          height: 1.5,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTripCard(BuildContext context, int index) {
    final trip = tripsList[index];
    return new Container(
      margin: EdgeInsets.only(left: 15,right: 15),
      child: Stack(
        children: <Widget>[
          Card(
            color: Color(0xffFFFFFF),
            elevation: 3,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Color(0xffFFFFFF), width: 1),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Image.asset('assets/images/settings.png',
                                  height: 16,
                                  width: 16,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 7),
                                  child: Text(
                                    "SER13312",
                                    style: TextStyle(
                                      //color: '#878DBA'.toColor(),
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 9,
                            ),
                            Row(
                              children: <Widget>[
                                Image.asset('assets/images/clock.png',
                                  height: 16,
                                  width: 16,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 7),
                                  child: Text(
                                    "${DateFormat('dd/MM/yyyy').format(trip.startDate).toString()} - ${DateFormat('dd/MM/yyyy').format(trip.endDate).toString()}",style: TextStyle(
                                      color: Color(0xff878DBA)
                                  ),),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 5),
                            child: RaisedButton(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  side: BorderSide(color: Color(0xffA880E3))),
                              onPressed: () {},
                              color: Color(0xffF2ECFB),
                              textColor: Color(0xffA880E3),
                              child: Text("In Progress".toUpperCase(),
                                  style: TextStyle(fontSize: 11)),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Replacement mouse required',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff1E2661),
                      fontFamily: 'DMSans-Bold',
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Divider(),
                  Row(
                    children: <Widget>[
                      Container(
                        height: 35,
                        width: 35.1,
                        color: Colors.transparent,
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(10.0))),
                            child: Image.asset('assets/images/bitaclogo.png')
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7),
                        child: Text(
                          "Fahad Ibn Sayeed, Arena Phone Ltd",
                          style: TextStyle(
                           //color: '#878DBA'.toColor(),
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 35),
            decoration: BoxDecoration(
                  color: Colors.greenAccent,
                  borderRadius: BorderRadius.all(Radius.circular(20))
              ),
              height: 100,
              width: 5,
            ),
        ],
      ),
    );
  }
}

