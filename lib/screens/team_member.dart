//import 'package:btakapps/db/dbhelper.dart';
//import 'package:btakapps/model/todo.dart';
//import 'package:btakapps/model/trip.dart';
//import 'package:btakapps/screens/taskdetails.dart';
//import 'package:flutter/material.dart';
//import 'package:http/http.dart' as http;
//import 'package:provider/provider.dart';
//import 'dart:async';
//import 'dart:convert';
//import 'create_new_task_technician.dart';
//import 'technician_my_task.dart';
//
////class TeamMember extends StatefulWidget {
////  @override
////  _TeamMemberState createState() => _TeamMemberState();
////}
////
////
////class _TeamMemberState extends State<TeamMember> {
////
////  TextEditingController textController = new TextEditingController();
////  bool expandFlag = false;
////
////  List<Todo> taskList = new List();
////
////  DateTime selectedDate = DateTime.now();
////  TextEditingController _date = new TextEditingController();
////
////  final List<Trip> tripsList = [
////    Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
////    Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
////    Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
////    Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
////    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
////    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
////    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
////  ];
////
////  @override
////  Widget build(BuildContext context) {
////    return Scaffold(
////        backgroundColor: Color(0xff6C5DD3),
////        appBar: AppBar(
////          title: Text('Team Member'),
////          backgroundColor: Color(0xff6C5DD3),
////          elevation: 0,
////          leading: IconButton(
////            icon: Icon(
////              Icons.arrow_back,
////              color: Colors.white,
////            ),
////          ),
////        ),
////
////        body: SingleChildScrollView(
////          child: Container(
////            margin: EdgeInsets.only(top: 25),
////            padding: EdgeInsets.all(20),
////            decoration: BoxDecoration(
////                color: Colors.white,
////                borderRadius: BorderRadius.only(
////                    topLeft: Radius.circular(30),
////                    topRight: Radius.circular(30)
////                )
////            ),
////
////            child: Expanded(
////                child: Container(
////                   child: ListView.builder(
////                     scrollDirection: Axis.vertical,
////                     shrinkWrap: true,
////                itemBuilder: (BuildContext context, int index) {
////              return ExpandableListView(title: "Title $index");
////              },
////                itemCount: tripsList.length,
////              ),
//////                child: new ListView.builder(
//////                    physics: const NeverScrollableScrollPhysics(), //
//////                    scrollDirection: Axis.vertical,
//////                    shrinkWrap: true,
//////                    itemCount: tripsList.length,
//////                    itemBuilder: (BuildContext context, int index) =>
//////                        buildTripCard(context, index)),
////                ),
////              ),
////            ),
////            //body: ListShow(),
////        ),
////    );
////  }
////
////  Widget buildTripCard(BuildContext context, int index) {
////    final trip = tripsList[index];
////
////    return SingleChildScrollView(
////       child:  Expanded(
////         child: new Container(
////           child: Column(
////             children: <Widget>[
////               Row(
////                 crossAxisAlignment: CrossAxisAlignment.center,
////                 children: <Widget>[
////                   Container(
////                     child: Stack(
////                       children: <Widget>[
////                         Container(
////                           margin: EdgeInsets.only(top: 30),
////                           height: 50.65,
////                           width: 55.51,
////                           child: CircleAvatar(
////                             //backgroundColor: Colors.white,
////                             backgroundImage: ExactAssetImage('assets/images/bg.png'),
////                           ),
////                         ),
////                         Positioned(
////                           left: 35,
////                           child: Container(
////                               margin: EdgeInsets.only(top: 60),
////                               height: 20,
////                               width: 20,
////                               child: CircleAvatar(
////                                 backgroundColor: Color(0xff6F5EDA),
////                                 child: Text(
////                                   "1",
////                                   style: TextStyle(
////                                       color: Colors.white
////                                   ),
////                                 ),
////                               )
////                           ),
////                         ),
////                       ],
////                     ),
////                   ),
////                   Column(
////                     mainAxisAlignment: MainAxisAlignment.center,
////                     children: <Widget>[
////                       Container(
////                         margin: EdgeInsets.only(left: 15,top: 30),
////                         child: Column(
////                           crossAxisAlignment: CrossAxisAlignment.start,
////                           mainAxisAlignment: MainAxisAlignment.center,
////                           children: <Widget>[
////                             GestureDetector(
////                                 child: Text(
////                                   '29,August 2019',
////                                   style: TextStyle(
////                                     fontSize: 16,
////                                     color: Color(0xff223E5C),
////                                     fontFamily: 'DMSans-Bold',
////                                   ),
////                                 ),
////                                 onTap: () {
////                                   setState(() {
////                                     expandFlag = !expandFlag;
////                                   });
////                                 }
////                             ),
////                             GestureDetector(
////                                 child: Text(
////                                   'Sr. Product Designer',
////                                   style: TextStyle(
////                                     fontSize: 12,
////                                     color: Color(0xffA6B0BF),
////                                     fontFamily: 'DMSans-Bold',
////                                   ),
////                                 ),
////                                 onTap: () {
////                                   setState(() {
////                                     expandFlag = !expandFlag;
////                                   }
////                                   );
////                                 }
////                             ),
////                           ],
////                         ),
////                       ),
////                     ],
////                   ),
////                   Spacer(),
////                   Container(
////                     margin: EdgeInsets.only(top: 25),
////                     child: SizedBox(
////                       width: 67,
////                       height: 28,
////                       child: RaisedButton(
////                         shape: RoundedRectangleBorder(
////                             borderRadius: BorderRadius.circular(18.0),
////                             side: BorderSide(color: Color(0xff2DCED6))
////                         ),
////                         onPressed: () {
////                           setState(() {
////                             expandFlag = !expandFlag;
////                           });
////                           //_addToDb();
////                           //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskDetails()));
////                         },
////                         color: Color(0xff2DCED6),
////                         textColor: Colors.white,
////                         child: Text("Add".toUpperCase(),
////                             style: TextStyle(fontSize: 14)),
////                       ),
////                     ),
////                   ),
////                 ],
////               ),
////               Divider(),
////               ExpandableContainer(
////                   expanded: expandFlag,
////                   child: Container(
////                     margin: EdgeInsets.all(10),
////                      child: Column(
////                         children: [
////                            Row(
////                               children: [
////                                  Column(
////                                    crossAxisAlignment: CrossAxisAlignment.start,
////                                    children: [
////                                      Text(
////                                        'Start Date',
////                                        style: TextStyle(
////                                            color: Color(0xff1E2661)
////                                        ),
////                                      ),
////
////                                      Container(
////                                          width: 130,
////                                          height: 40,
////                                          margin: EdgeInsets.only(top:10,right: 10),
////                                          //padding: EdgeInsets.all(6.0),
////                                          child: TextFormField(
////                                            //controller: email,
////                                            autocorrect: true,
////                                            decoration: InputDecoration(
////                                              hintText: '19 Jan 2020',
////                                              contentPadding:
////                                              EdgeInsets.all(0),
////                                              prefixIcon: Icon(
////                                                Icons.date_range,
////                                                //color: _icon,
////                                              ),
////                                              hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
////                                              filled: true,
////                                              fillColor: Color(0xffF8F8FD),
////                                              enabledBorder: OutlineInputBorder(
////                                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                                borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
////                                              ),
////                                              focusedBorder: OutlineInputBorder(
////                                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                                borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
////                                              ),
////                                            ),)
////                                      ),
////
////                                    ],
////                                  ),
////                                  Container(
////                                      margin: EdgeInsets.only(top: 15),
////                                      width: 15,
////                                      height: 20,
////                                      child: Image.asset('assets/images/arrowrght.png')
////                                  ),
////                                 Column(
////                                   crossAxisAlignment: CrossAxisAlignment.start,
////                                   children: [
////                                     Container(
////                                       margin: EdgeInsets.only(left: 7),
////                                       child: Text(
////                                         'Start Time',
////                                         style: TextStyle(
////                                             color: Color(0xff1E2661)
////                                         ),
////                                       ),
////                                     ),
////
////                                     Container(
////                                         width: 130,
////                                         height: 40,
////                                         margin: EdgeInsets.only(top:10,left: 10),
////                                         //padding: EdgeInsets.all(6.0),
////                                         child: TextFormField(
////                                           //controller: email,
////                                           autocorrect: true,
////                                           decoration: InputDecoration(
////                                             hintText: '10:30 PM',
////                                             contentPadding:
////                                             EdgeInsets.all(0),
////                                             prefixIcon: Icon(
////                                               Icons.access_alarm,
////                                               //color: _icon,
////                                             ),
////                                             hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
////                                             filled: true,
////                                             fillColor: Color(0xffF8F8FD),
////                                             enabledBorder: OutlineInputBorder(
////                                               borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                               borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
////                                             ),
////                                             focusedBorder: OutlineInputBorder(
////                                               borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                               borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
////                                             ),
////                                           ),)
////                                     ),
////
////                                   ],
////                                 ),
////                                ],
////                            ),
////                           SizedBox(
////                             height: 10,
////                           ),
////                            Row(
////                             children: [
////                               Column(
////                                 crossAxisAlignment: CrossAxisAlignment.start,
////                                 children: [
////                                   Text(
////                                     'End Date',
////                                     style: TextStyle(
////                                         color: Color(0xff1E2661)
////                                     ),
////                                   ),
////
////                                   Container(
////                                       width: 130,
////                                       height: 40,
////                                       margin: EdgeInsets.only(top:10,right: 10),
////                                       //padding: EdgeInsets.all(6.0),
////                                       child: TextFormField(
////                                         //controller: email,
////                                         autocorrect: true,
////                                         decoration: InputDecoration(
////                                           hintText: '19 Jan 2020',
////                                           contentPadding:
////                                           EdgeInsets.all(0),
////                                           prefixIcon: Icon(
////                                             Icons.date_range,
////                                             //color: _icon,
////                                           ),
////                                           hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
////                                           filled: true,
////                                           fillColor: Color(0xffF8F8FD),
////                                           enabledBorder: OutlineInputBorder(
////                                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                             borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
////                                           ),
////                                           focusedBorder: OutlineInputBorder(
////                                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                             borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
////                                           ),
////                                         ),)
////                                   ),
////
////                                 ],
////                               ),
////                               Container(
////                                   margin: EdgeInsets.only(top: 10),
////                                   width: 15,
////                                   height: 20,
////                                   child: Image.asset('assets/images/arrowrght.png')
////                               ),
////                               Column(
////                                 crossAxisAlignment: CrossAxisAlignment.start,
////                                 children: [
////                                   Container(
////                                     margin: EdgeInsets.only(left: 7),
////                                     child: Text(
////                                       'End Time',
////                                       style: TextStyle(
////                                           color: Color(0xff1E2661)
////                                       ),
////                                     ),
////                                   ),
////
////                                   Container(
////                                       width: 130,
////                                       height: 40,
////                                       margin: EdgeInsets.only(top:10,left: 10),
////                                       //padding: EdgeInsets.all(6.0),
////                                       child: TextFormField(
////                                         //controller: email,
////                                         autocorrect: true,
////                                         decoration: InputDecoration(
////                                           hintText: '10:30 PM',
////                                           contentPadding:
////                                           EdgeInsets.all(0),
////                                           prefixIcon: Icon(
////                                             Icons.access_alarm,
////                                             //color: _icon,
////                                           ),
////                                           hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
////                                           filled: true,
////                                           fillColor: Color(0xffF8F8FD),
////                                           enabledBorder: OutlineInputBorder(
////                                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                             borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
////                                           ),
////                                           focusedBorder: OutlineInputBorder(
////                                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
////                                             borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
////                                           ),
////                                         ),)
////                                   ),
////
////                                 ],
////                               ),
////                             ],
////                           ),
////                           SizedBox(
////                             height: 15,
////                           ),
////                           Row(
////                             children: <Widget>[
////                               Container(
////                                 width: 23.94,
////                                 height: 23.50,
////                                 child: Image.asset('assets/images/reminderbell.png'),
////                               ),
////                               Container(
////                                 margin: EdgeInsets.only(left: 10),
////                                 child: Text(
////                                   'Set priority',
////                                   style: TextStyle(
////                                     fontSize: 16,
////                                     color: Color(0xff1E2661),
////                                     fontFamily: 'DMSans-Bold',
////                                   ),
////                                 ),
////                               ),
////                             ],
////                           ),
////                           SizedBox(
////                             height: 15,
////                           ),
////                           Wrap(
////                               spacing: 6.0,
////                               runSpacing: 6.0,
////                               children: <Widget>[
////                                 chip('Health', Color(0xFFff8a65)),
////                                 chip('Food', Color(0xFF4fc3f7)),
////                                 chip('Lifestyle', Color(0xFF9575cd)),
////                               ]
////                           ),
////                         ],
////                      ),
////                   )
////               )
////             ],
////           ),
////         ),
////       )
////       );
////    }
////
////  void onAdd() {
////    // Products product = Products(
////    //         id: '1',
////    //         title: 'new shoes',
////    //         price: 69.99,
////    //         image: 'assets/images/bg.png',
////    //       );
////    // Provider.of<ImageModel>(context, listen: false).addTodo(product);
////    // Navigator.pop(context);
////
////
////    //ei porjonto kora chilo
////
////
////    // final Task todo = Task(
////    //     //   title: textVal,
////    //     //   completed: completed,
////    //     // );
////    //     // Provider.of<TodosModel>(context, listen: false).addTodo(todo);
////    //     // Navigator.pop(context);
////  }
////
////  void _addToDb() async {
////    String task = 'assets/images/img1.png';
////    var id = await DatabaseHelper.instance.insert(Todo(title: task));
////    setState(() {
////       //taskList.insert(0, Todo(id: 0,title: "assets/images/img1.png"));
////       //taskList.insert(1, Todo(id: 1,title: "assets/images/img2.png"));
////       //taskList.insert(2, Todo(id: 2,title: "assets/images/img3.png"));
////      taskList.insert(0, Todo(id: id, title: task));
////    });
////  }
////
////  Future<Null> _selectDate(BuildContext context) async {
////    final DateTime picked = await showDatePicker(
////        context: context,
////        initialDate: selectedDate,
////        firstDate: DateTime(1901, 1),
////        lastDate: DateTime(2100));
////    if (picked != null && picked != selectedDate)
////      setState(() {
////        selectedDate = picked;
////        _date.value = TextEditingValue(text: picked.toString());
////      });
////  }
////}
//class TeamMember extends StatelessWidget {
//  TextEditingController textController = new TextEditingController();
//  bool expandFlag = false;
//
//  List<Todo> taskList = new List();
//
//  DateTime selectedDate = DateTime.now();
//  TextEditingController _date = new TextEditingController();
//
//  final List<Trip> tripsList = [
//    Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
//    Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
//    Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
//    Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
//    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
//    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
//    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
//  ];
//  @override
//  Widget build(BuildContext context) {
//      return Scaffold(
//        backgroundColor: Color(0xff6C5DD3),
//        appBar: AppBar(
//          title: Text('Team Member'),
//          backgroundColor: Color(0xff6C5DD3),
//          elevation: 0,
//          leading: IconButton(
//            icon: Icon(
//              Icons.arrow_back,
//              color: Colors.white,
//            ),
//          ),
//        ),
//
//        body: SingleChildScrollView(
//           child: Container(
//             margin: EdgeInsets.only(top: 25),
//             padding: EdgeInsets.all(20),
//             decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(30),
//                     topRight: Radius.circular(30)
//                 )
//             ),
//             child: Expanded(
//                child: Container(
//                  child: ListView.builder(
//                    scrollDirection: Axis.vertical,
//                    shrinkWrap: true,
//                    itemBuilder: (BuildContext context, int index) {
//                      return ExpandableListView(title: "Title $index");
//                    },
//                    itemCount: tripsList.length,
//                  ),
//                ),
//             ),
//           ),
//        ),
//      );
//  }
//}
//
//class ExpandableListView extends StatefulWidget {
//  final String title;
//
//  const ExpandableListView({Key key, this.title}) : super(key: key);
//
//  @override
//  _ExpandableListViewState createState() => new _ExpandableListViewState();
//}
//
//class _ExpandableListViewState extends State<ExpandableListView> {
//  bool expandFlag = false;
//
//  @override
//  Widget build(BuildContext context) {
//    return SingleChildScrollView(
//      child: Expanded(
//        child: new Container(
//          child: Column(
//            children: <Widget>[
//              Row(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  Container(
//                    child: Stack(
//                      children: <Widget>[
//                        Container(
//                          margin: EdgeInsets.only(top: 30),
//                          height: 50.65,
//                          width: 55.51,
//                          child: CircleAvatar(
//                            //backgroundColor: Colors.white,
//                            backgroundImage: ExactAssetImage('assets/images/bg.png'),
//                          ),
//                        ),
//                        Positioned(
//                          left: 35,
//                          child: Container(
//                              margin: EdgeInsets.only(top: 60),
//                              height: 20,
//                              width: 20,
//                              child: CircleAvatar(
//                                backgroundColor: Color(0xff6F5EDA),
//                                child: Text(
//                                  "1",
//                                  style: TextStyle(
//                                      color: Colors.white
//                                  ),
//                                ),
//                              )
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Column(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Container(
//                        margin: EdgeInsets.only(left: 15,top: 30),
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          children: <Widget>[
//                            GestureDetector(
//                                child: Text(
//                                  '29,August 2019',
//                                  style: TextStyle(
//                                    fontSize: 16,
//                                    color: Color(0xff223E5C),
//                                    fontFamily: 'DMSans-Bold',
//                                  ),
//                                ),
//                            ),
//                            GestureDetector(
//                                child: Text(
//                                  'Sr. Product Designer',
//                                  style: TextStyle(
//                                    fontSize: 12,
//                                    color: Color(0xffA6B0BF),
//                                    fontFamily: 'DMSans-Bold',
//                                  ),
//                                ),
//                            ),
//                          ],
//                        ),
//                      ),
//                    ],
//                  ),
//                  Spacer(),
//                  Container(
//                    margin: EdgeInsets.only(top: 25),
//                    child: SizedBox(
//                      width: 67,
//                      height: 28,
//                      child: RaisedButton(
//                        shape: RoundedRectangleBorder(
//                            borderRadius: BorderRadius.circular(18.0),
//                            side: BorderSide(color: Color(0xff2DCED6))
//                        ),
//                        onPressed: () {
//                          setState(() {
//                            expandFlag = !expandFlag;
//                          });
//                          //_addToDb();
//                          //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TaskDetails()));
//                        },
//                        color: Color(0xff2DCED6),
//                        textColor: Colors.white,
//                        child: Text("Add".toUpperCase(),
//                            style: TextStyle(fontSize: 14)),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//              Divider(),
//              ExpandableContainer(
//                  expanded: expandFlag,
//                  child: Container(
//                    margin: EdgeInsets.all(10),
//                    child: Column(
//                      children: [
//                        Row(
//                          children: [
//                            Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Text(
//                                  'Start Date',
//                                  style: TextStyle(
//                                      color: Color(0xff1E2661)
//                                  ),
//                                ),
//
//                                Container(
//                                    width: 130,
//                                    height: 40,
//                                    margin: EdgeInsets.only(top:10,right: 10),
//                                    //padding: EdgeInsets.all(6.0),
//                                    child: TextFormField(
//                                      //controller: email,
//                                      autocorrect: true,
//                                      decoration: InputDecoration(
//                                        hintText: '19 Jan 2020',
//                                        contentPadding:
//                                        EdgeInsets.all(0),
//                                        prefixIcon: Icon(
//                                          Icons.date_range,
//                                          //color: _icon,
//                                        ),
//                                        hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
//                                        filled: true,
//                                        fillColor: Color(0xffF8F8FD),
//                                        enabledBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
//                                        ),
//                                        focusedBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
//                                        ),
//                                      ),)
//                                ),
//
//                              ],
//                            ),
//                            Container(
//                                margin: EdgeInsets.only(top: 15),
//                                width: 15,
//                                height: 20,
//                                child: Image.asset('assets/images/arrowrght.png')
//                            ),
//                            Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  margin: EdgeInsets.only(left: 7),
//                                  child: Text(
//                                    'Start Time',
//                                    style: TextStyle(
//                                        color: Color(0xff1E2661)
//                                    ),
//                                  ),
//                                ),
//
//                                Container(
//                                    width: 130,
//                                    height: 40,
//                                    margin: EdgeInsets.only(top:10,left: 10),
//                                    //padding: EdgeInsets.all(6.0),
//                                    child: TextFormField(
//                                      //controller: email,
//                                      autocorrect: true,
//                                      decoration: InputDecoration(
//                                        hintText: '10:30 PM',
//                                        contentPadding:
//                                        EdgeInsets.all(0),
//                                        prefixIcon: Icon(
//                                          Icons.access_alarm,
//                                          //color: _icon,
//                                        ),
//                                        hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
//                                        filled: true,
//                                        fillColor: Color(0xffF8F8FD),
//                                        enabledBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
//                                        ),
//                                        focusedBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
//                                        ),
//                                      ),)
//                                ),
//
//                              ],
//                            ),
//                          ],
//                        ),
//                        SizedBox(
//                          height: 10,
//                        ),
//                        Row(
//                          children: [
//                            Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Text(
//                                  'End Date',
//                                  style: TextStyle(
//                                      color: Color(0xff1E2661)
//                                  ),
//                                ),
//
//                                Container(
//                                    width: 130,
//                                    height: 40,
//                                    margin: EdgeInsets.only(top:10,right: 10),
//                                    //padding: EdgeInsets.all(6.0),
//                                    child: TextFormField(
//                                      //controller: email,
//                                      autocorrect: true,
//                                      decoration: InputDecoration(
//                                        hintText: '19 Jan 2020',
//                                        contentPadding:
//                                        EdgeInsets.all(0),
//                                        prefixIcon: Icon(
//                                          Icons.date_range,
//                                          //color: _icon,
//                                        ),
//                                        hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
//                                        filled: true,
//                                        fillColor: Color(0xffF8F8FD),
//                                        enabledBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
//                                        ),
//                                        focusedBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
//                                        ),
//                                      ),)
//                                ),
//
//                              ],
//                            ),
//                            Container(
//                                margin: EdgeInsets.only(top: 10),
//                                width: 15,
//                                height: 20,
//                                child: Image.asset('assets/images/arrowrght.png')
//                            ),
//                            Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  margin: EdgeInsets.only(left: 7),
//                                  child: Text(
//                                    'End Time',
//                                    style: TextStyle(
//                                        color: Color(0xff1E2661)
//                                    ),
//                                  ),
//                                ),
//
//                                Container(
//                                    width: 130,
//                                    height: 40,
//                                    margin: EdgeInsets.only(top:10,left: 10),
//                                    //padding: EdgeInsets.all(6.0),
//                                    child: TextFormField(
//                                      //controller: email,
//                                      autocorrect: true,
//                                      decoration: InputDecoration(
//                                        hintText: '10:30 PM',
//                                        contentPadding:
//                                        EdgeInsets.all(0),
//                                        prefixIcon: Icon(
//                                          Icons.access_alarm,
//                                          //color: _icon,
//                                        ),
//                                        hintStyle: TextStyle(color: Colors.grey,fontSize: 14),
//                                        filled: true,
//                                        fillColor: Color(0xffF8F8FD),
//                                        enabledBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color: Color(0xffE9EBF6), width: 1),
//                                        ),
//                                        focusedBorder: OutlineInputBorder(
//                                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                                          borderSide: BorderSide(color:  Color(0xffE9EBF6), width: 1),
//                                        ),
//                                      ),)
//                                ),
//
//                              ],
//                            ),
//                          ],
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                        Row(
//                          children: <Widget>[
//                            Container(
//                              width: 23.94,
//                              height: 23.50,
//                              child: Image.asset('assets/images/reminderbell.png'),
//                            ),
//                            Container(
//                              margin: EdgeInsets.only(left: 10),
//                              child: Text(
//                                'Set priority',
//                                style: TextStyle(
//                                  fontSize: 16,
//                                  color: Color(0xff1E2661),
//                                  fontFamily: 'DMSans-Bold',
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                        Wrap(
//                            spacing: 6.0,
//                            runSpacing: 6.0,
//                            children: <Widget>[
//                              chip('Health', Color(0xFFff8a65)),
//                              chip('Food', Color(0xFF4fc3f7)),
//                              chip('Lifestyle', Color(0xFF9575cd)),
//                            ]
//                        ),
//                      ],
//                    ),
//                  )
//              )
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
//
//class ExpandableContainer extends StatelessWidget {
//  final bool expanded;
//  final double collapsedHeight;
//  final double expandedHeight;
//  final Widget child;
//
//  ExpandableContainer({
//    @required this.child,
//    this.collapsedHeight = 270.0,
//    this.expandedHeight = 270.0,
//    this.expanded = true,
//  });
//
//  @override
//  Widget build(BuildContext context) {
//    double screenWidth = MediaQuery.of(context).size.width;
//    return new AnimatedContainer(
//      duration: new Duration(milliseconds: 500),
//      curve: Curves.easeInOut,
//      width: screenWidth,
//      height: expanded ? expandedHeight : collapsedHeight,
//      child: new Container(
//        child: child,
//      ),
//    );
//  }
//}
//
//
//
