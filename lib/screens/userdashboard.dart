import 'dart:convert';

import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/dashboard.dart';
import 'package:btakapps/model/technician_dashboard.dart';
import 'package:btakapps/model/trip.dart';
import 'package:btakapps/model/tripmodel.dart';
import 'package:btakapps/screens/teammember_userreport.dart';
import 'package:btakapps/screens/report.dart';
import 'package:btakapps/screens/techinician_profile.dart';
import 'package:btakapps/screens/user_dashboard.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'create_new_task_technician.dart';
import 'custombar.dart';
import 'package:http/http.dart' as http;
import 'job_admin_profile.dart';
import 'jobadmin_mytask.dart';
import 'jobadmin_myticket.dart';
import 'technician_my_request.dart';
import 'technician_my_task.dart';


class UserDash extends StatefulWidget {
  @override
  _TechnicianHomePageState createState() => _TechnicianHomePageState();
}

class _TechnicianHomePageState extends State<UserDash> {
  final List<Trip> tripsList = [
    Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
    Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
    Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
    Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
    Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
  ];

  String pending,progress,completed;
  var mytask,myservice,fullname;

  Future _list() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");
    var user_id = pref.get("user_id");
    fullname = pref.get("full_name");

    print(officeId);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id":officeId,
      "department_id":departmentId,
      "user_id" : user_id
    };

    final response =
    await http.post(BaseUrl.baseurl+"technician_dashboard.json",body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var data = Technician_Dashboard.fromJson(taskListMap);

    //print(data.data[0].results.)
    setState(() {
      pending = data.data[0].results.pendingTasks;
      progress = data.data[0].results.inProgressTasks;
      completed = data.data[0].results.completeTasks;
      mytask = data.data[0].results.newTasks;
      myservice = data.data[0].results.toalServices;
    });

    //print(token);

    print(response);

  }

  @override
  void initState() {
    // TODO: implement initState
    _list();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 312,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  padding: EdgeInsets.only(top: 25),
                ),

                Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 12,
                      ),
                      CustomBarWidget(),
                      SizedBox(
                        height: 20,
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                         children: [
                            Column(
                               children: [
                                 Text(
                                   pending ?? '',
                                   style: TextStyle(
                                     color: Color(0xffF2C94C),
                                     fontSize: 48,
                                   ),
                                 ),
                                 Text(
                                   "Tasks\nPending",
                                   style: TextStyle(
                                     color: Color(0xffFFFFFF),
                                     fontSize: 14,
                                   ),
                                 ),
                               ],
                            ),
                           Container(
                               height: 48,
                               width: 2,
                               color: Color(0xff6E64FE),
                           ),
                           Column(
                             children: [
                               Text(
                                 progress ?? '',
                                 style: TextStyle(
                                   color: Color(0xff2DCED6),
                                   fontSize: 48,
                                 ),
                               ),
                               Text(
                                 "Tasks\nInProgress",
                                 style: TextStyle(
                                   color: Color(0xffFFFFFF),
                                   fontSize: 14,
                                 ),
                               ),
                             ],
                           ),
                           Container(
                             height: 48,
                             width: 2,
                             color: Color(0xff6E64FE),
                           ),
                           Column(
                             children: [
                               Text(
                                 completed ?? '',
                                 style: TextStyle(
                                   color: Color(0xff33BC6D),
                                   fontSize: 48,
                                 ),
                               ),
                               Text(
                                 "Tasks\nCompleted",
                                 style: TextStyle(
                                   color: Color(0xffFFFFFF),
                                   fontSize: 14,
                                 ),
                               ),
                             ],
                           )

                         ],
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                         children: <Widget>[
                           _buildStatistic("My Task","$mytask new ticket adeed",Image.asset('assets/images/mytask.png'),0xffE3E6FF),
                           _buildStatistic("My Service","You have $myservice Ticket",Image.asset('assets/images/myticket.png'),0xffE0F4F4),
                         ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _buildStatistic("Report","See all your report",Image.asset('assets/images/report.png'),0xffD0E9F8),
                          _buildStatistic("My Profile",fullname ?? '',Image.asset('assets/images/person.png'),0xffEFEEFF),
                        ],
                      ),

                      SizedBox(
                        height: 35,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 327,
                            height: 48,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: BorderSide(color: Color(0xff2DCED6))
                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                                    CreateNewTask()));
                              },
                              color: Color(0xff2DCED6),
                              textColor: Colors.white,
                              child: Text("Create Ticket".toUpperCase(),
                                  style: TextStyle(fontSize: 14)),
                            ),
                          ),
                        ],
                      ),

                      SizedBox(
                        height: 35,
                      ),

                    ],
                  ),
                ),
              ],
            )
        )
       )
    );
  }

  Widget _buildStatistic(String title, String subtitle, Image image, int color) {
    return GestureDetector(
      child: Container(
        height: 140,
        width: 156,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
          border: Border.all(color: Colors.white),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(1, 1),
              spreadRadius: 1,
              blurRadius: 1,
            ),
          ],
        ),
        //margin: EdgeInsets.symmetric(horizontal: 5),
        padding: EdgeInsets.all(10),
        child: Center(
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(height: 5),
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: Container(
                        height: 43,
                        width: 43,
                        decoration: BoxDecoration(
                          color: Color(color),
                          border: Border.all(color: Color(color)),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                              image: image.image,
                              scale: 2.5
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: Text(
                        title,
                        style: TextStyle(
                          color: Color(0xff1E2661),
                          fontSize: 18,
                          fontFamily: "DMSans",  // <- Looks up the specified font in pubspec.yaml
                          fontWeight: FontWeight.w600,
                          height: 1.5,
                        ),
                      ),
                    ),
                    SizedBox(height: 3),
                    Row(
                      children: <Widget>[
                        Align(
                          alignment: AlignmentDirectional.topStart,
                          child: Text(
                            subtitle,
                            style: TextStyle(
                              color: Color(0xff878DBA),
                              fontFamily: "DMSans",  // <- Looks up the specified font in pubspec.yaml
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              height: 1.5,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: (){
         if(title=="My Task"){
           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
               MyTasks()));
         }else if(title=="My Service"){
           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
               MyARequest()));
         }else if(title=="Report"){
           showDialogMenu();
           //_buildAboutDialog(context),
//           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
//               TeamMemberUserReports()));
         }else if(title=="My Profile"){
           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
               TechnicianProfile()));
         }
      },
    );
  }

  showDialogMenu() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
          title: new Text("Repors"),
          content: Container(
            height: 120,
            width: 100,
            child: Column(
              children: [
                SizedBox(
                  width: 327,
                  height: 48,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color(0xff2DCED6))
                    ),
                    onPressed: () {
                      FocusScopeNode currentFocus = FocusScope.of(context);

                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }

                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                          TeamMemberUserReports()));

                      },
                    color: Color(0xff2DCED6),
                    textColor: Colors.white,
                    child: Text("Summary Report".toUpperCase(),
                        style: TextStyle(fontSize: 14)),
                  ),
                ),
                SizedBox(
                  height : 10
                ),
                SizedBox(
                  width: 327,
                  height: 48,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color(0xff3DA7F2))
                    ),
                    onPressed: () {
                      FocusScopeNode currentFocus = FocusScope.of(context);

                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                          Report()));
                    },
                    color: Color(0xff3DA7F2),
                    textColor: Colors.white,
                    child: Text("Details Report".toUpperCase(),
                        style: TextStyle(fontSize: 14)),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Close!'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ));
  }
}



