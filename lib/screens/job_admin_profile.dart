import 'dart:convert';

import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/technician_profile.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_widget.dart';
import 'package:btakapps/utils/flutter_icons.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class JobAminProfile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<JobAminProfile> {
  bool _enabled = false;
  var name,department,image;

  Future getProfileData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var user_id = pref.get("user_id");

    print(user_id);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "user_id" : user_id
    };

    final response =
    await http.post(BaseUrl.baseurl+"user_profile.json",body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var data = Technician_Profile.fromJson(taskListMap);

    setState(() {
      name = data.data[0].result.name;
      department = data.data[0].result.departmentName;
      image = data.data[0].result.img;
    });
    //image = data.data[0].result.i;
    print(data.data[0].result.name);
//    setState(() {
//      pending = data.data[0].results.pendingTasks;
//      progress = data.data[0].results.inProgressTasks;
//      completed = data.data[0].results.completeTasks;
//      mytask = data.data[0].results.newTasks;
//      myservice = data.data[0].results.toalServices;
//    });
//
//    //print(token);
//
//    print(response);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileData();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 220,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  padding: EdgeInsets.only(top: 25),
                ),
                Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomAppBarWidget(),
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                      ),
                      SizedBox(height: 25),
                      _buildStatistic(name,department),
                      Padding(
                          padding: EdgeInsets.all(16),
                          child : Column(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      child: Text(
                                        "User Activity",
                                        style: TextStyle(
                                          color: Color(0xff1E2661),
                                          fontSize: 20,
                                          height: 1.5,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: _buildGender(FlutterIcons.male,
                                        Colors.orangeAccent, "15", "Tasks Pending"),
                                  ),
                                  SizedBox(width: 16),
                                  Expanded(
                                    child: _buildGender(FlutterIcons.female,
                                        Colors.pinkAccent, "23", "Tasks OnGoing"),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              Container(
                                width: 450,
                                height: 70,
                                child: Card(
                                   shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)
                                   ),
                                  elevation: 10,
                                  color: Color(0xffFFFFFF),
                                  child: ListTile(
                                    title: Text('Notification', style: TextStyle(
                                        fontSize: 18,
                                        color: Color(0xff1E2661)
                                    )),
                                     leading: Container(
                                       height: 34,
                                       width: 34,
                                       decoration: BoxDecoration(
                                         color: Colors.yellow,
                                         border: Border.all(color: Colors.yellow),
                                         borderRadius: BorderRadius.all(Radius.circular(8)),
                                         image: DecorationImage(
                                             image: AssetImage(
                                                 'assets/images/ic_data.png'),
                                             fit: BoxFit.contain
                                         ),
                                       ),
                                     ),
                                    trailing: CustomSwitch(
                                      value: _enabled,
                                      onChanged: (bool val){
                                        setState(() {
                                          _enabled = val;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 450,
                                height: 70,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                  elevation: 10,
                                  color: Color(0xffFFFFFF),
                                  child: ListTile(
                                    title: Text('Notification', style: TextStyle(
                                        fontSize: 18,
                                        color: Color(0xff1E2661)
                                    )),
                                    leading: Container(
                                      height: 34,
                                      width: 34,
                                      decoration: BoxDecoration(
                                        color: Colors.yellow,
                                        border: Border.all(color: Colors.yellow),
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/images/ic_data.png'),
                                            fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                    trailing: Image(
                                       height: 20,
                                       width: 20,
                                       image: AssetImage('assets/images/trailimage.png'),
                                    )
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 450,
                                height: 70,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                  elevation: 10,
                                  color: Color(0xffFFFFFF),
                                  child: ListTile(
                                    title: Text('Notification', style: TextStyle(
                                        fontSize: 18,
                                        color: Color(0xff1E2661)
                                    )),
                                    leading: Container(
                                      height: 34,
                                      width: 34,
                                      decoration: BoxDecoration(
                                        color: Colors.yellow,
                                        border: Border.all(color: Colors.yellow),
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/images/ic_data.png'),
                                            fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                    trailing: Image(
                                      height: 20,
                                      width: 20,
                                      image: AssetImage('assets/images/trailimage.png'),
                                    )
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ],
                          )

                      ),
                    ],
                  ),
                ),

                Positioned(
                  top:130 ,//change this as needed
                  height: 64,
                  width: 64,
                  left: 150,
                  child:ClipOval(
                    child: Image.network(
                      'https://picsum.photos/250?image=9',
                    ),
                  ),
                )
              ],
            )

        ),
      ),
      //backgroundColor: AppColors.backgroundColor,

    );
  }

  Widget _buildStatistic(String name,String department) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 24),
      padding: EdgeInsets.all(24),
      child: Center(
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[

                SizedBox(height: 20),
                Text(
                  name,
                  style: TextStyle(
                    color: Color(0xff1E2661),
                    fontSize: 20,
                    height: 1.5,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  department,
                  style: TextStyle(
                    color: Color(0xff1DBAC1),
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGender(IconData icon, Color color, String title, String value) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black87,
                      fontSize: 42,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                   Row(
                     children: <Widget>[
                       Container(
                         height: 34,
                         width: 34,
                         decoration: BoxDecoration(
                           color: Colors.yellow,
                           border: Border.all(color: Colors.yellow),
                           borderRadius: BorderRadius.all(Radius.circular(8)),
                           image: DecorationImage(
                               image: AssetImage(
                                   'assets/images/ic_data.png'),
                               fit: BoxFit.contain
                           ),
                         ),
                       ),
                       Column(
                         children: <Widget>[
                           Row(
                             children: <Widget>[
                                SizedBox(
                                  width: 15,
                                ),
                                 Column(
                                   children: <Widget>[
                                     Text(
                                       "Task",
                                       style: TextStyle(
                                           color: Color(0xff1E2661),
                                           height: 1.5,
                                           fontSize: 15
                                       ),
                                     ),
                                     Text(
                                       "Managed",
                                       style: TextStyle(
                                           color: Color(0xff1E2661),
                                           height: 1.5,
                                           fontSize: 15
                                       ),
                                     ),
                                   ],
                                 )
                             ],
                           ),

                           Column(
//                             children: persons.map((p) {
//                               return personDetailCard(p);
//                             }).toList()

                           )
                         ],
                       )

                     ],
                   ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  CustomSwitch({
    Key key,
    this.value,
    this.onChanged})
      : super(key: key);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
   // _animationController = AnimationController(value: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
        begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
        end: widget.value ? Alignment.centerLeft :Alignment.centerRight).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            width: 50.0,
            height: 22.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              color: _circleAnimation.value ==
                  Alignment.centerLeft
                  ? Colors.grey
                  : Colors.blue,),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 2.0, bottom: 2.0, right: 2.0, left: 2.0),
              child:  Container(
                alignment: widget.value
                    ? Alignment.centerRight
                    : Alignment.centerLeft,
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
