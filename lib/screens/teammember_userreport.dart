import 'dart:io';
import 'package:btakapps/db/dbhelper.dart';
import 'package:btakapps/model/todo.dart';
import 'package:btakapps/model/trip.dart';
import 'package:btakapps/model/tripmodel.dart';
import 'package:btakapps/screens/team_member.dart';
import 'package:btakapps/screens/teammember.dart';
import 'package:btakapps/screens/user_dashboard.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:btakapps/model/technician.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:btakapps/apiBase/base.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';

import 'change_password.dart';

class TeamMemberUserReports extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Column(
          children: [
            Text('User Report'),
          ],
        ),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),

      body: Body(),
    );
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String title = 'DropDownButton';
  String val;
  var teammemberid;
  List _list = ['list1','list2','list3'];
  File imageFile;
  DateTime _setDate = DateTime.now();
  Duration initialtimer = new Duration();
  int selectitem = 1;
  List<Todo> taskList = new List();
  List<RadioModel> sampleData = new List<RadioModel>();
  List<TechnicianClass> technicianList = List();
  TextEditingController search = new TextEditingController();

  Future getList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");
    var officeId = pref.get("office_id");
    var departmentId = pref.get("department_id");

    print(userid);

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "office_id" : officeId,
      "department_id" : departmentId

    };

    final response =
    await http.post(BaseUrl.baseurl+"technician_list.json",body: json.encode(body));

    Map technicianListMap = jsonDecode(response.body);
    var technicianListData = Technician_List.fromJson(technicianListMap);

    for(int i=0; i<technicianListData.data[0].results.length; i++){
      var id = technicianListData.data[0].results[i].technician.id;
      var name = technicianListData.data[0].results[i].technician.name;
      var designation = technicianListData.data[0].results[i].technician.designation;
      var jobinhand = technicianListData.data[0].results[i].technician.totalJobInHands;
      var userprofile = technicianListData.data[0].results[i].technician.userProfile;

      setState(() {
        technicianList.add(new TechnicianClass(id,name,designation,jobinhand,userprofile));
      });
    }

    print(technicianListData.data[0].results[0].technician.name);


  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getList();
  }
  @override
  Widget build(BuildContext context) {


    var height = MediaQuery.of(context).size;

    DateTime _fromDate = DateTime.now();
    TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());

    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];
    return SingleChildScrollView(

      child:  Container(
        color: Color(0xff3D4FF4),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.12,
              margin: EdgeInsets.only(top: 10),
              child: Wrap(
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: search,
                        style: TextStyle(color: Color(0xff99FFFFFF)),
                        autocorrect: true,
                        onChanged: (query) =>  updateSearchQuery(query),
                        decoration: InputDecoration(
                          hintText: 'Search team member...',
                          prefixIcon: Icon(Icons.search,color: Color(0xff99FFFFFF),),
                          hintStyle: TextStyle(color: Color(0xff99FFFFFF)),
                          filled: true,
                          fillColor: Color(0xff4F6AF2),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Color(0xff1AFFFFFF), width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Color(0xff1AFFFFFF), width: 1),
                          ),
                        ),)
                  ),
                ],

              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.8,
              decoration: BoxDecoration(
                  color: kShadowColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0)
                  )
              ),
              child: SizedBox(

                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: new ListView.builder(
                            physics: const NeverScrollableScrollPhysics(), //
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: technicianList.length,
                            itemBuilder: (BuildContext context, int index) =>
                                buildTripCard(context, index)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //child: Text('This is text'),
          ],
        ),
      ),
    );
  }

  Widget buildTripCard(BuildContext context, int index) {
    final technician = technicianList[index];
    return new Container(
      margin: EdgeInsets.only(left: 10,right: 10),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      height: 50.65,
                      width: 55.51,
                      child: CircleAvatar(
                        //backgroundColor: Colors.white,
                        backgroundImage: NetworkImage(technician.userprofile),
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 15,top: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          technician.name,
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xff223E5C),
                            fontFamily: 'DMSans-Bold',
                          ),
                        ),
                        Text(
                          technician.designation,
                          style: TextStyle(
                            fontSize: 12,
                            color: Color(0xffA6B0BF),
                            fontFamily: 'DMSans-Bold',
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Spacer(),
              Container(
                margin: EdgeInsets.only(top: 25),
                child: SizedBox(
                  width: 115,
                  height: 33,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Color(0xff2DCED6))
                    ),
                    onPressed: () {
                      //_addToDb();
                      teammemberid = technician.id;
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => UserDashboard(teammemberid:teammemberid)));
                    },
                    color: Color(0xff2DCED6),
                    textColor: Colors.white,
                    child: Text("View Report".toUpperCase(),
                        style: TextStyle(fontSize: 13)),
                  ),
                ),
              ),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  void _addToDb() async {
    String task = 'assets/images/img1.png';
    var id = await DatabaseHelper.instance.insert(Todo(title: task));
    setState(() {
      //taskList.insert(0, Todo(id: 0,title: "assets/images/img1.png"));
      //taskList.insert(1, Todo(id: 1,title: "assets/images/img2.png"));
      //taskList.insert(2, Todo(id: 2,title: "assets/images/img3.png"));
      taskList.insert(0, Todo(id: id, title: task));
    });
  }

  updateSearchQuery(String query) {
    for(int i=0; i<technicianList.length; i++){
      if(technicianList[i].name.contains(query)){
        //print();
        setState(() {
          var id = technicianList[i].id;
          var name = technicianList[i].name;
          var designation = technicianList[i].designation;
          var jobinhand = technicianList[i].jobinhand;
          var userprofile = technicianList[i].userprofile;
          setState(() {
            technicianList.add(new TechnicianClass(id,name,designation,jobinhand,userprofile));
          });

        });
      }
    }
  }
}




class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  bool _value = true;

  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Container(
        margin: EdgeInsets.only(left: 25,top: 10),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Center(
                child: InkWell(
                  child: Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xff2DD8CF)),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: _value
                          ? Icon(
                        Icons.check,
                        size: 13.0,
                        color: Colors.white,
                      )
                          : Icon(
                        Icons.check_box_outline_blank,
                        size: 10.0,
                        color: Color(0xff2DD8CF),
                      ),
                    ),
                  ),
                )),

            new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(_item.text,style: TextStyle(
                  fontSize: 14
              ),),
            )
          ],
        ),
      ),
    );
  }
}

void _bottomSheetMore(BuildContext context) {
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}




