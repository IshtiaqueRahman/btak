import 'dart:io';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/categorywise_service.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'create_new_task_technician.dart';
import 'create_new_task_technician2.dart';

class CreateNewTask1 extends StatefulWidget {
  String serviceName,departmentName;
  int deptid;

  CreateNewTask1({this.serviceName,this.departmentName,this.deptid});

  @override
  _CreateNewTaskState createState() => _CreateNewTaskState(serviceName,departmentName,deptid);
}

class _CreateNewTaskState extends State<CreateNewTask1> {
  String title = 'DropDownButton';
  String val;
  List _list = List();
  bool visible = true;
  File imageFile;
  int value;
  List _lists = List();
  List _listdata = List();
  var user;
  int deptid;
  List _listid = List();
  String serviceName,departmentName;
  TextEditingController task = new TextEditingController();
  List<String> isChecked = [];


  _CreateNewTaskState(String serviceName, String departmentName, int deptid){
      this.serviceName = serviceName;
      this.departmentName = departmentName;
      this.deptid = deptid;

      print(deptid);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getlistView(0);
    getCategory();
  }

  Future getCategory() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid = pref.get("user_id");

    //print(userid);

    Map<String, String> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "lang":"lang",
    };

    final response =
    await http.post(BaseUrl.baseurl+"service_list_category_wise.json",body: json.encode(body));

    //print(response.body);

    //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
    final jsonresp = json.decode(response.body);

    Map categoryMap = jsonDecode(response.body);
    var categorylist = CategorywiseService.fromJson(categoryMap);

    setState(() {
      for(int i=0; i<response.body.length; i++){
        _list.add(categorylist.data[0].services[i].name);
      }
    });
    //print(servicedetails.data[0].serviceDetails.taskList[0].task.toString());
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3D4FF4),
      appBar: AppBar(
        title: Text('Create New Service'),
        backgroundColor: Color(0xff3D4FF4),
        elevation: 0,
        leading: GestureDetector(
          child: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          onTap: (){
            Navigator.push(context, new MaterialPageRoute(builder: (context) => new CreateNewTask()));
          },
        ),
      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        margin: EdgeInsets.only(top: 25),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30)
            )
        ),
        child: SingleChildScrollView(
          child: Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 Row(
                   children: <Widget>[
                     Container(
                       margin: EdgeInsets.only(left: 10),
                       height: 18,
                       width: 18,
                       child: Image.asset('assets/images/dept.png'),
                     ),
                     SizedBox(
                       width: 10,
                     ),
                     Text(
                       'Request Name',
                       style: TextStyle(
                         fontSize: 14,
                         color: Color(0xff1E2661),
                         fontFamily: 'DMSans-Bold',
                       ),
                     ),
                   ],
                 ),

                 Container(
                   margin: EdgeInsets.only(top: 10,left: 35),
                   child: Text(
                     serviceName ?? '',
                     style: TextStyle(
                       fontSize: 16,
                       color: Color(0xff1E2661),
                       fontFamily: 'DMSans-Bold',
                     ),
                   ),
                 ),


                 SizedBox(
                   height: 20,
                 ),

                 Row(
                   children: <Widget>[
                     Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 18,
                        width: 18,
                        child: Image.asset('assets/images/notes_icon.png'),
                     ),
                     SizedBox(
                        width: 10,
                     ),
                     Text(
                       'Service List',
                       style: TextStyle(
                         fontSize: 14,
                         color: Color(0xff1E2661),
                         fontFamily: 'DMSans-Bold',
                       ),
                     ),
                   ],
                 ),
                 Container(
                   margin: EdgeInsets.only(top: 10,left: 25),
                   height: 42,
                   width: 272,
                   decoration: BoxDecoration(
                       color: Color(0xffF5F5F5),
                       border: Border.all(
                           color: Color(0xffF5F5F5)
                       ),
                       borderRadius: BorderRadius.circular(8.0)
                   ),
                   child: Center(
                     child: DropdownButtonHideUnderline(
                       child: DropdownButton(
                         value: val,
                         hint: Text('Select Service Category'),
                         style: TextStyle(
                             color: Colors.black38
                         ),
                         elevation: 5,
                         icon: Container(
                             margin: EdgeInsets.only(left: 10),
                             child: Icon(Icons.arrow_drop_down)),
                         iconSize: 20.0,
                         dropdownColor: Color(0xffF5F5F5),
                         onChanged: (value) {
                           setState(() {
                             val = value;
                             if(val == "Ac"){
                               getlistView(0);
                             }
                             else if(val == "TV"){
                               getlistView(1);
                             }

                           });
                         },
                         items: _list.map((value){
                           return DropdownMenuItem(
                             value: value,
                             child: Text(value),
                           );
                         }).toList(),
                       ),
                     ),
                   ),
                 ),


                 val!=null ? ListView(
                     shrinkWrap: true,
                     physics: NeverScrollableScrollPhysics(),
                     children: <Widget>[
                     ..._listdata
                         .map(
                           (item) => CheckboxListTile(
                             title: Text(item),
                         value: isChecked.contains(item),
                         onChanged: (bool value) {
                           if (value) {
                             setState(() {
                               isChecked.add(item);
                               getData(item);
                             });
                           } else {
                             setState(() {
                               isChecked.remove(item);
                             });
                           }
                         },
                       ),
                     )
                         .toList()
                   ],
                 ) : new Container(),


//                          ListView.builder(
//                              shrinkWrap: true,
//                              padding: const EdgeInsets.all(8),
//                              itemCount: _listdata.length,
//                              itemBuilder: (BuildContext context, int index) {
//                                if(val!=null){
//                                  return CheckboxListTile(
//                                   // value: index,
//                                    //groupValue: value,
//                                    //onChanged: (ind) => setState(() => value = ind),
//                                    title: Text("${_listdata[index]}"),
//                                  );
//                                }else{
//                                  return new Container();
//                                }
//
//                              }
//                          ),

                 SizedBox(
                   height: 22,
                 ),


                 Container(
                   margin: EdgeInsets.only(left: 30),
                   child: isChecked.contains("Others") ? Text(
                     'Add Task Name',
                     style: TextStyle(
                       fontSize: 14,
                       color: Color(0xff1E2661),
                       fontFamily: 'DMSans-Bold',
                     ),
                   ) : new Container()
                 ),

                 SizedBox(
                   height: 10,
                 ),

                 Container(
                   margin: EdgeInsets.only(left: 30,right: 30),
                   child: isChecked.contains("Others") ? TextField(
                     controller: task,
                     style: TextStyle(color: Color(0xff1E2661)),
                     autocorrect: true,
                     decoration: InputDecoration(
                       //labelStyle: TextStyle(color: Colors.black),
                       hintText: 'Type task name...',
                       hintStyle: TextStyle(color: Color(0xff1E2661)),
                       filled: true,
                       fillColor: Color(0xffF5F5F5),
                       enabledBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(12.0)),
                         borderSide: BorderSide(color: Color(0xff1AFFFFFF), width: 1),
                       ),
                       focusedBorder: OutlineInputBorder(
                         borderRadius: BorderRadius.all(Radius.circular(12.0)),
                         borderSide: BorderSide(color: Color(0xff1AFFFFFF), width: 1),
                       ),
                     ),
                   ) : new Container()
                 ),

                 SizedBox(
                   height: 22,
                 ),

                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceAround,
                   children: <Widget>[
                     SizedBox(
                       width: 132,
                       height: 48,
                       child: RaisedButton(
                         elevation: 0,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(12.0),
                             side: BorderSide(color: Color(0xffD4D4D4))
                         ),
                         onPressed: () {
                           //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyRental()));
                         },
                         color: Color(0xffD4D4D4),
                         textColor: Color(0xffFFFFFF),
                         child: Text("Previous".toUpperCase(),
                             style: TextStyle(fontSize: 14)),
                       ),
                     ),
                     SizedBox(
                       width: 132,
                       height: 48,
                       child: RaisedButton(
                         elevation: 0,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(12.0),
                             side: BorderSide(color: Color(0xff2DCED6))
                         ),
                         onPressed: () {
                           if(task!=null){
                             if(isChecked.contains("Others")){
                               isChecked.removeWhere((item) => item == 'Others');
                               isChecked.add(task.text.toString());
                             }
                           }
                           //print(isChecked);
                           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CreateNewTask2(isChecked:isChecked,listid:_listid,serviceName:serviceName,deptid:deptid,category:val)));
                         },
                         color: Color(0xff2DCED6),
                         textColor: Color(0xffFFFFFF),
                         child: Text("Next".toUpperCase(),
                             style: TextStyle(fontSize: 14)),
                       ),
                     ),
                   ],
                 ),

                 SizedBox(
                   height: 15,
                 ),

               ],
          ),
        ),
      ),
      //body: ListShow(),
    );
  }

  String getData(String name){

    for(int i=0; i<user.data[0].services[0].serviceList.length; i++){
      if(user.data[0].services[0].serviceList[i].name==name.trim()){
        //print();
        _listid.add(user.data[0].services[0].serviceList[i].id);
      }
    }
  }


   Future getlistView(int position) async{
   // print(val);
     _listdata.clear();
    int _currentTimeValue = 1;
    List _list = List();

    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      'lang': "en",
    };

    final response = await http.post(BaseUrl.baseurl+"service_list_category_wise.json",body: json.encode(body));

    //print(response.body);

    final jsonresp = json.decode(response.body);

    Map userMap = jsonDecode(response.body);
    user = CategorywiseService.fromJson(userMap);

    for(int i=0; i<response.body.length; i++){
      setState(() {

        //getData(user);
        _listdata.add(user.data[0].services[position].serviceList[i].name);
        
        //_listdata.add(user.data[0].services[position].serviceList[i].name);
        // var a=user.data[0].services[position].serviceList[i].id;
      });

    }

    return _listdata;

//    if(val != null){
//      return ListView(
//        primary: false,
//        shrinkWrap: true,
//        padding: EdgeInsets.all(12.0),
//        children: _listdata.map((timeValue) => RadioListTile(
//          groupValue: _currentTimeValue,
//          title: Text(timeValue._value),
//          value: timeValue._key,
//          onChanged: (val) {
//            setState(() {
//              debugPrint('VAL = $val');
//              _currentTimeValue = val;
//            }
//            );
//          },
//        )).toList(),
//      );
//    }else{
//      return new Container();
//    }

  }

  Future<Widget> getlistView1(String val) async{
    //print(val);

    _listdata.clear();
    List allItems = new List();

    int _currentTimeValue = 1;


    Map<String, String> body = {
      'reference_id': "BITAC-a8dcb57ae8c5",
      'lang': "en",
    };

    final response = await http.post(BaseUrl.baseurl+"service_list_category_wise.json",body: json.encode(body));

    //print(response.body);

    final jsonresp = json.decode(response.body);
    _list = jsonresp['data'][0]['services'][0]['service_list'];

    for(int i=0; i<_list.length; i++){
      _listdata.add(_list[i]['name']);
      //print(_list[i]['name']);
    }

    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(12.0),
      children: _listdata.map((timeValue) => RadioListTile(
        groupValue: _currentTimeValue,
        title: Text(timeValue._value),
        value: timeValue._key,
        onChanged: (val) {
          setState(() {
            debugPrint('VAL = $val');
            _currentTimeValue = val;
          }
          );
        },
      )).toList(),
    );


    //return _listdata;

    //allItems = getdata();

    print(allItems.length);

//    int _currentTimeValue = 1;
//
//    final _buttonOptions = [
//      TimeValue(30,  "30 minutes"),
//      TimeValue(60,  "1 hour"),
//      TimeValue(120, "2 hours"),
//      TimeValue(240, "4 hours"),
//      TimeValue(480, "8 hours"),
//      TimeValue(880, "12 hours"),
//      TimeValue(1620, "12 hours"),
//      TimeValue(2420, "12 hours"),
//      TimeValue(4800, "12 hours"),
//    ];

//    return ListView(
//      shrinkWrap: true,
//      padding: EdgeInsets.all(12.0),
//      children: _buttonOptions.map((timeValue) => RadioListTile(
//        groupValue: _currentTimeValue,
//        title: Text(timeValue._value),
//        value: timeValue._key,
//        onChanged: (val) {
//          setState(() {
//            debugPrint('VAL = $val');
//            _currentTimeValue = val;
//          }
//          );
//        },
//      )).toList(),
//    );
  }

  Future<dynamic> getdata() async{


     //print(_list.length);

      //final jsonresp = json.decode({"userId": 1,"username": "Alex", "language": "Python", "favorites": ["requests","selenium", "scrapy"]})
      //final jsonresp = json.decode(response.body);

      //int arraysize = jsonresp['data'][0]['services'];
      //print(arraysize);
   }
}



class TimeValue {
  final int _key;
  final String _value;
  TimeValue(this._key, this._value);
}




Widget chip(String label, Color color) {
  return Chip(
    labelPadding: EdgeInsets.all(3.0),
    avatar: Container(
      height: 5,
      width: 5,
      child: MaterialButton(
        onPressed: () {},
        color: Colors.blue,
        textColor: Colors.white,
        padding: EdgeInsets.all(15),
        shape: CircleBorder(),
      ),
    ),
    label: Text(
      label,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    backgroundColor: color,
    elevation: 6.0,
    shadowColor: Colors.grey[60],
    padding: EdgeInsets.all(6.0),
  );
}



