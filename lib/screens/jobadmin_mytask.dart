import 'package:btakapps/screens/taskdetailsoverview_jobadmin.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class JobAdminMyTask extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
      return Scaffold(
          backgroundColor: kBlueColor,
          appBar: AppBar(
             title: Text('My Task'),
             backgroundColor: Color(0xff6C5DD3),
            elevation: 0,
             leading: IconButton(
                icon: Icon(
                   Icons.arrow_back,
                   color: Colors.white,
                ),
             ),
          ),

        body: Body(),
      );
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool _color1,_color2,_color3,_color4;
  Future<List<User>> _getUsers() async {
    List<User> users = [];
    var response = await http.get('https://api.randomuser.me/?results=20');

    var jsonData = jsonDecode(response.body);

    var usersData = jsonData["results"];

    for (var user in usersData) {
      User newUser = User(user["name"]["first"] + user["name"]["last"],
          user["email"], user["picture"]["large"], user["phone"]);

      users.add(newUser);
    }

    return users;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _color1 = true;
    _color2 = true;
    _color3 = true;
    _color4 = true;
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff6C5DD3),
      child: Column(
        children: <Widget>[
          SizedBox(
             height: 15,
          ),
          Container(
            width: 327,
            height: 37,
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xff3BFFFFFF),
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  child : _color1 ? Container(
                    child: Text(
                      'All',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ) : Container(
                    height: 35,
                    width: 58,
                    decoration: BoxDecoration(
                        color: Color(0xff33FFFFFF),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(8.0),
                            topRight: const Radius.circular(8.0),
                            bottomRight: const Radius.circular(8.0),
                            bottomLeft: const Radius.circular(8.0)
                        )
                    ),

                    child: Center(
                      child: Text(
                        'All',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xffFFFFFF),
                          fontFamily: 'DMSans-Bold',
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                      setState(() {
                        _color1 = !_color1;
                        _color2 = true;
                        _color3 = true;
                        _color4 = true;
                      });
                  },
                ),
                GestureDetector(
                  child : _color2 ? Container(
                    child: Text(
                      'Pending',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ) : Container(
                    height: 35,
                    width: 62,
                    decoration: BoxDecoration(
                        color: Color(0xff33FFFFFF),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(8.0),
                            topRight: const Radius.circular(8.0),
                            bottomRight: const Radius.circular(8.0),
                            bottomLeft: const Radius.circular(8.0)
                        )
                    ),

                    child: Center(
                      child: Text(
                        'Pending',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xffFFFFFF),
                          fontFamily: 'DMSans-Bold',
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                    setState(() {
                      _color2 = !_color2;
                      _color1 = true;
                      _color3 = true;
                      _color4 = true;
                    });
                  },
                ),
                GestureDetector(
                  child : _color3 ? Container(
                    child: Text(
                      'Ongoing',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ) : Container(
                    height: 35,
                    width: 62,
                    decoration: BoxDecoration(
                        color: Color(0xff33FFFFFF),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(8.0),
                            topRight: const Radius.circular(8.0),
                            bottomRight: const Radius.circular(8.0),
                            bottomLeft: const Radius.circular(8.0)
                        )
                    ),

                    child: Center(
                      child: Text(
                        'Ongoing',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xffFFFFFF),
                          fontFamily: 'DMSans-Bold',
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                      setState(() {
                        _color3 = !_color3;
                        _color2 = true;
                        _color1 = true;
                        _color4 = true;
                      });
                  },
                ),

                GestureDetector(
                  child : _color4 ? Container(
                    child: Text(
                      'Completed',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontFamily: 'DMSans-Bold',
                      ),
                    ),
                  ) : Container(
                    height: 35,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Color(0xff33FFFFFF),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(8.0),
                            topRight: const Radius.circular(8.0),
                            bottomRight: const Radius.circular(8.0),
                            bottomLeft: const Radius.circular(8.0)
                        )
                    ),

                    child: Center(
                      child: Text(
                        'Completed',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xffFFFFFF),
                          fontFamily: 'DMSans-Bold',
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                      setState(() {
                        _color4 = !_color4;
                        _color2 = true;
                        _color3 = true;
                        _color1 = true;
                      });
                  },
                ),

              ],
            ),
          ),
          SizedBox(
            height: 50.0,
          ),
          Expanded(
              child: Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)
                      )
                  ),

                  child: Container(
                    child: FutureBuilder(
                        future: _getUsers(),
                        builder: (BuildContext context, AsyncSnapshot snapshot) {
                          if(snapshot.data == null){
                            return Container(
                              child: Center(
                                child: Text('loading'),
                              ),
                            );
                          }else{
                            return ListView.builder(
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemCount: snapshot.data.length,
                                itemBuilder: (BuildContext context, int index){
                                  return GestureDetector(
                                    child: Card(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    margin : EdgeInsets.only(left: 15,top: 15),
                                                    height: 15,
                                                    width: 15,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                              'assets/images/settings.png'),
                                                          fit: BoxFit.contain
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                      margin : EdgeInsets.only(left: 15,top: 15),
                                                      child: Text('you paragraph here')
                                                  ),

                                                  Spacer(),
                                                  Padding(
                                                    padding: const EdgeInsets.only(top: 10,right: 15),
                                                    child: Container(
                                                      child: CircularPercentIndicator(
                                                        radius: 50.0,
                                                        lineWidth: 5.0,
                                                        percent: 1.0,
                                                        center: new Text("100%"),
                                                        progressColor: Color(0xff2DCED6),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                    height: 15,
                                                    width: 15,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                              'assets/images/time.png'),
                                                          fit: BoxFit.contain
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                      margin : EdgeInsets.only(left: 15,top: 5,bottom: 15),
                                                      child: Text('Tomorrow')
                                                  ),
                                                  Container(
                                                      margin : EdgeInsets.only(left: 5,top: 5,bottom: 15),
                                                      child: Text('-')
                                                  ),
                                                  Container(
                                                      margin : EdgeInsets.only(left: 5,top: 5,bottom: 15),
                                                      child: Text('12.45 pm')
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                          Divider(),
                                          ListTile(
                                            title: Text(snapshot.data[index].fullName),
                                            subtitle: Text(snapshot.data[index].mobileNumber),
                                            leading: Container(
                                              height: 34,
                                              width: 34,
                                              decoration: BoxDecoration(
                                                //color: Colors.yellow,
                                                //border: Border.all(color: Colors.yellow),
                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                image: DecorationImage(
                                                     image: NetworkImage(snapshot.data[index].imageUrl)
//                                                image: AssetImage(
//                                                    'assets/images/ic_data.png'),
//                                                fit: BoxFit.contain
                                                ),
                                              ),
                                            ),
                                            //backgroundImage:
                                            //NetworkImage(snapshot.data[index].imageUrl)),
                                            trailing: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                RaisedButton(
                                                  elevation: 0,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(5.0),
                                                      side: BorderSide(color: Color(0xffA880E3))),
                                                  onPressed: () {},
                                                  color: Color(0xffF2ECFB),
                                                  textColor: Color(0xffA880E3),
                                                  child: Text("In Progress".toUpperCase(),
                                                      style: TextStyle(fontSize: 11)),
                                                ),
                                              ],

                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>
                                          TaskDetailsOverview()));
                                      },
                                  );
                                }
                            );
                          }
                        }
                    ),
                  )
              )
          ),
        ],
      ),
    );
  }
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

class UserDetailPage extends StatelessWidget {
  final User user;

  UserDetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.fullName),
      ),
    );
  }
}
