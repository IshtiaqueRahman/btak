import 'package:flutter/material.dart';
import 'actviity_job_admin.dart';
import 'overview_job_admin.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Color(0xff6C5DD3),
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Color(0xff6C5DD3),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(150.0),
            child: Container(
              margin: EdgeInsets.only(bottom: 25),
              child: AppBar(
                leading: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                ),
                backgroundColor: Color(0xff6C5DD3),
                elevation: 0,
                bottom: TabBar(
                  tabs: [
                    Text(
                      'Overview',
                      style: TextStyle(
                         fontSize: 16,
                         color: Color(0xffC4BEEF)
                      ),
                    ),
                    Text(
                      'Activity',
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xffC4BEEF)
                      ),
                    ),
                    Text(
                      '',
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xffC4BEEF)
                      ),
                    ),
                    //Tab(icon: Icon(Icons.directions_car)),
                    //Tab(icon: Icon(Icons.directions_transit))
                  ],
                ),
                title: Wrap(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text('Task Details'),
                      ],
                    ),
                  ],
                 ),
                ),
            ),
          ),
          body: TabBarView(
            children: [
              Overview(),
              Activity(),
              Activity(),
            ],
          ),
        ),
      ),
    );
  }
}