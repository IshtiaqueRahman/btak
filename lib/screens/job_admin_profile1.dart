import 'dart:convert';
import 'package:btakapps/apiBase/base.dart';
import 'package:btakapps/model/technician_profile.dart';
import 'package:btakapps/utils/consts.dart';
import 'package:btakapps/utils/custom_appbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'custombar.dart';

class JobAdminProfile1 extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<JobAdminProfile1> {
  bool _enabled = false;
  var name,department,image,phone;

  Future getProfileData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var user_id = pref.get("user_id");

    print(user_id);

    Map<String, dynamic> body = {
      "reference_id":"BITAC-a8dcb57ae8c5",
      "user_id" : user_id
    };

    final response =
    await http.post(BaseUrl.baseurl+"user_profile.json",body: json.encode(body));

    Map taskListMap = jsonDecode(response.body);
    var data = Technician_Profile.fromJson(taskListMap);

    setState(() {
      name = data.data[0].result.name;
      department = data.data[0].result.departmentName;
      phone = data.data[0].result.phone;
      image = data.data[0].result.img;
    });
    //image = data.data[0].result.i;
    print(data.data[0].result.name);
//    setState(() {
//      pending = data.data[0].results.pendingTasks;
//      progress = data.data[0].results.inProgressTasks;
//      completed = data.data[0].results.completeTasks;
//      mytask = data.data[0].results.newTasks;
//      myservice = data.data[0].results.toalServices;
//    });
//
//    //print(token);
//
//    print(response);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileData();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Container(
            color: Color(0xffF8F8FD),
            child: Stack(
              children: <Widget>[
                Container(
                  height: 220,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                  ),
                  padding: EdgeInsets.only(top: 25),
                ),
                Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomBarWidget(),
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                      ),
                      SizedBox(height: 25),
                      _buildStatistic(name,department,phone),
                      Padding(
                          padding: EdgeInsets.all(16),
                          child : Column(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "My Profile",
                                        style: TextStyle(
                                          color: '#1E2661'.toColor(),
                                          fontSize: 20,
                                          height: 1.5,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 8,right: 8),
                                    color: Colors.transparent,
                                    child: new Container(
                                        decoration: new BoxDecoration(
                                            borderRadius: new BorderRadius.only(
                                              topLeft: const Radius.circular(12.0),
                                              topRight: const Radius.circular(12.0),
                                              bottomLeft: const Radius.circular(12.0),
                                              bottomRight: const Radius.circular(12.0)
                                            )
                                        ),

                                      child: Column(
                                         children: [
                                           Container(
                                             width: 450,
                                             height: 70,
                                             child: Card(
                                               shape: RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.circular(15)
                                               ),
                                               elevation: 3,
                                               color: '#FFFFFF'.toColor(),
                                               child: ListTile(
                                                 title: Text('Language Setup', style: TextStyle(
                                                     fontSize: 17,
                                                     color: '#1E2661'.toColor()
                                                 )),
                                                 leading: Container(
                                                   height: 34,
                                                   width: 34,
                                                   decoration: BoxDecoration(
                                                     color: Color(0xffDFF3E7),
                                                     border: Border.all(color: Color(0xffDFF3E7)),
                                                     borderRadius: BorderRadius.all(Radius.circular(8)),
                                                     image: DecorationImage(
                                                         scale: 2.2,
                                                         image: AssetImage(
                                                             'assets/images/languagesetup.png'
                                                         ),
                                                     ),
                                                   ),
                                                 ),
                                                 trailing: Row(
                                                     mainAxisSize: MainAxisSize.min,
                                                     children: [
                                                     Container(
                                                       margin: EdgeInsets.only(right: 2),
                                                       child: Text('BN', style: TextStyle(
                                                           fontSize: 14,
                                                           color: '#1E2661'.toColor()
                                                       )),
                                                     ),
                                                     CustomSwitch(
                                                       value: _enabled,
                                                       onChanged: (bool val){
                                                         setState(() {
                                                           _enabled = val;
                                                         });
                                                       },
                                                     ),
                                                     Container(
                                                       margin: EdgeInsets.only(left: 2),
                                                       child: Text('EN', style: TextStyle(
                                                           fontSize: 14,
                                                           color: '#1E2661'.toColor()
                                                       )),
                                                     ),
                                                   ],
                                                 ),
                                               ),
                                             ),
                                           ),
                                           SizedBox(
                                             height: 10,
                                           ),
                                           Container(
                                             width: 450,
                                             height: 70,
                                             child: Card(
                                               shape: RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.circular(15)
                                               ),
                                               elevation: 3,
                                               color: '#FFFFFF'.toColor(),
                                               child: ListTile(
                                                   title: Text('Change Password', style: TextStyle(
                                                       fontSize: 18,
                                                       color: '#1E2661'.toColor()
                                                   )),
                                                   leading: Container(
                                                     height: 34,
                                                     width: 34,
                                                     decoration: BoxDecoration(
                                                       color: Color(0xffF5E9F9),
                                                       border: Border.all(color: Color(0xffF5E9F9)),
                                                       borderRadius: BorderRadius.all(Radius.circular(8)),
                                                       image: DecorationImage(
                                                           scale: 2.2,
                                                           image: AssetImage(
                                                               'assets/images/locks.png'),

                                                       ),
                                                     ),
                                                   ),
                                                   trailing: Image(
                                                     height: 12,
                                                     width: 10,
                                                     image: AssetImage('assets/images/right.png'),
                                                   )
                                               ),
                                             ),
                                           ),
                                           SizedBox(
                                             height: 10,
                                           ),
                                           Container(
                                             width: 450,
                                             height: 70,
                                             child: Card(
                                               shape: RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.circular(15)
                                               ),
                                               elevation: 3,
                                               color: '#FFFFFF'.toColor(),
                                               child: ListTile(
                                                   title: Text('Privacy Policy', style: TextStyle(
                                                       fontSize: 18,
                                                       color: '#1E2661'.toColor()
                                                   )),
                                                   leading: Container(
                                                     height: 34,
                                                     width: 34,
                                                     decoration: BoxDecoration(
                                                       color: Color(0xffFDF0E4),
                                                       border: Border.all(color: Color(0xffFDF0E4)),
                                                       borderRadius: BorderRadius.all(Radius.circular(8)),
                                                       image: DecorationImage(
                                                           scale: 2.2,
                                                           image: AssetImage(
                                                               'assets/images/policy.png'),

                                                       ),
                                                     ),
                                                   ),
                                                   trailing: Image(
                                                     height: 12,
                                                     width: 10,
                                                     image: AssetImage('assets/images/right.png'),
                                                   )
                                               ),
                                             ),
                                           ),
                                           SizedBox(
                                             height: 10,
                                           ),
                                           Container(
                                             width: 450,
                                             height: 70,
                                             child: Card(
                                               shape: RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.circular(15)
                                               ),
                                               elevation: 3,
                                               color: '#FFFFFF'.toColor(),
                                               child: ListTile(
                                                   title: Text('Terms & Conditions', style: TextStyle(
                                                       fontSize: 18,
                                                       color: '#1E2661'.toColor()
                                                   )),
                                                   leading: Container(
                                                     height: 34,
                                                     width: 34,
                                                     decoration: BoxDecoration(
                                                       color: Color(0xffFCE6E6),
                                                       border: Border.all(color: Color(0xffFCE6E6)),
                                                       borderRadius: BorderRadius.all(Radius.circular(8)),
                                                       image: DecorationImage(
                                                           scale: 2.2,
                                                           image: AssetImage(
                                                               'assets/images/terms.png'),

                                                       ),
                                                     ),
                                                   ),
                                                   trailing: Image(
                                                     height: 12,
                                                     width: 10,
                                                     image: AssetImage('assets/images/right.png'),
                                                   )
                                               ),
                                             ),
                                           ),
                                           SizedBox(
                                             height: 15,
                                           ),
                                         ],
                                      )

                                    ),
                                  ),

                                  SizedBox(
                                     height: 15,
                                  ),

                                  SizedBox(
                                    width: 327,
                                    height: 48,
                                    child: RaisedButton(
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(15.0),
                                          side: BorderSide(color: Color(0xff2DCED6))
                                      ),
                                      onPressed: () {},
                                      color: Color(0xff2DCED6),
                                      textColor: Color(0xffFFFFFF),
                                      child: Text("Logout".toUpperCase(),
                                          style: TextStyle(fontSize: 14)),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ],
                          )

                      ),
                    ],
                  ),
                ),

                Positioned(
                  top: 90 ,//change this as needed
                  height: 64,
                  width: 64,
                  left: 150,
                  child:ClipOval(
                    child: Image.network(
                      image,
                    ),
                  ),
                )
              ],
            )

        ),
      ),
      //backgroundColor: AppColors.backgroundColor,

    );
  }

  Widget _buildStatistic(String name,String department,String phone) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(1, 1),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 24),
      padding: EdgeInsets.all(24),
      child: Center(
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 2),
                      child: Text(
                        name,
                        style: TextStyle(
                          color: '#1E2661'.toColor(),
                          fontSize: 20,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 2),
                      child: Text(
                        phone,
                        style: TextStyle(
                          color: '#878DBA'.toColor(),
                          fontSize: 16,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Text(
                  department,
                  style: TextStyle(
                    color: '#1DBAC1'.toColor(),
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  CustomSwitch({
    Key key,
    this.value,
    this.onChanged})
      : super(key: key);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
        begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
        end: widget.value ? Alignment.centerLeft :Alignment.centerRight).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            width: 50.0,
            height: 22.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              color: _circleAnimation.value ==
                  Alignment.centerLeft
                  ? Colors.cyanAccent
                  : Colors.cyanAccent,),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 2.0, bottom: 2.0, right: 2.0, left: 2.0),
              child:  Container(
                alignment: widget.value
                    ? Alignment.centerRight
                    : Alignment.centerLeft,
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.cyan),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class User {
  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  User(this.fullName, this.email, this.imageUrl, this.mobileNumber);
}

extension ColorExtension on String {
  toColor() {
    var hexColor = this.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      return Color(int.parse("0x$hexColor"));
    }
  }
}