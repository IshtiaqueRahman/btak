import 'package:btakapps/utils/category_selector.dart';
import 'package:btakapps/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TechOverView extends StatefulWidget {
  @override
  _OverViewState createState() => _OverViewState();
}

class _OverViewState extends State<TechOverView> {
  String title = 'DropDownButton';
  String val;
  List _list = ['list1','list2','list3'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0xff3D4FF4),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0), // here the desired height
          child : AppBar(
            backgroundColor: Color(0xff3D4FF4),
            elevation: 0,
            title: Column(
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Text(
                  'OverView',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),
        ),

        body: Column(
          children: <Widget>[
            CategorySelector(),
            Expanded(
              child: Container(
                width: double.infinity,
                height: 1000,
                decoration: BoxDecoration(
                    color: kShadowColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0)
                    )
                ),
                child: Column(
                   children: <Widget>[
                      Container(
                         padding: EdgeInsets.all(20),
                         child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Container(
                                child: new Image.asset(
                                  'assets/images/clock.png',
                                  height: 17.0,
                                  width: 17.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Text(
                                'Requested Date',
                                style: TextStyle(
                                    color: '#878DBA'.toColor(),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                              Text(
                                '19 January at 10:00',
                                style: TextStyle(
                                    color: '#1E2661'.toColor(),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal
                                ),
                              ),
                            ],
                         )
                      ),
                      Container(
                        height: 0.2,
                        color: Colors.grey,
                      ),
                      Container(
                         padding: EdgeInsets.all(20),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: <Widget>[
                             new Container(
                               child: new Image.asset(
                                 'assets/images/clock.png',
                                 height: 17.0,
                                 width: 17.0,
                                 fit: BoxFit.cover,
                               ),
                             ),
                             Container(
                               margin: EdgeInsets.only(right: 75),
                               child: Text(
                                 'Task',
                                 style: TextStyle(
                                     color: '#878DBA'.toColor(),
                                     fontSize: 16.0,
                                     fontWeight: FontWeight.normal
                                 ),
                               ),
                             ),
                             Text(
                               'Please fix my laptop',
                               textAlign: TextAlign.right,
                               style: TextStyle(
                                   color: '#1E2661'.toColor(),
                                   fontSize: 16.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),
                           ],
                         )
                     ),
                      Container(
                       height: 0.2,
                       color: Colors.grey,
                     ),
                      Container(
                         padding: EdgeInsets.all(20),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: <Widget>[
                             new Container(
                               child: new Image.asset(
                                 'assets/images/status.png',
                                 height: 17.0,
                                 width: 17.0,
                                 fit: BoxFit.cover,
                               ),
                             ),
                             Container(
                               margin: EdgeInsets.only(right: 100),
                               child: Text(
                                 'Status',
                                 style: TextStyle(
                                     color: '#878DBA'.toColor(),
                                     fontSize: 16.0,
                                     fontWeight: FontWeight.normal
                                 ),
                               ),
                             ),
                             InkWell(
                               onTap: () => print('hello'),
                               child: new Container(
                                 width: 97.0,
                                 height: 32.0,
                                 decoration: new BoxDecoration(
                                   color: '#ECE6F9'.toColor(),
                                   border: new Border.all(color: '#A880E3'.toColor(), width: 2.0),
                                   borderRadius: new BorderRadius.circular(5.0),
                                 ),
                                 child: new Center(child: new Text('IN PROGRESS', style: new TextStyle(fontSize: 11.0, color: '#A880E3'.toColor()),),),
                               ),
                             ),
                           ],
                         )
                     ),
                      Container(
                       height: 0.2,
                       color: Colors.grey,
                     ),
                      Container(
                         padding: EdgeInsets.all(20),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: <Widget>[
                             new Container(
                               child: new Image.asset(
                                 'assets/images/progress.png',
                                 height: 17.0,
                                 width: 17.0,
                                 fit: BoxFit.cover,
                               ),
                             ),
                             Container(
                               margin: EdgeInsets.only(right: 105),
                               child: Text(
                                 'Progress',
                                 style: TextStyle(
                                     color: '#878DBA'.toColor(),
                                     fontSize: 16.0,
                                     fontWeight: FontWeight.normal
                                 ),
                               ),
                             ),
                             Container(
                               height: 30,
                               width: 70,
                               decoration: BoxDecoration(
                                   color: Colors.greenAccent,
                                   border: Border.all(
                                     color: Colors.greenAccent
                                  ),
                                 borderRadius: BorderRadius.circular(8.0)
                               ),
                               child: Center(
                                 child: DropdownButtonHideUnderline(
                                   child: DropdownButton(
                                     value: val,
                                     hint: Text('85%'),
                                     style: TextStyle(
                                         color: Colors.white
                                     ),
                                     elevation: 5,
                                     icon: Icon(Icons.arrow_drop_down),
                                     iconSize: 20.0,
                                     dropdownColor: Colors.white,
                                     onChanged: (value) {
                                       setState(() {
                                         val = value;
                                       });
                                     },
                                     items: _list.map((value){
                                       return DropdownMenuItem(
                                         value: value,
                                         child: Text(value),
                                       );
                                     }).toList(),
                                   ),
                                 ),
                               ),
                             )
                           ],
                         )
                     ),
                      Container(
                       height: 0.2,
                       color: Colors.grey,
                     ),
                     Container(
                         padding: EdgeInsets.all(20),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: <Widget>[
                             new Container(
                               child: new Image.asset(
                                 'assets/images/progress.png',
                                 height: 17.0,
                                 width: 17.0,
                                 fit: BoxFit.cover,
                               ),
                             ),
                             Container(
                               margin: EdgeInsets.only(right: 120),
                               child: Text(
                                 'Assigned to',
                                 style: TextStyle(
                                     color: '#878DBA'.toColor(),
                                     fontSize: 16.0,
                                     fontWeight: FontWeight.normal
                                 ),
                               ),
                             ),
                             Container(
                               height: 35,
                               width: 35,
                               child: Image.asset('assets/images/plus.png'),
                             )
                           ],
                         )
                     ),
                     Container(
                       height: 0.2,
                       color: Colors.grey,
                     ),
                     Container(
                         padding: EdgeInsets.all(20),
                         child: Column(
                           children: <Widget>[
                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               children: <Widget>[
                                 new Container(
                                   child: new Image.asset(
                                     'assets/images/clock.png',
                                     height: 17.0,
                                     width: 17.0,
                                     fit: BoxFit.cover,
                                   ),
                                 ),
                                 Container(
                                   margin: EdgeInsets.only(left: 20),
                                   child: Text(
                                     'Description',
                                     style: TextStyle(
                                         color: '#878DBA'.toColor(),
                                         fontSize: 16.0,
                                         fontWeight: FontWeight.normal
                                     ),
                                   ),
                                 ),
                               ],
                             ),
                             SizedBox(
                               height: 5,
                             ),
                             Text(
                               'Task Descriptions are the statements of scope for each of the project activities.Are written in the format of “action.',
                               style: TextStyle(
                                   color: '#1E2661'.toColor(),
                                   fontSize: 14.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),
                           ],
                         )
                     ),
                     Container(
                       height: 0.2,
                       color: Colors.grey,
                     ),
                     Container(
                         padding: EdgeInsets.all(20),
                         child: Column(
                           children: <Widget>[
                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               children: <Widget>[
                                 new Container(
                                   child: new Image.asset(
                                     'assets/images/clock.png',
                                     height: 17.0,
                                     width: 17.0,
                                     fit: BoxFit.cover,
                                   ),
                                 ),
                                 Container(
                                   margin: EdgeInsets.only(left: 20),
                                   child: Text(
                                     'Instruction from admin',
                                     style: TextStyle(
                                         color: '#878DBA'.toColor(),
                                         fontSize: 16.0,
                                         fontWeight: FontWeight.normal
                                     ),
                                   ),
                                 ),
                               ],
                             ),
                             SizedBox(
                               height: 5,
                             ),
                             Text(
                               'Task Descriptions are the statements of scope for each of the project activities.Are written in the format of “action.',
                               style: TextStyle(
                                   color: '#1E2661'.toColor(),
                                   fontSize: 14.0,
                                   fontWeight: FontWeight.normal
                               ),
                             ),
                             SizedBox(
                               height: 20,
                             ),
                           ],
                         )
                     ),

                   ],
                ),
                //child: Text('This is text'),
              ),
            )
          ],
        )

    );
  }
}

extension ColorExtension on String {
  toColor() {
    var hexColor = this.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      return Color(int.parse("0x$hexColor"));
    }
  }
}