import 'package:flutter/material.dart';

class CustomAppBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
               children: <Widget>[
                 Align(
                   alignment: AlignmentDirectional.topStart,
                   child: Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     mainAxisAlignment: MainAxisAlignment.start,
                     children: <Widget>[
                       Container(
                         child: Text(
                           "Hello Alamin",
                           style: TextStyle(
                             fontSize: 20,
                             color: Colors.white,
                             height: 1.5,
                           ),
                         ),
                       ),
                       Container(
                         child: Text(
                           "Monday, 17 jan 2020",
                           style: TextStyle(
                             fontSize: 16,
                             color: Color(0xffCCFFFFFF),
                             height: 1.5,
                           ),
                         ),
                       ),
                     ],
                   ),
                 ),
                 SizedBox(
                   height: 24,
                   width: 24,
                 ),
                 Row(
                   children: <Widget>[
                     Container(
                       height: 24,
                       width: 24,
                       child: Image.asset('assets/images/bell.png'),
                     ),
                     Container(
                       margin: EdgeInsets.only(bottom: 25),
                       height: 5,
                       width: 5,
                       child: Image.asset('assets/images/middledot.png'),
                     )
                   ],
                 )


               ],
            ),
          ),
        )
      ],
    );
  }
}

