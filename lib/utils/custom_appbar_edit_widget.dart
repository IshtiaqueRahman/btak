import 'package:flutter/material.dart';

class CustomAppBarEditWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 25,
              height: 25,
              margin: EdgeInsets.all(32),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/path.png"),
                ),
              ),
            ),
            SizedBox(
              width: 40,
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                "Edit Profile",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  height: 1.5,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

